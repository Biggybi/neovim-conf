if vim.b.did_ftplugin ~= 1 then return end
vim.b.did_ftplugin = 1

vim.keymap.set("n", "<leader>cb", function() vim.cmd("10sp | terminal bear -- make") end, {
  desc = "bear make",
  buffer = 0,
})
