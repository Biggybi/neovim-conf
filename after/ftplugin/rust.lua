if vim.b.did_ftplugin ~= 1 then return end
vim.b.did_ftplugin = 1

vim.bo.makeprg = "cargo build"

--stylua: ignore start
vim.keymap.set("n", "<leader>cc", "<cmd>!cargo check<CR>",  { desc = "Cargo check<CR>", buffer = 0 })
vim.keymap.set("n", "<leader>cb", "<cmd>!cargo build<CR>",  { desc = "Cargo build<CR>", buffer = 0 })
vim.keymap.set("n", "<leader>cr", "<cmd>!cargo run<CR>",    { desc = "Cargo run<CR>", buffer = 0 })
vim.keymap.set("n", "<leader>ct", "<cmd>!cargo test<CR>",   { desc = "Cargo test<CR>", buffer = 0 })
vim.keymap.set("n", "<leader>cd", "<cmd>!cargo doc<CR>",    { desc = "Cargo doc<CR>", buffer = 0 })
vim.keymap.set("n", "<leader>ci", "<cmd>!cargo clippy<CR>", { desc = "Cargo clippy<CR>", buffer = 0 })
vim.keymap.set("n", "<leader>cp", "<cmd>!cargo fmt<CR>",    { desc = "Cargo fmt<CR>", buffer = 0 })
--stylua: ignore end

require("core.conflib").set_undo_ftplugin("setlocal makeprg<")
