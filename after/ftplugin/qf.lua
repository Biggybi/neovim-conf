if vim.b.did_ftplugin ~= 1 then return end
vim.b.did_ftplugin = 1

vim.keymap.set("n", "dd", function()
  local count = vim.v.count1
  for _ = 1, count do
    require("core.conflib").del_qf_item(vim.api.nvim_win_get_cursor(0)[1])
  end
end, { silent = true, buffer = true, desc = "Remove qf entry" })

vim.keymap.set("n", "d", function()
  vim.go.operatorfunc = "v:lua.require'core.conflib'.del_qf_range"
  return "g@"
end, { expr = true, silent = true, buffer = true, desc = "Remove qf entry" })

vim.keymap.set("x", "d", function()
  vim.go.operatorfunc = "v:lua.require'core.conflib'.del_qf_range"
  return "g@"
end, { expr = true, silent = true, buffer = true, desc = "Remove qf range" })

require("core.conflib").set_undo_ftplugin("exe 'nunmap <buffer> dd' | exe 'xunmap <buffer> d'")
