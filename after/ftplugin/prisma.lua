if vim.b.did_ftplugin and vim.b.did_ftplugin ~= 1 then return end
vim.b.did_ftplugin = 1

vim.bo.commentstring = "// %s"

require("core.conflib").set_undo_ftplugin("setlocal commentstring<")
