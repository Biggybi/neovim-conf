if vim.b.did_ftplugin ~= 1 then return end
vim.b.did_ftplugin = 1

vim.keymap.set("n", "<leader>c.", function() vim.cmd("10sp | terminal python3 %") end, {
  desc = "Run current file",
  buffer = 0,
})
