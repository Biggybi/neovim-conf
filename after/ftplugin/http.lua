local map = vim.keymap.set

map("n", "<leader>cc", function() require("kulala").run() end, { buffer = 0, desc = "Run http request" })
map("n", "<c-j>", function() require("kulala").jump_next() end, { buffer = 0, desc = "Next http request" })
map("n", "<c-k>", function() require("kulala").jump_prev() end, { buffer = 0, desc = "Next http request" })
