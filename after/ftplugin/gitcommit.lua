if vim.b.did_ftplugin ~= 1 then return end
vim.b.did_ftplugin = 1

vim.b.spell = true

require("core.conflib").set_undo_ftplugin("setlocal spell<")
