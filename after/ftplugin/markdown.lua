if vim.b.did_ftplugin ~= 1 then return end
vim.b.did_ftplugin = 1

vim.bo.iskeyword = "@,48-57,_,192-255,-"
vim.bo.shiftwidth = 2
vim.bo.tabstop = 2
vim.bo.softtabstop = 2
vim.bo.suffixesadd = ".md"

require("core.conflib").set_undo_ftplugin("setlocal iskeyword< shiftwidth< tabstop< softtabstop< suffixesadd<")
