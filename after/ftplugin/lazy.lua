if vim.b.did_ftplugin and vim.b.did_ftplugin ~= 1 then return end
vim.b.did_ftplugin = 1

vim.keymap.set("n", "<esc>", function() vim.api.nvim_win_close(0, false) end, {
  desc = "close lazy",
  buffer = 0,
})
