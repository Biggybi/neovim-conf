if vim.b.did_ftplugin ~= 1 then return end
vim.b.did_ftplugin = 1

vim.opt_local.signcolumn = "no"

require("core.conflib").set_undo_ftplugin("setlocal signcolumn<")
