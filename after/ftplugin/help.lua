if vim.b.did_ftplugin ~= 1 then return end
vim.b.did_ftplugin = 1

vim.bo.iskeyword = "@,48-57,_,192-255,-"
vim.wo.number = false
vim.wo.statuscolumn = ""
vim.wo.signcolumn = "no"

vim.keymap.set("n", "<c-m>", "<c-]>", { buffer = 0, desc = "Jump to tag" })
vim.keymap.set("n", "<BS>", "<c-t>", { buffer = 0, desc = "Back to tag" })

local function find_doc()
  local saved_iskeyword = vim.bo.iskeyword
  vim.bo.iskeyword = '!-~,^*,^|,^",192-255'
  local ok, err = pcall(function() vim.cmd("normal! K") end, "normal! K")
  if not ok then
    if not err then return end
    local errnum = err:match("E[0-9]+")
    -- remove 'Sorry, ', capitalize the first letter
    local errmsg = err:match("E[0-9]+: Sorry, (.*)"):gsub("^%l", string.upper)
    vim.notify(errmsg, 2, { title = errnum })
  end
  vim.bo.iskeyword = saved_iskeyword
end

vim.keymap.set("n", "K", function() find_doc() end, { buffer = 0, desc = "Find doc" })
vim.keymap.set("n", "<c-]>", function() find_doc() end, { buffer = 0, desc = "Find doc" })

require("core.conflib").set_undo_ftplugin("setlocal iskeyword< number< statuscolumn< signcolumn")
