vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  pattern = "*.http",
  command = "setfiletype http",
  desc = "Set http filetype",
})
