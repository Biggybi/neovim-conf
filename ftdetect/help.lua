local autocmd = vim.api.nvim_create_autocmd

-- HACK: for telescope skipping ft detect
autocmd({ "BufRead", "BufEnter", "WinEnter", "BufNewFile" }, {
  pattern = {
    vim.fn.stdpath("data") .. "/lazy/*/doc/*.txt",
    vim.fn.stdpath("state") .. "/nvim/lazy/readme/doc/*.md",
    "/usr/share/nvim/runtime/doc/*.txt",
  },
  callback = function() vim.bo.buftype = "help" end,
  desc = "Set help files buftype",
})
