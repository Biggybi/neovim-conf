vim.api.nvim_create_autocmd({ "BufRead", "BufEnter", "WinEnter", "BufNewFile" }, {
  pattern = { "git/config" },
  callback = function() vim.bo.filetype = "gitconfig" end,
  desc = "Set git/config filetype",
})
