return {
  "mbbill/undotree",
  keys = {
    {
      "you",
      function() vim.cmd("UndotreeToggle") end,
      desc = "Toggle Undotree",
    },
  },
  cmd = {
    "UndotreeToggle",
    "UndotreeShow",
    "UndotreeHide",
    "UndotreeFocus",
    "UndotreePersistUndo",
  },
  -- config = function() end,
}
