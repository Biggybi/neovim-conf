return {
  "folke/flash.nvim",
  -- event = "BufRead",
  opts = function()
    vim.api.nvim_create_autocmd({ "CmdlineLeave" }, {
      group = vim.api.nvim_create_augroup("FlashToggleOff", {}),
      pattern = "*",
      callback = function() require("flash").toggle(false) end,
    })

    return {
      search = {
        incremental = true,
        enabled = true,
      },
      highlight = {
        backdrop = false,
      },
      modes = {
        search = { enabled = false },
        char = { enabled = false },
      },
      prompt = {
        enable = true,
        prefix = { { "⚡", "FlashPromptIcon" } },
        win_config = {
          relative = "editor",
          row = 2,
          col = vim.o.columns / 2 - 10,
          width = 20,
        },
      },
    }
  end,
  keys = {
    { "<C-/>", mode = { "n", "x" }, function() require("flash").jump() end, desc = "Flash" },
    { "<C-/>", mode = { "c" }, function() require("flash").toggle() end, desc = "Toggle Flash Search" },
    { "R", mode = { "o", "x" }, function() require("flash").treesitter_search() end, desc = "Treesitter Search" },
    { "[V", mode = { "n", "x", "o" }, function() require("flash").treesitter() end, desc = "Flash Treesitter" },
    { "<C-/>", mode = "o", function() require("flash").jump() end, desc = "Remote Flash" },
  },
}
