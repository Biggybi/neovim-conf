-- require("snacks") -- for lazydev

return {
  "biggybi/snacks.nvim",
  priority = 1000,
  lazy = false,
  ---@type snacks.Config|function: () -> snacks.Config
  opts = {
    image = require("plugins.snacks.image"),
    indent = require("plugins.snacks.indent"),
    notifier = require("plugins.snacks.notifier"),
    styles = require("plugins.snacks.styles"),
    picker = require("plugins.snacks.picker"),
    dashboard = require("plugins.snacks.dashboard"),
    dim = { enabled = false },
    statuscolumn = { enabled = false },
    scroll = { enabled = false },
    scratch = { enabled = false },
    git = { enabled = false },
    input = { enabled = false },
    words = {},
    quickfile = {},
    bigfile = {},
  },
  keys = {
    {
      "<leader>fe",
      function() Snacks.picker.explorer({ layout = "prompt_center", select = "prompt" }) end,
      desc = "File explorer",
    },
    {
      "<leader>ef",
      function() Snacks.picker.explorer() end,
      desc = "File explorer panel",
    },
    { "<leader>fb", function() Snacks.picker.buffers() end, desc = "Buffers" },
    { "<leader>fh", function() Snacks.picker.recent() end, desc = "Recent" },
    { "<leader>,", function() Snacks.picker.smart() end, desc = "Smart" },
    -- { "<leader>/", function() Snacks.picker.grep() end, desc = "Grep" },
    -- { "<leader>:", function() Snacks.picker.command_history() end, desc = "Command History" },
    { "<leader>fo", function() Snacks.picker.recent() end, desc = "Old files (history)" },
    {
      "<leader>ff",
      function() Snacks.picker.files({ dirs = { vim.uv.os_homedir() .. "/" } }) end,
      desc = "Find Files Home",
    },
    { "<leader>F", function() Snacks.picker.files({ cwd = "/" }) end, desc = "Find Files Root" },
    { "<leader><c-f>", function() Snacks.picker.files({ cwd = vim.uv.cwd() }) end, desc = "Find Files CurDir" },
    {
      "<leader>f.",
      function() Snacks.picker.files({ cwd = "/home/trx/dotfiles/" }) end,
      desc = "Find Files Dotfiles",
    },
    { "<leader>fw", function() Snacks.picker.files({ cwd = "/data/work/" }) end, desc = "Find Files" },
    { "<leader>fp", function() Snacks.picker.projects() end, desc = "Projects" },
    {
      "<leader>fP",
      function()
        Snacks.picker.files({
          cwd = vim.fs.joinpath(vim.fn.stdpath("data") .. "/lazy"),
        })
      end,
      desc = "find plugin files",
    },
    {
      "<leader>fn",
      function() Snacks.picker.files({ cwd = "~/Notes/second_brain/" }) end,
      desc = "Find Files Notes",
    },
    {
      "<leader>fi",
      function()
        local cwd = vim.fn.stdpath("config")
        if type(cwd) == "table" then cwd = cwd[1] end
        Snacks.picker.files({ cwd = cwd })
      end,
      desc = "Find Files Config",
    },
    -- git
    { "<leader>gf", function() Snacks.picker.git_files() end, desc = "Find Git Files" },
    { "<leader>gc", function() Snacks.picker.git_log() end, desc = "Git Log" },
    { "<leader>gb", function() Snacks.picker.git_branches({ all = true }) end, desc = "Snacks git branches" },
    { "<leader>gB", function() Snacks.gitbrowse() end, desc = "Git Browse", mode = { "n", "v" } },
    { "<leader>g<c-b>", function() Snacks.git.blame_line() end, desc = "Git Blame Line" },
    -- grep
    { "<leader>fl", function() Snacks.picker.lines() end, desc = "Buffer Lines" },
    { "<leader>fR", function() Snacks.picker.grep_buffers() end, desc = "Grep Open Buffers" },
    { "<leader>fr", function() Snacks.picker.grep() end, desc = "Grep" },
    { "<leader>f<c-r>", function() Snacks.picker.grep_word() end, desc = "Grep cursor word" },
    { "<leader>fr", function() Snacks.picker.grep_word() end, desc = "Grep selection", mode = { "x" } },
    { "<leader>f<c-r>", function() Snacks.picker.grep_word() end, desc = "Grep selection", mode = { "x" } },
    -- search
    { '<leader>f"', function() Snacks.picker.registers() end, desc = "Registers" },
    { "<leader>fa", function() Snacks.picker.autocmds() end, desc = "Autocmds" },
    { "<leader>f:", function() Snacks.picker.command_history() end, desc = "Command History" },
    { "<leader>f;", function() Snacks.picker.commands() end, desc = "Commands" },
    { "<leader>f/", function() Snacks.picker.search_history() end, desc = "Searches" },
    { "<leader>fd", function() Snacks.picker.diagnostics() end, desc = "Diagnostics" },
    { "<leader>f<c-d>", function() Snacks.picker.diagnostics_buffer() end, desc = "Buffer Diagnostics" },
    { "<leader>fh", function() Snacks.picker.help() end, desc = "Help Pages" },
    { "<leader>fm", function() Snacks.picker.man() end, desc = "Man Pages" },
    { "<leader>fH", function() Snacks.picker.highlights() end, desc = "Highlights" },
    { "<leader>fj", function() Snacks.picker.jumps() end, desc = "Jumps" },
    { "<leader>fk", function() Snacks.picker.keymaps() end, desc = "Keymaps" },
    { "<leader>fq", function() Snacks.picker.qflist() end, desc = "Quickfix List" },
    { "<leader>f<c-q>", function() Snacks.picker.loclist() end, desc = "Location List" },
    { "<leader>f'", function() Snacks.picker.marks() end, desc = "Marks" },
    { "<leader>f<space>", function() Snacks.picker.resume() end, desc = "Resume" },
    { "<leader>fC", function() Snacks.picker.colorschemes() end, desc = "Colorschemes" },
    { "<leader>fu", function() Snacks.picker.undo() end, desc = "Undo tree" },
    -- LSP
    { "<leader>gd", function() Snacks.picker.lsp_definitions() end, desc = "Goto Definition" },
    { "<leader>gD", function() Snacks.picker.lsp_declarations() end, desc = "Goto Declaration" },
    { "<leader>gr", function() Snacks.picker.lsp_references() end, nowait = true, desc = "References" },
    { "<leader>gi", function() Snacks.picker.lsp_implementations() end, desc = "Goto Implementation" },
    { "<leader>gt", function() Snacks.picker.lsp_type_definitions() end, desc = "Goto Type Definition" },
    { "<leader>fs", function() Snacks.picker.lsp_symbols() end, desc = "LSP Symbols" },
    -- various select
    { "z=", function() Snacks.picker.spelling({ layout = "select" }) end, desc = "Correct spell" },
    -- toggle
    { "yug", function() Snacks.zen() end, desc = "Toggle Zen Mode" },
    { "yuz", function() Snacks.zen.zoom() end, desc = "Toggle Zoom" },
    { "<leader>es", function() Snacks.scratch() end, desc = "Toggle Scratch Buffer" },
    { "<leader>eS", function() Snacks.scratch.select() end, desc = "Select Scratch Buffer" },
    { "yun", function() Snacks.notifier.show_history() end, desc = "Notification History" },
    -- { "<leader>bd", function() Snacks.bufdelete() end, desc = "Delete Buffer" },
    { "<leader>rf", function() Snacks.rename.rename_file() end, desc = "Rename File" },
    { "]]", function() Snacks.words.jump(vim.v.count1) end, desc = "Next Reference", mode = { "n", "t" } },
    { "[[", function() Snacks.words.jump(-vim.v.count1) end, desc = "Prev Reference", mode = { "n", "t" } },
    {
      "yun",
      desc = "Neovim News",
      function()
        Snacks.win({
          file = vim.api.nvim_get_runtime_file("doc/news.txt", false)[1],
          width = 0.6,
          height = 0.6,
          wo = { spell = false, wrap = false, signcolumn = "yes", statuscolumn = " ", conceallevel = 3 },
        })
      end,
    },
  },
  init = function()
    vim.api.nvim_create_autocmd("User", {
      pattern = "VeryLazy",
      callback = function()
        -- Setup some globals for debugging (lazy-loaded)
        _G.dd = function(...) Snacks.debug.inspect(...) end
        _G.bt = function() Snacks.debug.backtrace() end
        vim.print = _G.dd -- Override print to use snacks for `:=` command

        -- Create some toggle mappings
        Snacks.toggle.option("spell", { name = "Spelling" }):map("yos")
        Snacks.toggle.option("wrap", { name = "Wrap" }):map("yow")
        Snacks.toggle.option("relativenumber", { name = "Relative Number" }):map("yor")
        Snacks.toggle.diagnostics():map("yud")
        Snacks.toggle.line_number():map("yon")
        Snacks.toggle
          .option("conceallevel", { off = 0, on = vim.o.conceallevel > 0 and vim.o.conceallevel or 2 })
          :map("yoC")
        Snacks.toggle.treesitter():map("yot")
        Snacks.toggle.option("background", { off = "light", on = "dark", name = "Dark Background" }):map("yub")
        Snacks.toggle.inlay_hints():map("yuk")
        Snacks.toggle.indent():map("yui")
        Snacks.toggle.dim():map("yu<c-d>")
      end,
    })
  end,
}
