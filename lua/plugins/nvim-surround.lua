return {
  "kylechui/nvim-surround",
  keys = {
    { "ys" },
    { "yss" },
    { "yS" },
    { "ySS" },
    { "ds" },
    { "cs" },
    { "cS" },
    { "S", mode = "v" },
    { "gS", mode = "v" },
    { "<C-g>s", mode = "i" },
    { "<C-g>S", mode = "i" },
  },
  config = true,
}
