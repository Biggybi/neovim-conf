local function feedTermCode(code)
  return vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(code, true, true, true), "n", true)
end

return {
  enabled = false,
  "L3MON4D3/LuaSnip",
  event = { "InsertEnter" },
  dependencies = {
    "rafamadriz/friendly-snippets",
  },
  keys = {
    { "<C-Space>", function() require("luasnip").expand() end, mode = "i", desc = "Luasnip expand" },
    {
      "<c-f>",
      function()
        local luasnip = require("luasnip")
        if luasnip.expand_or_jumpable() then
          luasnip.jump(1)
        else
          feedTermCode("<right>")
        end
      end,
      mode = { "i", "s" },
      desc = "Luasnip next",
    },
    {
      "<c-b>",
      function()
        local luasnip = require("luasnip")
        if luasnip.expand_or_jumpable() then
          luasnip.jump(-1)
        else
          feedTermCode("<left>")
        end
      end,
      mode = { "i", "s" },
      desc = "Luasnip prev",
    },
    {
      "<C-E>",
      function()
        local luasnip = require("luasnip")
        if luasnip.choice_active() then
          luasnip.change_choice(1)
        else
          feedTermCode("<end>")
        end
      end,
      mode = { "i", "s" },
      desc = "Luasnip change choice",
    },
  },
  opts = function() require("luasnip.loaders.from_vscode").lazy_load() end,
}
