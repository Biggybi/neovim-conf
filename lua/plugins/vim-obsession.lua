local function obsessionToggle()
  if vim.fn.ObsessionStatus() == "" then
    vim.cmd("Obsession .git/Session.vim")
  else
    vim.cmd("Obsession")
  end
end

return {
  "tpope/vim-obsession",
  event = { "BufEnter" },
  keys = {
    { "yoo", obsessionToggle, desc = "Toggle Obsession" },
  },
  cmd = { "Obsession" },
}
