return {
  "epwalsh/obsidian.nvim",
  version = "*", -- recommended, use latest release instead of latest commit
  dependencies = {
    "nvim-lua/plenary.nvim",
  },
  event = {
    string.format("BufReadPre %s/**", vim.fn.expand("~/Notes/second_brain")),
    string.format("BufNewFile %s/**", vim.fn.expand("~/Notes/second_brain")),
  },
  cmd = {
    "ObsidianOpen",
    "ObsidianNew",
    "ObsidianQuickSwitch",
    "ObsidianFollowLink",
    "ObsidianBacklinks",
    "ObsidianTags",
    "ObsidianToday",
    "ObsidianYesterday",
    "ObsidianTomorrow",
    "ObsidianDailies",
    "ObsidianTemplate",
    "ObsidianSearch",
    "ObsidianLink",
    "ObsidianLinkNew",
    "ObsidianLinks",
    "ObsidianExtractNote",
    "ObsidianWorkspace",
    "ObsidianPasteImg",
    "ObsidianRename",
    "ObsidianToggleCheckbox",
    "ObsidianNewFromTemplate",
    "ObsidianTOC",
  },
  keys = {
    { "gf", "<cmd>ObsidianFollowLink<cr>", desc = "Follow link", ft = "markdown" },
    { "<cr>", "<cmd>ObsidianToggleCheckbox<cr>", desc = "Toggle checkbox", ft = "markdown" },
  },
  opts = {
    ui = {
      enable = false,
    },
    workspaces = {
      {
        name = "second_brain",
        path = "~/Notes/second_brain",
      },
    },
    completion = {
      min_chars = 0,
    },
    follow_img_func = function(img)
      local os = vim.system({ "uname" }):wait().stdout
      if os == nil then return end
      os = os:sub(1, -2):lower()
      if os:match("linux") then vim.fn.jobstart({ "xdg-open", img }) end
      if os:match("darwin") then vim.fn.jobstart({ "qlmanage", "-p", img }) end
    end,
  },
}
