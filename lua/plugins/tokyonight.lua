local terminal_colors = {
  dark = function()
    vim.g.terminal_color_0 = "#222436"
    vim.g.terminal_color_1 = "#ff757f"
    vim.g.terminal_color_2 = "#c3e88d"
    vim.g.terminal_color_3 = "#ffc777"
    vim.g.terminal_color_4 = "#82aaff"
    vim.g.terminal_color_5 = "#c099ff"
    vim.g.terminal_color_6 = "#86e1fc"
    vim.g.terminal_color_7 = "#c8d3f5"
    vim.g.terminal_color_8 = "#3b4261"
    vim.g.terminal_color_9 = "#ff757f"
    vim.g.terminal_color_10 = "#c3e88d"
    vim.g.terminal_color_11 = "#ffc777"
    vim.g.terminal_color_12 = "#82aaff"
    vim.g.terminal_color_13 = "#fca7ea"
    vim.g.terminal_color_14 = "#86e1fc"
    vim.g.terminal_color_15 = "#828bb8"
  end,
  light = function()
    vim.g.terminal_color_0 = "#e1e2e7"
    vim.g.terminal_color_1 = "#f52a65"
    vim.g.terminal_color_2 = "#587539"
    vim.g.terminal_color_3 = "#8c6c3e"
    vim.g.terminal_color_4 = "#2e7de9"
    vim.g.terminal_color_5 = "#9854f1"
    vim.g.terminal_color_6 = "#007197"
    vim.g.terminal_color_7 = "#3760bf"
    vim.g.terminal_color_8 = "#c4c8da"
    vim.g.terminal_color_9 = "#f52a65"
    vim.g.terminal_color_10 = "#587539"
    vim.g.terminal_color_11 = "#8c6c3e"
    vim.g.terminal_color_12 = "#2e7de9"
    vim.g.terminal_color_13 = "#7847bd"
    vim.g.terminal_color_14 = "#007197"
    vim.g.terminal_color_15 = "#6172b0"
  end,
}

local auto_reload_term_colors = function()
  -- HACK: update terminal colors (for new terms only)
  vim.api.nvim_create_autocmd({ "TermOpen" }, {
    group = vim.api.nvim_create_augroup("AutoReloadTerminalColors", {}),
    desc = "Set terminal colors",
    pattern = { "*" },
    callback = function() terminal_colors[vim.o.background]() end,
  })
end

local function promptsBorderless(hl, c, bg)
  local prompt = c.fg_gutter

  hl.FloatBorder = { fg = prompt, bg = prompt }
  hl.FloatTitle = { fg = c.fg, bg = prompt }
  hl.NormalFloat = { bg = c.bg_highlight }

  hl.TelescopeNormal = { bg = bg, fg = c.fg }
  hl.TelescopeBorder = { bg = bg, fg = bg }
  hl.TelescopePromptNormal = { bg = prompt }
  hl.TelescopePromptBorder = { bg = prompt, fg = prompt }
  hl.TelescopePromptTitle = { bg = prompt, fg = c.comment, bold = true }
  hl.TelescopePreviewTitle = { bg = c.bg_light, fg = c.comment }
  hl.TelescopeResultsTitle = { bg = bg }
  hl.TelescopeSelection = { bg = prompt }
  hl.TelescopePromptCounter = { bg = prompt, fg = c.blue }
  hl.TelescopeResultsNumber = { fg = c.teal }
  hl.TelescopePreviewLine = { bg = prompt }

  hl.TelescopeResultsDiffUntracked = { fg = c.git.text }
  hl.TelescopeResultsDiffDelete = { fg = c.git.add }
  hl.TelescopeResultsDiffAdd = { fg = c.git.add }
  hl.TelescopeResultsDiffChange = { fg = c.git.add }

  local noice_prompt = c.bg_highlight

  hl.NoiceCmdlinePopupTitleSearch = { fg = c.blue }
  hl.NoiceCmdlinePrompt = { bg = noice_prompt }
  hl.NoiceCmdlinePopup = { bg = noice_prompt }
  hl.NoiceCmdlinePopupBorder = { fg = noice_prompt, bg = noice_prompt }
  hl.NoiceCmdlinePopupBorderInput = { fg = noice_prompt, bg = noice_prompt }
  hl.NoiceCmdlineIcon = { fg = c.yellow }
  hl.NoiceCmdlinePopupTitle = { fg = c.fg }
  hl.NoiceCmdlineIconSearch = { fg = c.blue }
  hl.NoiceCmdlinePopupBorderSearch = { fg = noice_prompt, bg = noice_prompt }
  hl.NoiceCmdlinePopupTitleHelp = { fg = c.purple }
  hl.NoiceCmdlineIconHelp = { fg = c.purple }
  hl.NoiceCmdlinePopupTitleLua = { fg = c.orange }
  hl.NoiceCmdlineIconLua = { fg = c.orange }
  hl.NoiceMini = { bg = c.bg_highlight }

  hl.Noice = { bg = bg }
  hl.NoicePopup = { bg = bg }
  hl.NoiceSplit = { bg = "" }
  hl.NoicePopupMenuBorder = { fg = noice_prompt, bg = noice_prompt }
  hl.NoicePopupMenu = { bg = bg }
  hl.NoiceConfirm = { bg = bg }
  hl.NoiceConfirmBorder = { fg = noice_prompt, bg = noice_prompt }
  hl.NoiceSplitBorder = { fg = noice_prompt, bg = noice_prompt }
  hl.NoicePopupBorder = { fg = noice_prompt, bg = noice_prompt }
  hl.NoiceCmdlinePopupBorderLua = { fg = noice_prompt, bg = noice_prompt }
end

local function popupUI(hl, c, bg_popup)
  local active_button = c.blue
  local inactive_button = c.dark3

  hl.TroubleNormalNc = { bg = "none" }
  hl.TroubleNormal = { bg = "none" }

  -- Floats

  -- Lazy
  hl.LazyNormal = { bg = bg_popup }
  hl.LazyButtonActive = { fg = bg_popup, bg = active_button }
  hl.LazyButton = { fg = c.fg, bg = inactive_button }
  hl.LazyH1 = { fg = bg_popup, bg = active_button }

  -- Mason
  -- hl.MasonMutedBlockBold = { bg = inactive_button }
  hl.MasonNormal = { bg = bg_popup }
  hl.MasonMuted = { fg = active_button }
  hl.MasonHighlight = { fg = active_button }
  hl.MasonMutedBlock = { bg = inactive_button }
  hl.MasonHeader = { fg = active_button, bg = bg_popup }
  hl.MasonHeaderSecondary = { fg = c.fg, bg = active_button }
  hl.MasonHighlightBlock = { fg = c.fg, bg = active_button }

  -- Notify
  hl.NotifyBackground = { fg = bg_popup, bg = bg_popup }
  hl.NotifyTRACEBorder = { fg = bg_popup, bg = bg_popup }
  hl.NotifyDEBUGBorder = { fg = bg_popup, bg = bg_popup }
  hl.NotifyINFOBorder = { fg = bg_popup, bg = bg_popup }
  hl.NotifyWARNBorder = { fg = bg_popup, bg = bg_popup }
  hl.NotifyERRORBorder = { fg = bg_popup, bg = bg_popup }

  hl.NotifyTRACEBody = { bg = bg_popup }
  hl.NotifyDEBUGBody = { bg = bg_popup }
  hl.NotifyINFOBody = { bg = bg_popup }
  hl.NotifyWARNBody = { bg = bg_popup }
  hl.NotifyERRORBody = { bg = bg_popup }
  hl.MasonHighlightBlockBold = { fg = bg_popup, bg = active_button }

  hl.HarpoonInactive = { fg = bg_popup, bg = bg_popup }
  hl.HarpoonActive = { fg = bg_popup, bg = bg_popup }
  hl.HarpoonNumberActive = { fg = bg_popup, bg = bg_popup }
  hl.HarpoonNumberInactive = { fg = bg_popup, bg = bg_popup }

  hl.WhichKeyFloat = { bg = bg_popup }
  hl.WhichKey = { bg = bg_popup }
  hl.WhichKeyNormal = { bg = bg_popup }
end

local function beline(hl, c, bg_popup)
  local fg_nc = c.comment
  local bg_nc = bg_popup

  local sep_bg = bg_popup

  local modified = c.red
  local modified_nc = c.red1

  local search_fg = c.teal
  local search_fg_nc = c.comment

  local mode_fg = c.bg_dark
  local terminal = c.teal
  local help = c.purple
  local quickfix = c.orange

  local git_fg = c.blue5
  local git_fg_nc = c.blue
  local git_bg = hl.DiagnosticVirtualTextHint.bg

  local tline_bg = bg_popup
  local tline_sel_bg = c.bg_visual

  -- statuscolumn
  hl.BeNLineNr = { fg = c.blue, bg = bg_popup, bold = false }
  hl.BeNLineNrModified = { fg = c.blue, bg = "none", bold = false }

  -- tabline
  hl.TabLine = { fg = c.dark5, bg = bg_popup }
  hl.TabLineFill = { fg = "", bg = "" }
  hl.TabLineSel = { bg = c.blue7 }

  hl.BeTLine = { fg = c.dark5, bg = tline_bg }
  hl.BeTLineSel = { bg = tline_sel_bg }

  hl.BeTLineModified = { fg = modified, bg = tline_bg }
  hl.BeTLineModifiedSel = { fg = modified, bg = tline_sel_bg }

  hl.BeTLineDir = { fg = c.fg, bg = c.fg_gutter }
  hl.BeTLineDirSep = { fg = c.fg_gutter, bg = "none" }
  hl.BeTLineGitDir = { fg = git_fg, bg = c.fg_gutter }
  hl.BeTLineGitDirSep = { fg = c.fg_gutter, bg = "none" }
  hl.BeTLineIcon = { bg = bg_popup }
  hl.BeTLineIconSel = { bg = bg_popup }
  hl.BeTLineMargin = { bg = "none" }
  hl.BeTLineSeparator = { fg = tline_bg, bg = "none" }
  hl.BeTLineSeparatorSel = { fg = tline_sel_bg, bg = "none" }

  --- winbar
  hl.WinBar = { fg = c.dark3, bg = "none" }
  hl.WinBarNC = { fg = c.fg_gutter, bg = "none" }
  hl.DropBarFileName = { fg = c.dark5, bold = true }
  hl.DropBarFileNameNC = { fg = c.dark3, bold = true }
  hl.DropBarFileNameModified = { fg = modified }
  hl.DropBarFileNameModifiedNC = { fg = modified_nc }

  -- statusline
  hl.StatusLine = { bg = "none" }
  hl.StatusLineNC = { bg = "none" }

  hl.BeSLine = { fg = c.fg, bg = bg_popup }
  hl.BeSLineFile = { fg = c.fg, bg = bg_popup }
  hl.BeSLineFileMod = { fg = modified, bg = bg_popup }
  hl.BeSLineFolderGit = { fg = git_fg, bg = bg_popup }
  hl.BeSLineGitBranch = { fg = git_fg, bg = c.blue7 }
  hl.BeSLineGitBranchUntracked = { fg = c.orange, bg = c.diff.delete }
  hl.BeSLineFolder = { fg = c.fg, bg = bg_popup }
  hl.BeSLineFolderTerminal = { fg = terminal, bg = bg_popup }
  hl.BeSLineFolderHelp = { fg = c.magenta, bg = bg_popup }
  hl.BeSLineShowcmd = { fg = c.blue5, bg = bg_popup }
  hl.BeSLineFolderQuickfix = { fg = quickfix, bg = bg_popup }
  hl.BeSLineQuickfix = { fg = quickfix, bg = c.fg_gutter }
  hl.BeSLineSearch = { fg = search_fg, bg = bg_popup }
  hl.BeSLineMacro = { fg = c.orange, bg = bg_popup }

  hl.BeSLineDiagnosticError = { fg = c.red, bg = bg_popup }
  hl.BeSLineDiagnosticWarn = { fg = c.yellow, bg = bg_popup }
  hl.BeSLineDiagnosticInfo = { fg = c.green, bg = bg_popup }
  hl.BeSLineDiagnosticHint = { fg = c.blue, bg = bg_popup }

  hl.BeSLineLspIcon = { fg = c.blue5, bg = bg_popup }
  hl.BeSLineCopilotIcon = { fg = c.blue5, bg = bg_popup }

  hl.BeSLineModeLeader = { fg = mode_fg, bg = c.blue2, bold = true }
  hl.BeSLineModeCtrlLeader = { fg = mode_fg, bg = c.blue6, bold = true }
  hl.BesLineModeNormal = { fg = mode_fg, bg = c.blue, bold = true }
  hl.BeSLineModePending = { fg = mode_fg, bg = c.red, bold = true }
  hl.BeSLineModePendingSearch = { fg = mode_fg, bg = c.orange, bold = true }
  hl.BeSLineLft = { fg = mode_fg, bg = c.blue5, bold = true }
  hl.BeSLineModeVisual = { fg = mode_fg, bg = c.magenta, bold = true }
  hl.BeSLineModeVisualLine = { fg = mode_fg, bg = c.magenta, bold = true }
  hl.BeSLineModeVisualBlock = { fg = mode_fg, bg = c.magenta, bold = true }
  hl.BeSLineModeSelect = { fg = mode_fg, bg = c.purple, bold = true }
  hl.BeSLineModeSelectLine = { fg = mode_fg, bg = c.purple, bold = true }
  hl.BeSLineModeSelectBlock = { fg = mode_fg, bg = c.purple, bold = true }
  hl.BeSLineModeInsert = { fg = mode_fg, bg = c.green, bold = true }
  hl.BeSLineModeReplace = { fg = mode_fg, bg = c.green1, bold = true }
  hl.BeSLineModeVisualReplace = { fg = mode_fg, bg = c.green2, bold = true }
  hl.BeSLineModeCommand = { fg = mode_fg, bg = c.yellow, bold = true }
  hl.BeSLineModeMore = { fg = mode_fg, bg = c.bg, bold = true }
  hl.BeSLineModeConfirm = { fg = mode_fg, bg = c.bg, bold = true }
  hl.BeSLineModeShell = { fg = mode_fg, bg = c.orange, bold = true }
  hl.BeSLineModeTerminal = { fg = mode_fg, bg = terminal, bold = true }
  hl.BeSLineModeGit = { fg = mode_fg, bg = git_fg, bold = true }

  hl.BeSLineGitBranchSep = { fg = c.blue7, bg = bg_popup }
  hl.BeSLineGitBranchUntrackedSep = { fg = c.diff.delete, bg = bg_popup }
  hl.BeSLineModeLeaderSep = { fg = c.blue2, bg = sep_bg }
  hl.BeSLineModeCtrlLeaderSep = { fg = c.blue6, bg = sep_bg }
  hl.BeSLineModeNormalSep = { fg = c.blue, bg = sep_bg }
  hl.BeSLineModePendingSep = { fg = c.red, bg = sep_bg }
  hl.BeSLineModePendingSearchSep = { fg = c.orange, bg = sep_bg }
  hl.BeSLineLftSep = { fg = c.blue5, bg = sep_bg }
  hl.BeSLineModeVisualSep = { fg = c.magenta, bg = sep_bg }
  hl.BeSLineModeVisualLineSep = { fg = c.magenta, bg = sep_bg }
  hl.BeSLineModeVisualBlockSep = { fg = c.magenta, bg = sep_bg }
  hl.BeSLineModeSelectSep = { fg = c.purple, bg = sep_bg }
  hl.BeSLineModeSelectLineSep = { fg = c.purple, bg = sep_bg }
  hl.BeSLineModeSelectBlockSep = { fg = c.purple, bg = sep_bg }
  hl.BeSLineModeInsertSep = { fg = c.green, bg = sep_bg }
  hl.BeSLineModeReplaceSep = { fg = c.green1, bg = sep_bg }
  hl.BeSLineModeVisualReplaceSep = { fg = c.green2, bg = sep_bg }
  hl.BeSLineModeCommandSep = { fg = c.yellow, bg = sep_bg }
  hl.BeSLineModeMoreSep = { fg = c.bg, bg = sep_bg }
  hl.BeSLineModeConfirmSep = { fg = c.bg, bg = sep_bg }
  hl.BeSLineModeShellSep = { fg = terminal, bg = sep_bg }
  hl.BeSLineModeTerminalSep = { fg = terminal, bg = sep_bg }
  hl.BeSLineFolderTerminalSep = { fg = bg_popup, bg = terminal }
  hl.BeSLineFolderHelpSep = { fg = bg_popup, bg = help }
  hl.BeSLineShowcmdSep = { fg = bg_popup, bg = help }
  hl.BeSLineFolderQuickfixSep = { fg = bg_popup, bg = quickfix }
  hl.BeSLineQuickfixSep = { fg = c.fg_gutter, bg = sep_bg }
  hl.BeSLineModeGitSep = { fg = git_fg, bg = sep_bg }

  hl.BeSLineNC = { fg = fg_nc, bg = bg_nc }
  hl.BeSLineFileNC = { fg = fg_nc, bg = bg_nc }
  hl.BeSLineFileModNC = { fg = modified_nc, bg = bg_nc }
  hl.BeSLineFolderGitNC = { fg = git_fg_nc, bg = bg_nc }
  hl.BeSLineGitBranchNC = { fg = fg_nc, bg = bg_nc }
  hl.BeSLineGitBranchUntrackedNC = { fg = fg_nc, bg = bg_nc }
  hl.BeSLineFolderNC = { fg = fg_nc, bg = bg_nc }

  hl.BeSLineDiagnosticErrorNC = { fg = fg_nc, bg = bg_nc }
  hl.BeSLineDiagnosticWarnNC = { fg = fg_nc, bg = bg_nc }
  hl.BeSLineDiagnosticInfoNC = { fg = fg_nc, bg = bg_nc }
  hl.BeSLineDiagnosticHintNC = { fg = fg_nc, bg = bg_nc }
  hl.BeSLineModeTerminalNC = { fg = fg_nc, bg = sep_bg }
  hl.BeSLineFolderTerminalNC = { fg = fg_nc, bg = bg_nc }
  hl.BeSLineFolderHelpNC = { fg = fg_nc, bg = bg_nc }
  hl.BeSLineShowcmdNc = { fg = fg_nc, bg = bg_nc }
  hl.BeSLineFolderQuickfixNC = { fg = fg_nc, bg = bg_nc }
  hl.BeSLineQuickfixNC = { fg = quickfix, bg = bg_nc }
  hl.BeSLineSearchNC = { fg = search_fg_nc, bg = bg_popup }
  hl.BeSLineMacroNC = { fg = search_fg_nc, bg = bg_nc }

  hl.BeSLineLspIconNC = { fg = c.blue7, bg = bg_nc }
  hl.BeSLineCopilotIconNC = { fg = c.blue7, bg = bg_nc }

  local mode_fg_nc = bg_popup
  local mode_bg_nc = bg_popup
  hl.BeSLineModeNormalNC = { fg = c.red1, bg = mode_bg_nc }
  hl.BeSLineModePendingNc = { fg = c.red1, bg = mode_bg_nc }
  hl.BeSLineModeVisualNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeVisualLineNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeVisualBlockNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeSelectNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeSelectLineNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeSelectBlockNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeInsertNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeReplaceNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeVisualReplaceNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeCommandNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeMoreNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeConfirmNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeShellNC = { fg = mode_fg_nc, bg = mode_bg_nc }
  hl.BeSLineModeTerminalNC = { fg = mode_fg_nc, bg = mode_bg_nc }

  local sep_fg_nc = bg_popup
  local sep_bg_nc = bg_popup
  hl.BeSLineModeNormalSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModePendingSepNc = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineLftSepNc = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeVisualSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeVisualLineSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeVisualBlockSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeSelectSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeSelectLineSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeSelectBlockSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineInsertSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeReplaceSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeVisualReplaceSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeCommandSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeMoreSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeConfirmSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeShellSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineModeTerminalSepNC = { fg = sep_fg_nc, bg = sep_bg_nc }
  hl.BeSLineGitBranchSepNC = { fg = sep_bg_nc, bg = sep_fg_nc }
  hl.BeSLineGitBranchUntrackedSepNC = { fg = sep_bg_nc, bg = sep_fg_nc }
  hl.BeSLineQuickfixSepNC = { fg = sep_bg_nc, bg = sep_fg_nc }
end

local function markdown(hl, c, bg_popup)
  hl["@markup.heading.1.markdown"] = { link = "DiagnosticVirtualTextError" }
  hl["@markup.heading.2.markdown"] = { link = "DiagnosticVirtualTextInfo" }
  hl["@markup.heading.3.markdown"] = { link = "DiagnosticVirtualTextHint" }
  hl["@markup.heading.4.markdown"] = { link = "DiagnosticVirtualTextWarn" }
  hl["@markup.heading.5.markdown"] = { link = "DiagnosticVirtualTextOk" }
  -- hl["@markup.heading.6.markdown"] = { link = "DiagnosticVirtualTextError" }

  hl.RenderMarkdownBullet = { fg = c.green1 }
  hl.RenderMarkdownChecked = { fg = c.blue2 }
  hl.RenderMarkdownCode = { bg = bg_popup }
  -- hl.RenderMarkdownDash = {}
  -- hl.RenderMarkdownError = {}
  hl.RenderMarkdownH1 = { link = "DiagnosticVirtualTextError" }
  hl.RenderMarkdownH1Bg = { link = "DiagnosticVirtualTextError" }
  hl.RenderMarkdownH2 = { link = "DiagnosticVirtualTextInfo" }
  hl.RenderMarkdownH2Bg = { link = "DiagnosticVirtualTextInfo" }
  hl.RenderMarkdownH3 = { link = "DiagnosticVirtualTextHint" }
  hl.RenderMarkdownH3Bg = { link = "DiagnosticVirtualTextHint" }
  hl.RenderMarkdownH4 = { link = "DiagnosticVirtualTextWarn" }
  hl.RenderMarkdownH4Bg = { link = "DiagnosticVirtualTextWarn" }
  hl.RenderMarkdownH5 = { link = "DiagnosticVirtualTextOk" }
  hl.RenderMarkdownH5Bg = { link = "DiagnosticVirtualTextOk" }
  -- hl.RenderMarkdownH6 = {}
  -- hl.RenderMarkdownH6Bg = {}
  -- hl.RenderMarkdownHint = {}
  -- hl.RenderMarkdownInfo = {}
  -- hl.RenderMarkdownLink = {}
  -- hl.RenderMarkdownMath = {}
  hl.RenderMarkdownQuote = { fg = c.blue6 }
  -- hl.RenderMarkdownSign = {}
  -- hl.RenderMarkdownSuccess = {}
  -- hl.RenderMarkdownTableFill = {}
  -- hl.RenderMarkdownTableHead = {}
  -- hl.RenderMarkdownTableRow = {}
  -- hl.RenderMarkdownTodo = {}
  -- hl.RenderMarkdownUnchecked = {}
  -- hl.RenderMarkdownWarn = {}
end

local function various(hl, c, bg_popup)
  hl.Visual = { bg = c.bg_visual }

  hl.MatchParen = { bold = true }
  hl.SignColumn = {}

  hl.TelescopeResultsNumber = { fg = c.teal }

  hl.Search = { link = "DiagnosticVirtualTextInfo" }
  hl.IncSearch = { link = "DiagnosticVirtualTextWarn" }
  hl.DiagnosticUnnecessary = { fg = c.fg_dark }

  hl.Folded = { link = "DiagnosticVirtualTextInfo" }

  hl.CmpDocumentation = { fg = c.fg, bg = bg_popup }
  local prompt = c.fg_gutter
  hl.Pmenu = { bg = bg_popup }
  hl.PmenuSel = { bg = prompt }
  hl.PmenuSbar = { bg = prompt }
  hl.PmenuThumb = { bg = bg_popup }
  -- hl.BlinkCmpMenu = { bg = bg_popup }
  hl.BlinkCmpMenu = { bg = bg_popup }
  hl.BlinkCmpDoc = { fg = c.fg, bg = bg_popup }
  hl.BlinkCmpDocBorder = { fg = c.fg, bg = bg_popup }

  hl.helpSectionDelim = { link = "RenderMarkdownH2" }
  hl.helpCommand = { fg = c.fg_dark }

  hl.IlluminatedWord = { bg = bg_popup }
  hl.IlluminatedWordText = { bg = bg_popup }
  hl.IlluminatedWordRead = { bg = bg_popup }
  hl.IlluminatedWordWrite = { bg = bg_popup }
  hl.IlluminatedCurWord = { bg = bg_popup }

  hl.LspReferenceTarget = { bg = bg_popup }
  hl.LspReferenceText = { bg = bg_popup }
  hl.LspReferenceRead = { bg = bg_popup }
  hl.LspReferenceWrite = { bg = bg_popup }

  hl.NvimTreeIndentMarker = { fg = c.blue1 }

  hl.CopilotSuggestion = { fg = c.fg_dark }

  hl.FlashLabel = { fg = c.bg, bg = c.red }
  hl.FlashPrompt = { bg = bg_popup }
end

local function snacks(hl, c, bg_popup)
  -- hl.snacksnotifier = { fg = "", bg = bg_popup }

  local hl_info = hl.DiagnosticVirtualTextInfo
  hl.SnacksNotifierInfo = { fg = hl_info.fg, bg = hl_info.bg }
  hl.SnacksNotifierBorderInfo = { fg = hl_info.bg, bg = hl_info.bg }
  hl.SnacksNotifierTitleInfo = { fg = hl_info.fg, bg = hl_info.bg }
  hl.SnacksNotifierIconInfo = { fg = hl_info.fg, bg = hl_info.bg }
  hl.SnacksNotifierFooterInfo = { fg = hl_info.fg, bg = hl_info.bg }

  local hl_error = hl.DiagnosticVirtualTextError
  hl.SnacksNotifierError = { fg = hl_error.fg, bg = hl_error.bg }
  hl.SnacksNotifierBorderError = { fg = hl_error.bg, bg = hl_error.bg }
  hl.SnacksNotifierTitleError = { fg = hl_error.fg, bg = hl_error.bg }
  hl.SnacksNotifierIconError = { fg = hl_error.fg, bg = hl_error.bg }
  hl.SnacksNotifierFooterError = { fg = hl_error.fg, bg = hl_error.bg }

  local hl_warn = hl.DiagnosticVirtualTextWarn
  hl.SnacksNotifierWarn = { fg = hl_warn.fg, bg = hl_warn.bg }
  hl.SnacksNotifierBorderWarn = { fg = hl_warn.bg, bg = hl_warn.bg }
  hl.SnacksNotifierTitleWarn = { fg = hl_warn.fg, bg = hl_warn.bg }
  hl.SnacksNotifierIconWarn = { fg = hl_warn.fg, bg = hl_warn.bg }
  hl.SnacksNotifierFooterWarn = { fg = hl_warn.fg, bg = hl_warn.bg }

  local hl_debug = hl.DiagnosticVirtualTextWarn
  hl.SnacksNotifierDebug = { fg = hl_debug.fg, bg = hl_debug.bg }
  hl.SnacksNotifierBorderDebug = { fg = hl_info.bg, bg = hl_info.bg }
  hl.SnacksNotifierTitleDebug = { fg = hl_debug.fg, bg = hl_debug.bg }
  hl.SnacksNotifierIconDebug = { fg = hl_debug.fg, bg = hl_debug.bg }
  hl.SnacksNotifierFooterDebug = { fg = hl_debug.fg, bg = hl_debug.bg }

  local hl_trace = hl.DiagnosticVirtualTextWarn
  hl.SnacksNotifierTrace = { fg = hl_trace.fg, bg = hl_trace.bg }
  hl.SnacksNotifierBorderTrace = { fg = hl_trace.bg, bg = hl_info.bg }
  hl.SnacksNotifierTitleTrace = { fg = hl_trace.fg, bg = hl_trace.bg }
  hl.SnacksNotifierIconTrace = { fg = hl_trace.fg, bg = hl_trace.bg }
  hl.SnacksNotifierFooterTrace = { fg = hl_trace.fg, bg = hl_trace.bg }

  local hl_history = hl.DiagnosticVirtualTextInfo
  hl.SnacksNotifierHistory = { fg = hl_history.fg, bg = hl_history.bg }
  hl.SnacksNotifierHistoryTitle = { fg = hl_history.fg, bg = hl_history.bg }
  hl.SnacksNotifierHistoryDateTime = { fg = hl_history.fg, bg = hl_history.bg }

  hl.SnacksIndentScope = { fg = bg_popup, bold = true }
  hl.SnacksIndent = { fg = bg_popup }
  hl.SnacksDashboardKey = { fg = c.blue }
  hl.SnacksDashboardDesc = { fg = c.fg }
  hl.SnacksDashboardIcon = { fg = c.comment }
  hl.SnacksDashboardHeader = { fg = c.magenta }
  hl.SnacksDashboardFooter = { fg = c.comment }
  hl.SnacksDashboardSpecial = { fg = c.fg }
end

local function transparentUi(hl, c)
  hl.Normal = { fg = c.fg, bg = "none" }
  hl.NeoTreeNormal = { fg = c.fg, bg = "none" }
  hl.NeoTreeNormalNC = { fg = c.fg, bg = "none" }
  hl.NormalNC = { fg = c.fg, bg = "none" }
end

local function config()
  require("tokyonight").setup({
    style = "moon",
    styles = {
      sidebars = "",
      keywords = { italic = false },
    },
    terminal_colors = false,
    dim_inactive = false,
    lualine_bold = true,
    on_colors = function(c)
      c.bg_statusline = c.bg_highlight
      c.border = c.bg_highlight
    end,
    on_highlights = function(hl, c)
      -- c.blue = "#bdd2ff"
      -- c.blue0 = "#6f8ee1"
      -- c.blue1 = "#a0d6ff"
      -- c.blue2 = "#2dd5f2"
      -- c.blue5 = "#89ddff"
      -- c.blue6 = "#c4eeff"
      -- c.green = "#ddf2be"
      -- c.green1 = "#9dcab9"
      -- c.green2 = "#68bcc9"
      -- c.magenta = "#e4d4ff"
      -- c.magenta2 = "#ff62ae"
      -- c.orange = "#ffc0a7"
      -- c.purple = "#fee0f8"
      -- c.red = "#ffb0b6"
      -- c.red1 = "#d2687b"
      -- c.teal = "#7fe1d0"
      -- c.yellow = "#ffdfb2"
      if not vim.g.neovide then transparentUi(hl, c) end
      local bg_popup = c.bg_highlight
      promptsBorderless(hl, c, bg_popup)
      popupUI(hl, c, bg_popup)
      beline(hl, c, bg_popup)
      various(hl, c, bg_popup)
      snacks(hl, c, bg_popup)
      markdown(hl, c, bg_popup)
    end,
  })
  auto_reload_term_colors()
  vim.cmd("colorscheme tokyonight")
end

return {
  "folke/tokyonight.nvim",
  lazy = false,
  priority = 1000,
  config = config,
}
