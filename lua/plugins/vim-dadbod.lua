return {
  {
    "tpope/vim-dadbod",
    cmd = "DB",
  },
  {
    "kristijanhusak/vim-dadbod-ui",
    dependencies = { "tpope/vim-dadbod" },
    cmd = {
      "DBUI",
      "DBUIToggle",
      "DBUIAddConnection",
      "DBUIFindBuffer",
      "DBUIRenameBuffer",
      "DBUILastQueryInfo",
      "DBUIHideNotifications",
    },
    init = function()
      vim.g.db_ui_use_nerd_fonts = 1
      vim.g.db_ui_use_nvim_notify = 1
      vim.g.db_ui_hide_schemas = { "pg_catalog", "pg_toast_temp.*" }
      vim.g.db_ui_show_help = 0
      vim.g.db_ui_save_location = vim.fn.stdpath("data") .. "/db_ui"
      vim.g.db_ui_tmp_query_location = vim.fn.stdpath("data") .. "/db_ui_queries"
      vim.g.db_ui_icons = {
        expanded = {
          db = " ",
          buffers = " ", -- 
          saved_queries = " ",
          schemas = " ",
          schema = " ",
          tables = " ",
          table = " ",
        },
        collapsed = {
          db = " ",
          buffers = " ",
          saved_queries = " ",
          schemas = " ",
          schema = " ",
          tables = " ",
          table = " ",
        },
        new_query = " ",
        saved_query = "",
        tables = "",
        buffers = "",
        add_connection = "",
        connection_ok = "",
        connection_error = "",
      }
    end,
  },
  {
    "kristijanhusak/vim-dadbod-completion",
    dependencies = {
      "tpope/vim-dadbod",
      "kristijanhusak/vim-dadbod-ui",
    },
    ft = { "sql", "mysql", "plsql" },
  },
}
