local function getVisualSelection()
  vim.cmd('noau normal! "vy"')
  local text = vim.fn.getreg("v")
  return string.gsub(text, "\n", "")
end

return {
  "nvim-telescope/telescope.nvim",
  dependencies = {
    {
      "nvim-telescope/telescope-fzf-native.nvim",
      build = "make",
    },
    "nvim-lua/plenary.nvim",
    "nvim-tree/nvim-web-devicons",
    "nvim-telescope/telescope-file-browser.nvim",
    "nvim-telescope/telescope-ui-select.nvim",
    "debugloop/telescope-undo.nvim",
  },
  cmd = "Telescope",
  keys = {
    { "<leader>fe", "<cmd>Telescope file_browser<cr>", desc = "Telescope file browser" },
    { "<leader>F", require("core.telescope.finders").root, desc = "Telescope /" },
    { "<leader><c-f>", require("core.telescope.finders").curdir, desc = "Telescope ./" },
    { "<leader>ff", require("core.telescope.finders").home, desc = "Telescope $HOME" },
    { "<leader>f.", require("core.telescope.finders").dot, desc = "Telescope working dir" },
    { "<leader>fw", require("core.telescope.finders").work, desc = "Telescope work" },
    { "<leader>fi", require("core.telescope.finders").nvim, desc = "Telescope neovim config" },
    { "<leader>fn", require("core.telescope.finders").notes, desc = "Telescope notes" },
    { "<leader>fb", "<cmd>Telescope buffers sort_mru=true<cr>", desc = "Telescope buffers" },
    { "<leader>j", "<cmd>Telescope buffers sort_mru=true<cr>", desc = "Telescope buffers" },
    { "<leader>ft", "<cmd>Telescope tags<cr>", desc = "Telescope tags" },
    { "<leader>f<c-t>", "<cmd>Telescope current_buffer_tags<cr>", desc = "Telescope buffer tags" },
    { "<leader>fl", "<cmd>Telescope current_buffer_fuzzy_find<cr>", desc = "Telescope buffer fuzzy" },
    { "<leader>f:", "<cmd>Telescope command_history<cr>", desc = "Telescope command history" },
    { "<leader>;", "<cmd>Telescope command_history<cr>", desc = "Telescope command history" },
    { "<leader>f;", "<cmd>Telescope commands<cr>", desc = "Telescope commands" },
    { "<leader>f/", "<cmd>Telescope search_history<cr>", desc = "Telescope search history" },
    { "<leader>fh", "<cmd>Telescope oldfiles<cr>", desc = "Telescope old files" },
    { "<leader>fm", "<cmd>Telescope help_tags<cr>", desc = "Telescope help tags" },
    { "<leader>fq", "<cmd>Telescope quickfix<cr>", desc = "Telescope quickfix" },
    { "<leader>fQ", "<cmd>Telescope quickfixhistory<cr>", desc = "Telescope qf history" },
    { "<leader>f'", "<cmd>Telescope marks<cr>", desc = "Telescope marks" },
    { '<leader>f"', "<cmd>Telescope registers<cr>", desc = "Telescope registers" },
    { "<leader>fk", "<cmd>Telescope keymaps<cr>", desc = "Telescope keymaps" },
    { "<leader>f<space>", "<cmd>Telescope resume<cr>", desc = "Telescope resume" },
    { "<leader>fc", "<cmd>Telescope colorscheme<cr>", desc = "Telescope colorscheme" },

    { "<leader>gs", "<cmd>Telescope git_status<cr>", desc = "Telescope git status" },
    { "<leader>gb", "<cmd>Telescope git_branches<cr>", desc = "Telescope git branches" },
    { "<leader>gf", "<cmd>Telescope git_files<cr>", desc = "Telescope git files" },
    { "<leader>gc", "<cmd>Telescope git_commits<cr>", desc = "Telescope git commits" },
    { "<leader>gC", "<cmd>Telescope git_bcommits<cr>", desc = "Telescope buffer commits" },
    {
      "<leader>fr",
      function() require("telescope.builtin").live_grep({ default_text = getVisualSelection() }) end,
      mode = "v",
      desc = "Telescope current selection",
    },
    {
      "<leader>f<c-r>",
      function() require("telescope.builtin").live_grep({ default_text = vim.fn.expand("<cword>") }) end,
      mode = "n",
      desc = "Telescope grep current word",
    },
    { "<leader>fr", "<cmd>Telescope live_grep<cr>", desc = "Telescope grep" },
    { "<leader>fu", "<cmd>Telescope undo<cr>", desc = "Telescope undo" },
    { "<leader>ca", desc = "Code action", vim.lsp.buf.code_action, { mode = "n", "v" } },
    {
      "<leader>fP",
      function()
        require("telescope.builtin").find_files({
          prompt_title = "plugins",
          cwd = vim.fs.joinpath(vim.fn.stdpath("data") .. "/lazy"),
        })
      end,
      desc = "find plugin files",
    },
  },

  opts = function()
    require("core.telescope.layouts.prompt_center")
    local telescope = require("telescope")
    local actions = require("telescope.actions")
    local themes = require("telescope.themes")
    local feed_keycode = require("core.conflib").feed_keycode

    require("telescope").setup({
      defaults = {
        borderchars = { "", "", "", "", "", "", "", "" },
        preview = {
          hide_on_startup = true,
        },
        result_title = false,
        layout_strategy = "prompt_center",
        -- theme = "dropdown",
        sorting_strategy = "ascending",
        layout_config = {
          vertical = {
            prompt_position = "top",
            -- mirror = true,
            preview_cutoff = 1, -- Preview should always show (unless previewer = false)
            -- preview_height = 30,
            width = function(_, max_columns, _) return math.min(max_columns - 4, 80) end,
            height = function(_, _, max_lines) return math.min(max_lines - 4, 25) end,
            preview_height = function(_, _, max_lines) return math.floor(math.min(max_lines - 4, 25) / 2) - 2 end,
          },
          prompt_center = {
            prompt_position = "top",
            prompt_height = 1,
            width = function(_, max_columns, _) return math.min(max_columns - 4, 80) end,
            -- results_height = function(_, _, max_lines) return math.min(max_lines - 4, 16) end,
            preview_cutoff = 1, -- Preview should always show (unless previewer = false)
          },
        },
        path_display = { "truncate " },
        --  
        selection_caret = " ",
        multi_icon = " ",
        entry_prefix = "  ",
        --  
        prompt_prefix = " ",
        wrap_results = false,
        mappings = {
          i = {
            ["<C-k>"] = actions.move_selection_previous,
            ["<C-j>"] = actions.move_selection_next,
            ["<C-Space>"] = actions.toggle_selection,
            ["<C-q>"] = actions.smart_send_to_qflist + actions.open_qflist,
            ["<c-s-q>"] = require("trouble.sources.telescope").open,
            ["<c-f>"] = feed_keycode("<Right>"),
            ["<c-b>"] = feed_keycode("<Left>"),
            ["<c-]>"] = require("telescope.actions.layout").toggle_preview,
            ["<Esc>"] = actions.close,
          },
          -- n = { ["<c-t>"] = trouble.open_with_trouble },
        },
        cmd = { "Telescope" },
      },
      extensions = {
        ["fzf"] = {
          fuzzy = true, -- false will only do exact matching
          override_generic_sorter = true, -- override the generic sorter
          override_file_sorter = true, -- override the file sorter
          case_mode = "smart_case", -- or "ignore_case" or "respect_case"
          -- the default case_mode is "smart_case"
        },
        ["ui-select"] = {
          themes.get_cursor({
            layout_config = {
              prompt_position = "top",
              width = 70,
              height = 12,
            },
          }),
          specific_opts = {
            ["rename"] = {
              themes.get_cursor({
                layout_config = {
                  width = 20,
                  height = 1,
                },
              }),
            },
          },
        },
        ["live_grep"] = {
          themes.get_dropdown({}),
        },
        ["file_browser"] = {
          themes.get_dropdown({}),
        },
        ["undo"] = {
          themes.get_dropdown({}),
          use_delta = true,
          use_custom_command = nil, -- setting this implies `use_delta = false`. Accepted format is: { "bash", "-c", "echo '$DIFF' | delta" }
          side_by_side = true,
          vim_diff_opts = vim.o.scrolloff,
          entry_format = "state #$ID, $STAT, $TIME",
          mappings = {
            i = {
              -- IMPORTANT: Note that telescope-undo must be available when telescope is configured if
              -- you want to replicate these defaults and use the following actions. This means
              -- installing as a dependency of telescope in it's `requirements` and loading this
              -- extension from there instead of having the separate plugin definition as outlined
              -- above.
              ["<cr>"] = require("telescope-undo.actions").yank_additions,
              ["<S-cr>"] = require("telescope-undo.actions").yank_deletions,
              ["<C-cr>"] = require("telescope-undo.actions").restore,
            },
          },
        },
      },
    })
    local telescope = require("telescope")
    local extensions = { "file_browser", "undo", "fzf", "harpoon", "ui-select", "noice" }
    for _, ext in pairs(extensions) do
      telescope.load_extension(ext)
    end
  end,
}
