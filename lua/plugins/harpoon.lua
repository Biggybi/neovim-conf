return {
  "ThePrimeagen/harpoon",
  dependencies = { "nvim-lua/plenary.nvim", "nvim-telescope/telescope.nvim" },
  keys = {
    {
      "<leader>hh",
      function() require("telescope").extensions.harpoon.marks() end,
      desc = "Harpoon marks",
    },
    { "<leader>hm", function() require("harpoon.mark").add_file() end, desc = "Harpoon mark file" },
    { "<leader>hj", function() require("harpoon.ui").nav_next() end, desc = "Harpoon next mark" },
    { "<leader>hk", function() require("harpoon.ui").nav_prev() end, desc = "Harpoon prev mark" },
    { "<leader>ht", function() require("harpoon.term").gotoTerminal(1) end, desc = "Harpoon terminal 1" },
    { "<leader>hx", function() require("harpoon.tmux").gotoTerminal(1) end, desc = "Harpoon tmux 1" },
  },
  opts = {},
}
