return {
  "folke/persistence.nvim",
  -- event = "BufReadPre", -- only start saving if file
  keys = {
    {
      "<leader>ss",
      function() require("persistence").load() end,
      desc = "Restore current session",
    },
    {
      "<leader>sl",
      function() require("persistence").load({ last = true }) end,
      desc = "Restore last session",
    },
    {
      "<leader>sp",
      function() require("persistence").stop() end,
      desc = "Stop Persistence",
    },
  },
  opts = {},
}
