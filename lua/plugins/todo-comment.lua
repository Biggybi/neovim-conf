local prefix = "<leader>t"
return {
  "folke/todo-comments.nvim",
  dependencies = { "nvim-lua/plenary.nvim" },
  event = { "BufRead", "BufNewFile" },
  cmd = { "TodoTelescope", "TodoQuickFix", "TodoLocList", "TodoTrouble" },
  keys = {
    { prefix, "<Nop>", desc = "Todo" },
    {
      prefix .. "q",
      "<cmd>TodoQuickFix<CR>",
      desc = "Todo quickfix",
    },
    {
      prefix .. "l",
      "<cmd>TodoLocList<CR>",
      desc = "Todo location",
    },
    {
      prefix .. "d",
      "<cmd>TodoTrouble<CR>",
      desc = "Todo trouble",
    },
    {
      prefix .. "t",
      "<cmd>TodoTelescope<CR>",
      desc = "Todo find",
    },
    {
      prefix .. ".",
      "<cmd>TodoTelescope cwd=~/dotfiles/<CR>",
      desc = "Todo find (dotfiles)",
    },
    {
      prefix .. "n",
      "<cmd>TodoTelescope cwd=~/.config/nvim<CR>",
      desc = "Todo find (neovim config)",
    },
    {
      prefix .. "w",
      "<cmd>TodoTelescope cwd=~/work/<CR>",
      desc = "Todo find (work dir)",
    },
    {
      "]<c-t>",
      function() require("todo-comments").jump_next() end,
      desc = "Todo next comment",
    },
    {
      "[<c-t>",
      function() require("todo-comments").jump_prev() end,
      desc = "Todo previous comment",
    },
  },
  opts = {
    gui_style = {
      fg = "NONE",
      bg = "NONE",
    },
    keywords = {
      FIX = {
        icon = " ",
        color = "error",
        alt = { "FIXME", "BUG", "FIXIT", "ISSUE" },
      },
      TODO = { icon = " ", color = "info" },
      HACK = { icon = " ", color = "warning", alt = { "WORKAROUND" } },
      WARN = { icon = " ", color = "warning", alt = { "WARNING", "XXX" } },
      PERF = { icon = " ", alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
      NOTE = { icon = " ", color = "hint", alt = { "INFO" } },
      TEST = { icon = " ", color = "test", alt = { "TESTING", "PASSED", "FAILED" } },
    },
    highlight = {
      multiline = false,
    },
  },
}
