return {
  "mrcjkb/rustaceanvim",
  version = "^5", -- Recommended
  -- lazy = false, -- This plugin is already lazy
  ft = "rust",
  cmd = { "RustAnalyzer", "Rustc", "RustLsp" },
  -- ["rust-analyzer"] = {
  --   cargo = {
  --     allFeatures = true,
  --   },
  -- },
  config = function()
    -- codelldb setup
    local mason_registry = require("mason-registry")
    local codelldb_extension_path = mason_registry.get_package("codelldb"):get_install_path() .. "/extension"
    local codelldb_path = codelldb_extension_path .. "/adapter/codelldb"
    local liblldb_path = codelldb_extension_path .. "/lldb/lib/liblldb.dylib"
    local cfg = require("rustaceanvim.config")

    vim.g.rustaceanvim = {
      dap = {
        adapter = cfg.get_codelldb_adapter(codelldb_path, liblldb_path),
      },
    }
  end,
}
