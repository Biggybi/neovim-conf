return {
  "williamboman/mason.nvim",
  cmd = { "Mason", "MasonUpdate", "MasonInstall", "MasonUninstall", "MasonUninstallAll", "MasonLolg" },
  build = ":MasonUpdate",
  keys = { { "<leader>em", "<cmd>Mason<cr>", desc = "Mason" } },
  opts = {
    automatic_installation = true,
    ensure_installed = {
      "rust-analyzer",
      "ts_ls",
      "html",
      "cssls",
      "tailwindcss",
      "svelte",
      "lua_ls",
      "graphql",
      "emmet_ls",
      "prismals",
      "pyright",
    },
    ui = {
      icons = {
        package_installed = "●",
        package_pending = "◍",
        package_uninstalled = "○",
      },
    },
  },
}
