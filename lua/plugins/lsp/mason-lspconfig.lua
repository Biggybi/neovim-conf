return {
  "williamboman/mason-lspconfig.nvim",
  dependencies = { "williamboman/mason.nvim", "saghen/blink.cmp" },
  lazy = true,
  opts = {
    handlers = {
      function(server_name)
        require("lspconfig")[server_name].setup({
          capabilities = require("blink.cmp").get_lsp_capabilities(),
        })
      end,
      ["ts_ls"] = function() end, -- handled by `typescript_tools.lua`
      ["rust_analyzer"] = function() end, -- handled by `rustaceanvim.lua`
      ["typos_lsp"] = function()
        require("lspconfig").typos_lsp.setup({
          init_options = {
            diagnosticSeverity = "Hint",
            config = "~/.config/typos-lsp/typos.toml",
          },
        })
      end,
    },
  },
}
