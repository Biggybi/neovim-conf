return {
  {
    "nvim-treesitter/nvim-treesitter",
    event = { "BufReadPre", "BufNewFile" },
    build = ":TSUpdate",
    dependencies = {
      "nvim-treesitter/nvim-treesitter-textobjects",
    },
    cmd = {
      "TSInstall",
      "TSInstallSync",
      "TSInstallInfo",
      "TSUpdate",
      "TSUpdateSync",
      "TSUninstall",
      "TSBufEnable",
      "TSBufDisable",
      "TSBufToggle",
      "TSEnable",
      "TSDisable",
      "TSToggle",
      "TSModuleInfo",
      "TSEditQuery",
      "TSEditQueryUserAfter",
    },
    keys = {
      { "[v", desc = "Increment Selection" },
      { "]v", desc = "Decrement Selection", mode = "x" },
    },
    opts_extend = { "ensure_installed" },
    ---@type TSConfig
    ---@diagnostic disable-next-line: missing-fields
    opts = {
      highlight = { enable = true },
      indent = { enable = true },
      ensure_installed = {
        "c",
        "cpp",
        "cmake",
        "diff",
        "make",
        "json",
        "javascript",
        "typescript",
        "tsx",
        "python",
        "rust",
        "yaml",
        "html",
        "css",
        "prisma",
        "markdown",
        "markdown_inline",
        "svelte",
        "graphql",
        "bash",
        "lua",
        "vim",
        "dockerfile",
        "gitcommit",
        "git_config",
        "git_rebase",
        "gitignore",
        "gitattributes",
      },
      -- auto install above language parsers
      auto_install = true,
      incremental_selection = {
        enable = true,
        keymaps = {
          init_selection = "[v",
          node_incremental = "[v",
          scope_incremental = "[V",
          node_decremental = "]v",
        },
      },
      -- enable nvim-ts-context-commentstring plugin for commenting tsx and jsx
      context_commentstring = {
        enable = true,
        enable_autocmd = false,
      },
      textobjects = {
        move = {
          enable = true,
          set_jumps = true, -- whether to set jumps in the jumplist
          goto_next_start = {
            ["]f"] = "@function.outer",
            ["]c"] = "@comment.outer",
            ["]b"] = "@block.outer",
            ["]k"] = "@class.outer",
            ["]a"] = "@parameter.outer",
            ["]i"] = "@conditional.outer",
          },
          goto_next_end = {
            ["]F"] = "@function.outer",
            ["]K"] = "@class.outer",
            ["]B"] = "@block.outer",
            ["]A"] = "@parameter.outer",
            ["]C"] = "@comment.outer",
            ["]I"] = "@conditional.outer",
          },
          goto_previous_start = {
            ["[f"] = "@function.outer",
            ["[c"] = "@comment.outer",
            ["[b"] = "@block.outer",
            ["[k"] = "@class.outer",
            ["[a"] = "@parameter.outer",
            ["[i"] = "@conditional.outer",
          },
          goto_previous_end = {
            ["[F"] = "@function.outer",
            ["[K"] = "@class.outer",
            ["[B"] = "@block.outer",
            ["[A"] = "@parameter.outer",
            ["[C"] = "@comment.outer",
            ["[I"] = "@conditional.outer",
          },
        },
        select = {
          enable = true,

          -- Automatically jump forward to textobj, similar to targets.vim
          lookahead = true,

          keymaps = {
            -- You can use the capture groups defined in textobjects.scm
            ["a="] = { query = "@assignment.outer", desc = "Select outer assignment" },
            ["i="] = { query = "@assignment.inner", desc = "Select inner assignment" },

            ["aa"] = { query = "@parameter.outer", desc = "Select outer parameter/field" },
            ["ia"] = { query = "@parameter.inner", desc = "Select inner parameter/field" },

            ["ai"] = { query = "@conditional.outer", desc = "Select outer conditional" },
            ["ii"] = { query = "@conditional.inner", desc = "Select inner conditional" },

            ["al"] = { query = "@loop.outer", desc = "Select outer loop" },
            ["il"] = { query = "@loop.inner", desc = "Select inner loop" },

            ["ab"] = { query = "@block.outer", desc = "Select outer block" }, -- overrides default text object block of parenthesis to parenthesis
            ["ib"] = { query = "@block.inner", desc = "Select inner block" }, -- overrides default text object block of parenthesis to parenthesis

            ["af"] = { query = "@function.outer", desc = "Select outer function" },
            ["if"] = { query = "@function.inner", desc = "Select inner function" },

            ["ak"] = { query = "@class.outer", desc = "Select outer class" },
            ["ik"] = { query = "@class.inner", desc = "Select inner class" },

            ["ac"] = { query = "@comment.outer", desc = "Select outer comment" },
            ["ic"] = { query = "@comment.inner", desc = "Select inner comment" },

            -- ["ag"] = { query = "@buffer.outer", desc = "Select outer comment" },
          },
          include_surrounding_whitespace = true,
        },
        swap = {
          enable = true,
          swap_next = {
            ["g>"] = "@parameter.inner", -- swap object under cursor with next
          },
          swap_previous = {
            ["g<"] = "@parameter.inner", -- swap object under cursor with previous
          },
        },
      },
    },
    config = function(_, opts)
      require("nvim-treesitter.install").prefer_git = true
      require("nvim-treesitter.configs").setup(opts)
    end,
  },
}
