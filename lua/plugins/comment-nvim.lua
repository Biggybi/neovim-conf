return {
  "numToStr/Comment.nvim",
  opts = {
    toggler = { line = "gcc", block = "gCC" },
    opleader = { line = "gc", block = "gC" },
  },
  keys = {
    { "gc", desc = "Comment", mode = { "n", "v" } },
    { "gC", desc = "Comment block", mode = { "n", "v" } },
  },
}
