return {
  "ckolkey/ts-node-action",
  dependencies = { "nvim-treesitter" },
  keys = {
    {
      "<leader>rr",
      function() require("ts-node-action").node_action() end,
      desc = "Trigger Node Action",
    },
  },
  opts = {},
}
