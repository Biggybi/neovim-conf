-- 	{ "--search-parent-directories", "--collapse-simple-statement", "--stdin-filepath", "$FILENAME", "-" }

local cmd = vim.api.nvim_create_user_command

local function notif(buffer)
  -- local on = buffer and (vim.b.disable_autoformat == false) or (vim.g.disable_autoformat == false)
  local on = true
  if buffer then
    on = vim.b.disable_autoformat == false
  else
    on = vim.g.disable_autoformat == false
  end
  vim.notify(
    string.format("Autoformat %s (%s)", on and "enabled " or "disabled", buffer and "buffer" or "global"),
    vim.diagnostic.severity.INFO,
    { title = "Conform" }
  )
end

local function formatDisable(args)
  if args.bang then
    -- buffer only
    vim.b.disable_autoformat = true
  else
    vim.g.disable_autoformat = true
  end
  notif(args.bang)
end

local function formatEnable(args)
  if args.bang then
    -- buffer only
    vim.b.disable_autoformat = false
  else
    vim.g.disable_autoformat = false
  end
  notif(args.bang)
end

local function formatToggle(args)
  if args.bang then
    -- buffer only
    vim.b.disable_autoformat = not vim.b.disable_autoformat
  else
    vim.g.disable_autoformat = not vim.g.disable_autoformat
  end
  notif(args.bang)
end

local function format() require("conform").format({ timeout_ms = 500, lsp_fallback = true }) end

cmd("FormatDisable", formatDisable, { desc = "Disable format on save", bang = true })
cmd("FormatEnable", formatEnable, { desc = "Enable format on save", bang = true })
cmd("FormatToggle", formatToggle, { desc = "Toggle format on save", bang = true })
cmd("Format", format, { desc = "format with conform" })

local function autoformat(bufnr)
  -- Disable with a global or buffer-local variable
  if vim.g.disable_autoformat or vim.b[bufnr].disable_autoformat then return end
  return { timeout_ms = 500, lsp_fallback = true }
end

return {
  event = "BufWritePre",
  keys = {
    { "<leader>==", format, desc = "Conform Format" },
    { "yo=", function() formatToggle({ bang = true }) end, desc = "Toggle format on save (buffer)" },
    { "yo+", function() formatToggle({ bang = false }) end, desc = "Toggle format on save (global)" },
  },
  cmd = {
    Format = "Format",
    FormatToggle = "FormatToggle",
    FormatEnable = "FormatEnable",
    FormatDisable = "FormatDisable",
  },
  "stevearc/conform.nvim",
  opts = function()
    -- require("conform.formatters.prettierd").args = function(ctx)
    -- 	local args = { "--stdin-filepath", "$FILENAME" }
    -- 	local found = vim.fs.find(".prettierrc.js", { upward = true, path = ctx.dirname })[1]
    -- 	if found then
    -- 		vim.list_extend(args, { "--config", found })
    -- 	end
    -- 	return args
    -- end

    return {
      formatters_by_ft = {
        lua = { "stylua" },
        -- Conform will run multiple formatters sequentially
        python = { "isort", "black" },
        -- Use a sub-list to run only the first available formatter
        javascript = { "prettierd" },
        typescript = { "prettierd" },
        typescriptreact = { "prettierd" },
        javascriptreact = { "prettierd" },
        json = { "prettierd" },
        markdown = { "prettierd" },
        c = { "clang-format" },
        yaml = { "yamlfmt" },
        bash = { "shfmt" },
      },
      format_on_save = function(bufnr) return autoformat(bufnr) end,
    }
  end,
}
