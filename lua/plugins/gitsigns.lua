local prefix = "gh"

return {
  "lewis6991/gitsigns.nvim",
  event = { "BufReadPre", "BufNewFile" },
  dependencies = "tpope/vim-fugitive",
  keys = {
    { "]h", function() require("gitsigns").nav_hunk("next") end, desc = "Next git hunk" },
    { "[h", function() require("gitsigns").nav_hunk("prev") end, desc = "Previous git hunk" },
    {
      "]<c-h>",
      function() require("gitsigns").nav_hunk("next", { preview = true }) end,
      desc = "Next git hunk preview",
    },
    {
      "[<c-h>",
      function() require("gitsigns").nav_hunk("prev", { preview = true }) end,
      desc = "Previous git hunk preview",
    },

    { prefix, "<Nop>", desc = "Git" },
    { "yo<c-g>", function() require("gitsigns").toggle_signs() end, desc = "Toggle git signs" },
    { prefix .. "h", function() require("gitsigns").stage_buffer() end, desc = "Stage buffer" },
    { prefix .. "s", function() require("gitsigns").stage_hunk() end, desc = "Stage hunk" },
    {
      prefix .. "s",
      function() require("gitsigns").stage_hunk({ vim.fn.line("."), vim.fn.line("v") }) end,
      mode = "x",
      desc = "Stage selection",
    },
    { prefix .. "S", function() require("gitsigns").undo_stage_hunk() end, desc = "Undo stage hunk" },
    { prefix .. "d", function() require("gitsigns").preview_hunk() end, desc = "Preview hunk" },
    { prefix .. "b", function() require("gitsigns").blame_line() end, desc = "Blame line" },
    { prefix .. "u", function() require("gitsigns").reset_hunk() end, desc = "Reset hunk" },
    { prefix .. "U", function() require("gitsigns").reset_buffer() end, desc = "Reset buffer" },
    { prefix .. "R", function() require("gitsigns").reset_buffer_index() end, desc = "Reset buffer index" },
    { prefix .. "<c-h>", "<cmd>G add %<CR>", desc = "Add file" },
    { "g<c-h><c-h>", "<cmd>G add %<CR>", desc = "Add file" },
    {
      "yo<c-b>",
      function() require("gitsigns").toggle_current_line_blame() end,
      desc = "Toggle current line blame",
    },
    {
      "ih",
      -- NOTE: looks like we can't do '<c-u>' in pure lua
      ":<C-U>lua require('gitsigns.actions').select_hunk()<CR>",
      mode = { "o", "x" },
      silent = true,
      desc = "Select hunk",
    },
    {
      "ah",
      ":<C-U>lua require('gitsigns.actions').select_hunk()<CR>",
      mode = { "o", "x" },
      silent = true,
      desc = "Select hunk",
    },
  },
  opts = {
    signs = {
      add = { text = " ┃" },
      change = { text = " ┃" },
      delete = { text = " _" },
      topdelete = { text = " ‾" },
      changedelete = { text = " ┃" },
      untracked = { text = " ┆" },
    },
    signs_staged = {
      add = { text = " ┃" },
      change = { text = " ┃" },
      delete = { text = " _" },
      topdelete = { text = " ‾" },
      changedelete = { text = " ~" },
      untracked = { text = " ┆" },
    },
    current_line_blame_opts = {
      delay = 200,
    },
    preview_config = {
      border = "none",
    },
  },
}
