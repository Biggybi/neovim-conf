local prefix = "gh"
return {
  "tpope/vim-fugitive",
  dependencies = { "tpope/vim-rhubarb", "shumphrey/fugitive-gitlab.vim" },
  cmd = { "Gclog", "Glog", "Gedit", "Gsplit", "Gvsplit", "Gtabedit", "G" },
  keys = {
    { prefix, "<Nop>", desc = "Git" },
    { "<leader>gg", "<cmd>vert Git<cr>", desc = "Status" },
    { prefix .. "c", "<cmd>Git commit<cr>", desc = "Commit" },
    { prefix .. "e", "<cmd>Git commit --amend --no-edit<cr>", desc = "Commit amend (noedit)" },
    { prefix .. "a", "<cmd>Git commit --amend<cr>", desc = "Commit amend (edit)" },
    { prefix .. "p", "<cmd>Git push<cr>", desc = "Push" },

    { prefix .. "lq", "<cmd>Gclog<cr>", desc = "Log LocationList" },
    { prefix .. "ll", "<cmd>vert Git log<cr>", desc = "Log horizontal split" },
    { prefix .. "<c-l>", "<cmd>Git log<cr>", desc = "Log vert split" },
    { prefix .. "L", "<cmd>tab Git log<cr>", desc = "Log tab" },
    {
      prefix .. "lf",
      function()
        -- vim width
        vim.cmd("Git log")
        local current_win = vim.api.nvim_get_current_win()
        vim.api.nvim_open_win(0, true, {
          relative = "editor",
          width = 80,
          height = 15,
          row = (vim.o.lines - 15) / 2,
          col = (vim.o.columns - 80) / 2,
        })
        vim.api.nvim_win_close(current_win, true)
      end,
      desc = "Log float",
    },

    { prefix .. "<c-b>", "<cmd>Git blame<cr>", desc = "blame" },
    { prefix .. "<c-d>", "<cmd>Git diff<cr>", desc = "Diff" },
    { prefix .. "r", "<cmd>Git rebase<cr>", desc = "Rebase" },
    { prefix .. "m", "<cmd>Git mergetool<cr>", desc = "Mergetool" },
    { prefix .. "o", "<cmd>Git checkout<cr>", desc = "Checkout" },
    { prefix .. "B", "<cmd>Git branch<cr>", desc = "Branch" },
    { prefix .. "P", "<cmd>Git pull<cr>", desc = "Pull" },
    { prefix .. "f", "<cmd>Git fetch<cr>", desc = "Fetch" },
    { prefix .. "<c-p>", "<cmd>Git pull --rebase<cr>", desc = "Pull rebase" },
    { prefix .. "z", "<cmd>Git stash<cr>", desc = "Stash" },
    { prefix .. "x", "<cmd>Git stash pop<cr>", desc = "Stash pop" },
    { prefix .. "X", "<cmd>Git stash apply<cr>", desc = "Stash apply" },
  },
}
