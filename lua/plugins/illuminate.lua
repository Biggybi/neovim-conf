return {
  "RRethy/vim-illuminate",
  event = { "BufEnter" },
  keys = {
    { "[r", function() require("illuminate").goto_prev_reference() end, desc = "Previous Reference" },
    { "]r", function() require("illuminate").goto_next_reference() end, desc = "Next Reference" },
  },
  config = function()
    require("illuminate").configure({
      delay = 500,
      filetypes_denylist = {
        "fugitive",
        "NvimTree",
      },
    })
  end,
}
