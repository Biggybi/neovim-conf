local prefix = "<leader>q"

local function toggle(section) require("trouble").toggle(section) end

local function toggle_quickfix() toggle("quickfix") end
local function toggle_loclist() toggle("loclist") end
local function toggle_buffer_diagnostics() toggle("diagnostics_buffer") end
local function toggle_workspace_diagnostics() toggle("diagnostics") end
local function trouble_cascade() toggle("cascade") end

return {
  "folke/trouble.nvim",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  cmd = "Trouble",
  keys = {
    { prefix, "<Nop>", desc = "Trouble" },
    { prefix .. "q", toggle_quickfix, desc = "Trouble quickfix" },
    { prefix .. "l", toggle_loclist, desc = "Trouble loclist" },
    { prefix .. "b", toggle_buffer_diagnostics, desc = "Trouble document_diagnostics" },
    { prefix .. "w", toggle_workspace_diagnostics, desc = "Trouble workspace_diagnostics" },
    { prefix .. "d", trouble_cascade, desc = "Trouble cascade" },
  },
  opts = {
    padding = false,
    use_diagnostic_signs = true,
    modes = {
      diagnostics_buffer = {
        mode = "diagnostics", -- inherit from diagnostics mode
        filter = { buf = 0 }, -- filter diagnostics to the current buffer
      },
      -- show only most severe available diagnostics
      cascade = {
        mode = "diagnostics",
        filter = function(items)
          local severity = vim.diagnostic.severity.HINT
          for _, item in ipairs(items) do
            severity = math.min(severity, item.severity)
          end
          return vim.tbl_filter(function(item) return item.severity == severity end, items)
        end,
      },
    },
  },
}
