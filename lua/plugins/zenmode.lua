return {
  "folke/zen-mode.nvim",
  -- opts = {}
  keys = {
    {
      "yog",
      function() require("zen-mode").toggle() end,
      desc = "Toggle Zen Mode",
    },
    command = "ZenMode",
  },
}
