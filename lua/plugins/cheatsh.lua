return {
  "siawkz/nvim-cheatsh",
  dependencies = {
    "nvim-telescope/telescope.nvim",
  },
  cmd = { "Cheat", "CheatList" },
  opts = {},
}
