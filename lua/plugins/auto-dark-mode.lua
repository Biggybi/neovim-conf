return {
  "f-person/auto-dark-mode.nvim",
  event = "ColorScheme",
  enabled = vim.g.neovide == nil,
  opts = {
    fallback = "light",
    update_interval = 1000,
  },
}
