local prefix = "<leader>m"

return {
  "folke/noice.nvim",
  dependencies = { "MunifTanjim/nui.nvim" },
  event = "VeryLazy",
  keys = {
    { prefix, "<Nop>", desc = "Noice" },
    {
      prefix .. "a",
      function() require("noice").cmd("all") end,
      desc = "Noice all",
    },
    {
      prefix .. "f",
      function() require("noice").cmd("telescope") end,
      desc = "Noice filter",
    },
    {
      prefix .. "n",
      function() require("noice").cmd("notify") end,
      desc = "Noice notify",
    },
    {
      prefix .. "h",
      function() require("noice").cmd("history") end,
      desc = "Noice history",
    },
    {
      prefix .. "l",
      function() require("noice").cmd("last") end,
      desc = "Noice last",
    },
    {
      prefix .. "e",
      function() require("noice").cmd("errors") end,
      desc = "Noice errors",
    },
    {
      prefix .. "q",
      function() require("noice").cmd("dismiss") end,
      desc = "Noice dismiss",
    },
    {
      prefix .. "m",
      function() vim.cmd("messages") end,
      desc = "Messages",
    },
    {
      "<S-Enter>",
      function() require("noice").redirect(vim.fn.getcmdline()) end,
      desc = "Redirect Cmdline",
      mode = "c",
    },
    {
      "<c-d>",
      function()
        if not require("noice.lsp").scroll(4) then return "<c-d>" end
      end,
      desc = "Scroll hover down",
      silent = true,
      expr = true,
      mode = { "n", "i", "s", "c" },
    },
    {
      "<c-u>",
      function()
        if not require("noice.lsp").scroll(-4) then return "<c-u>" end
      end,
      desc = "Scroll hover up",
      silent = true,
      expr = true,
      mode = { "n", "i", "s" },
    },
  },

  opts = {
    presets = {
      long_message_to_split = false, -- long messages will be sent to a split
      inc_rename = true, -- enables an input dialog for inc-rename.nvim
      -- lsp_doc_border = false, -- add a border to hover docs and signature help
    },
    messages = {
      view_search = false,
    },
    views = {
      confirm = {
        size = {
          width = "auto",
          min_width = 60,
          height = "auto",
        },
        border = {
          style = "none",
          padding = { 1, 2 },
        },
      },
      cmdline_popup = {
        relative = "editor",
        border = {
          style = "none",
          padding = { 1, 2 },
        },
        filter_options = {},
        win_options = {},
        position = {
          row = 3,
          col = "50%",
        },
        size = {
          min_width = 60,
          height = "auto",
        },
      },
      popupmenu = {
        relative = "editor",
        size = {
          width = 60,
          height = "auto",
        },
        position = "auto",
        border = {
          style = "none",
          padding = { 1, 2 },
        },
      },
      popup = {
        win_options = {
          wrap = true,
          linebreak = true,
        },
        border = {
          style = "none",
          padding = { 1, 2 },
        },
      },
    },
    cmdline = {
      format = {
        -- conceal: (default=true) This will hide the text in the cmdline that matches the pattern.
        -- view: (default is cmdline view)
        -- opts: any options passed to the view
        -- icon_hl_group: optional hl_group for the icon
        -- title: set to anything or empty string to hide
        cmdline = {
          title = "",
          pattern = "^:",
          icon = "  ", -- 
          lang = "vim",
        },

        search_down = {
          kind = "search",
          title = "",
          pattern = "^/",
          -- icon = "/ ",
          icon = " ",
          lang = "regex",
        },
        search_up = {
          -- kind = "search",
          title = "",
          pattern = "^%?",
          -- icon = " ",
          -- icon = "?",
          icon = " ",
          lang = "regex",
        },
        -- rename = {
        --   title = "rename",
        --   pattern = "New Name:",
        -- },

        calculator = {
          pattern = "^=",
          icon = " ", -- 
          lang = "vimnormal",
        },

        filter = {
          title = "",
          pattern = "^:%s*!",
          icon = "  ",
          lang = "bash",
        },

        lua = {
          pattern = { "^:%s*lua%s+", "^:%s*lua%s*=%s*", "^:%s*=%s*" },
          icon = "  ",
          lang = "lua",
        },

        help = {
          title = "",
          pattern = "^:%s*[hH]e?l?p?%s+",
          icon = "  ",
          -- icon = ">  h"   
        },

        man = {
          title = "",
          pattern = "^:%s*Ma?n?%s+",
          icon = "  ",
          -- icon = ">  h"   
        },

        input = {
          title = "",
        }, -- Used by input()
        -- lua = false, -- to disable a format, set to `false`
      },
    },
    lsp = {
      -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
      override = {
        ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
        ["vim.lsp.util.stylize_markdown"] = true,
        ["cmp.entry.get_documentation"] = true,
      },
    },
    commands = {
      history = {
        view = "popup",
        opts = { enter = true, format = "details" },
        filter = {
          any = {
            { event = "notify" },
            { error = true },
            { warning = true },
            { event = "msg_show", kind = { "" } },
            { event = "lsp", kind = "message" },
          },
        },
      },
      last = {
        view = "popup",
        opts = { enter = true, format = "details" },
        filter = {
          any = {
            { event = "notify" },
            { error = true },
            { warning = true },
            { event = "msg_show", kind = { "" } },
            { event = "lsp", kind = "message" },
          },
        },
        filter_opts = { count = 1 },
      },
      errors = {
        view = "popup",
        opts = { enter = true, format = "details" },
        filter = { error = true },
        filter_opts = { reverse = true },
      },
    },
    notify = {
      enabled = true,
      view = "notify",
    },
  },
}
