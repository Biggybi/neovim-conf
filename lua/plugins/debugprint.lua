return {
  "andrewferrier/debugprint.nvim",
  dependencies = {
    "nvim-treesitter/nvim-treesitter",
  },
  keys = {
    { "<leader>lv", mode = { "n", "v" }, desc = "DebugPrint Var Below" },
    { "<leader>lV", mode = { "n", "v" }, desc = "DebugPrint Var Above" },
    { "<leader>lc", desc = "DebugPrint Toggle Comment" },
    { "<leader>lx", desc = "DebugPrint Delete" },
    { "<leader>ll", desc = "DebugPrint Below" },
    { "<leader>lL", desc = "DebugPrint Above" },
  },
  cmd = {
    "ToggleCommentDebugPrints",
    "DebugprintDelete",
  },
  opts = {
    keymaps = {
      normal = {
        variable_below = "<leader>lv",
        variable_above = "<leader>lV",
        toggle_comment_debug_prints = "<leader>lc",
        delete_debug_prints = "<leader>lx",
        plain_below = "<leader>ll",
        plain_above = "<leader>lL",
        variable_below_alwaysprompt = nil,
        variable_above_alwaysprompt = nil,
        textobj_below = nil,
        textobj_above = nil,
      },
      visual = {
        variable_below = "<leader>lv",
        variable_above = "<leader>lV",
      },
      commands = {
        toggle_comment_debug_prints = "DebugprintToggleComment",
        delete_debug_prints = "DebugprintDelete",
      },
    },
  },
}
