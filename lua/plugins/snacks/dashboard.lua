---@type (snacks.dashboard.Config)?
---@diagnostic disable-next-line: missing-fields
return {
  preset = {
    keys = {
      {
        icon = "",
        key = "e",
        desc = "new file",
        action = "<cmd>ene<CR>",
      },
      {
        icon = "",
        key = "<leader>ee",
        desc = "toggle file explorer",
        action = function() Snacks.picker.explorer() end,
      },
      {
        icon = "",
        key = "<leader>f-",
        desc = "toolbox",
        action = function() require("plugins.snacks.pickers.toolbox").show_toolbox() end,
      },
      {
        icon = "",
        desc = "find files",
      },
      {
        icon = "     ",
        key = "<leader>F",
        desc = "root",
        action = function() Snacks.picker.files({ cwd = "/" }) end,
      },
      {
        icon = "     ",
        key = "<leader>ff",
        desc = "home",
        action = function() Snacks.picker.files({ cwd = vim.uv.os_homedir() }) end,
      },
      {
        icon = "     ",
        key = "<leader>fw",
        desc = "work",
        action = function() Snacks.picker.files({ cwd = "/data/work/" }) end,
      },
      {
        icon = "     ",
        key = "<leader>fn",
        desc = "notes",
        action = function() Snacks.picker.files({ cwd = "~/Notes/second_brain/" }) end,
      },
      {
        icon = "     ",
        key = "<leader>f.",
        desc = "dotfiles",
        action = function() Snacks.picker.files({ cwd = vim.uv.os_homedir() .. "/dotfiles/" }) end,
      },
      {
        icon = "     ",
        key = "<leader>fi",
        desc = "neovim config",
        action = function() Snacks.picker.files({ cwd = vim.fn.stdpath("config") }) end,
      },
      {
        icon = "     ",
        key = "<leader><c-f>",
        desc = "current directory",
        action = function() Snacks.picker.files({ cwd = vim.uv.cwd() }) end,
      },
      {
        icon = "     ",
        key = "<leader>gf",
        desc = "git directory",
        action = function() Snacks.picker.git_files() end,
      },
      {
        icon = "",
        key = "<leader>fr",
        desc = "find in file",
        action = function() Snacks.picker.grep() end,
      },
      {
        icon = "",
        key = "q",
        desc = "quit",
        action = "<cmd>qa<CR>",
      },
    },
  },
  formats = {
    header = { "Trx's neovim", align = "center" },
  },
}
