---@type (snacks.notifier.Config)?
return {
  style = function(buf, notif, ctx)
    ctx.opts.border = "none"
    vim.api.nvim_buf_set_lines(buf, 0, -1, false, vim.split(notif.msg .. "  ", "\n"))
    vim.api.nvim_buf_set_extmark(buf, ctx.ns, 0, 0, {
      virt_text = { { notif.icon, ctx.hl.icon } },
      virt_text_pos = "right_align",
    })
  end,
  enabled = true,
  timeout = 3000,
  width = { min = 20, max = 0.4 },
  top_down = false,
  icons = {
    error = " ", --   
    warn = " ",
    info = " ",
    debug = " ",
    trace = " ", --   
  },
}
