---@type table<string, snacks.win.Config>?
return {
  snacks_image = {
    relative = nil,
    border = "none",
  },
  notification = {
    wo = { wrap = true },
    relative = "editor",
  },
}
