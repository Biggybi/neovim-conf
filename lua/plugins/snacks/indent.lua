---@type (snacks.indent.Config)?
return {
  animate = { enabled = vim.g.neovide == true },
  indent = {
    enabled = true,
    only_scope = true, -- only show indent guides of the scope
    only_current = true, -- only show indent guides in the current window
    hl = "SnacksIndent", ---@type string|string[] hl groups for indent guides
    -- hl = {
    --   "SnacksIndent1",
    --   "SnacksIndent2",
    --   "SnacksIndent3",
    --   "SnacksIndent4",
    --   "SnacksIndent5",
    --   "SnacksIndent6",
    --   "SnacksIndent7",
    --   "SnacksIndent8",
    -- },
  },
  scope = {
    underline = false, -- underline the start of the scope
    only_current = true, -- only show scope in the current window
    hl = "SnacksIndentScope", ---@type string|string[] hl group for scopes
  },
  blank = {
    -- char = " ",
    char = "·",
    hl = "SnacksIndentBlank", ---@type string|string[] hl group for blank spaces
  },
}
