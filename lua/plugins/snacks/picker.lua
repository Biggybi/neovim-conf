local conflib = require("core.conflib")

---@type snacks.picker.Config
return {
  enabled = true,
  prompt = " ",
  sources = {
    explorer = {
      layout = {
        layout = {
          box = "vertical",
          position = "left",
          width = 0.2,
          {
            win = "input",
            max_height = 1,
            height = 1,
            border = { "", "", "", "", "", "", "", " " },
            wo = {
              winhighlight = "FloatBorder:Normal,NormalFloat:Normal,SnacksPickerPrompt:SnacksPickerPromptTransparent",
            },
          },
          {
            win = "list",
            border = "none",
            wo = {
              winhighlight = "FloatBorder:Normal,NormalFloat:Normal",
            },
          },
        },
      },
      win = {
        list = {
          keys = {
            ["h"] = "explorer_collapse",
            ["l"] = "explorer_expand",
          },
        },
      },
    },
  },
  layouts = {
    select = {
      preview = nil,
      cycle = true,
      layout = {
        relative = "cursor",
        row = 1,
        backdrop = false,
        max_width = 60,
        max_height = 10,
        box = "vertical",
        {
          win = "input",
          height = 1,
          border = { "", "", "", "", "", "", "", " " },
          wo = {
            winhighlight = "SnacksPickerInput:SnacksPicker,FloatBorder:SnacksPicker,SnacksPickerPrompt:SnacksPickerInputSearch",
          },
        },
        {
          win = "list",
          border = { "", "", "", " ", "", "", "", " " },
          wo = {
            winhighlight = "FloatBorder:SnacksPicker,SnacksPickerPrompt:SnacksPickerInputSearch",
          },
        },
        { win = "preview", title = "{preview}", height = 0.4, border = "none" },
      },
    },
    vscode = {
      preview = nil,
      layout = {
        backdrop = false,
        row = 3,
        width = 60,
        height = 0.3,
        max_height = 30,
        border = "none",
        box = "vertical",
        {
          win = "input",
          height = 1,
          border = { " " },
          title = "{title} {live} {flags}",
          title_pos = "center",
        },
        {
          win = "list",
          border = { " " },
        },
      },
    },
    prompt_center = {
      preview = nil,
      layout = {
        backdrop = false,
        width = 0.5,
        min_width = 100,
        max_width = 120,
        height = 0.8,
        min_height = 30,
        max_height = 50,
        box = "vertical",
        border = "none",
        title_pos = "center",
        {
          win = "preview",
          show = false,
          title_pos = "center",
          border = { " " },
          wo = {
            relativenumber = false,
            number = true,
            winbar = "",
            signcolumn = "yes",
            cursorlineopt = "screenline,number",
            -- HACK: preview window color
            winhighlight = "FloatBorder:SnacksPickerPreviewBorder,CursorLine:SnacksPickerPreviewCursorLine,BeNLineNr:SnacksPickerPreviewCursorLineNr,CursorLineSep:SnacksPickerPreviewCursorLineNrSep",
          },
        },
        {
          win = "input",
          title = "{source} {live}",
          row = function() return vim.o.lines / 4 end,
          relative = "editor",
          height = 1,
          border = { " ", " ", "", "", "", " ", " ", " " },
        },
        {
          win = "list",
          border = { " " },
        },
      },
    },
    prompt_center_narrow = {
      preview = nil,
      layout = {
        backdrop = false,
        width = 0.9,
        min_width = 70,
        max_width = 120,
        height = 0.9,
        min_height = 30,
        max_height = 50,
        box = "vertical",
        border = "none",
        title_pos = "center",
        {
          win = "preview",
          show = false,
          title_pos = "center",
          border = { " " },
          wo = {
            relativenumber = false,
            number = true,
            winbar = "",
            signcolumn = "yes",
            cursorlineopt = "screenline,number",
            -- HACK: preview window color
            winhighlight = "FloatBorder:SnacksPickerPreviewBorder,CursorLine:SnacksPickerPreviewCursorLine,BeNLineNr:SnacksPickerPreviewCursorLineNr,CursorLineSep:SnacksPickerPreviewCursorLineNrSep",
          },
        },
        {
          win = "input",
          title = "{source} {live}",
          row = function() return vim.o.lines / 4 end,
          relative = "editor",
          height = 1,
          border = { " ", " ", "", "", "", " ", " ", " " },
        },
        {
          win = "list",
          border = { " " },
        },
      },
    },
  },
  layout = {
    preset = function() return vim.o.columns >= 120 and "prompt_center" or "prompt_center_narrow" end,
  },
  win = {
    input = {
      keys = {
        ["<Esc>"] = { "close", mode = { "n", "i" } },
        ["<c-]>"] = { "toggle_preview", mode = { "i", "n" } },
        ["<c-f>"] = "<right>",
        ["<c-b>"] = "<left>",
        ["<a-j>"] = { "list_down", mode = { "i", "n" } },
        ["<a-k>"] = { "list_up", mode = { "i", "n" } },
        ["<c-o>"] = { "select_and_prev", mode = { "i", "n" } },
        ["<c-d>"] = { "preview_scroll_down", mode = { "i", "n" } },
        ["<c-u>"] = { "preview_scroll_up", mode = { "i", "n" } },
        ["<c-t>"] = { "edit_tab", mode = { "i", "n" } },
        ["<c-v>"] = { "edit_vsplit", mode = { "i", "n" } },
        ["<c-s>"] = { "edit_split", mode = { "i", "n" } },
        ["<c-x>"] = { "edit_split", mode = { "i", "n" } },
        ["<a-d>"] = { "bufdelete", mode = { "i", "n" } },
      },
    },
    list = {
      keys = {
        ["<c-d>"] = "preview_scroll_down",
        ["<c-u>"] = "preview_scroll_up",
        ["<c-v>"] = "edit_vsplit",
        ["<c-s>"] = "edit_split",
        ["<c-t>"] = "edit_tab",
        ["<c-n>"] = { "preview_scroll_down", mode = { "i", "n" } },
        ["<c-p>"] = { "preview_scroll_up", mode = { "i", "n" } },
        ["<a-w>"] = "cycle_win",
        ["<Esc>"] = "close",
      },
    },
    preview = {
      keys = {
        ["<c-d>"] = { "preview_scroll_down", mode = { "i", "n" } },
        ["<c-u>"] = { "preview_scroll_up", mode = { "i", "n" } },
        ["<a-w>"] = { "cycle_win", mode = { "i", "n" } },
      },
    },
  },
  formatters = {
    selected = {
      show_always = true,
    },
  },
  icons = {
    lsp = {
      unavailable = "",
      enabled = " ",
      disabled = " ",
      attached = " ",
    },
    keymaps = {
      nowait = " ", --      
    },
    undo = {
      saved = " ",
    },
    files = {
      enabled = true, -- show file icons
    },
    tree = {
      vertical = "│ ",
      middle = "├╴",
      last = "└╴",
    },
    indent = {
      vertical = "│ ",
      middle = "├╴",
      last = "└╴",
    },
    ui = {
      live = " ", --  
      selected = "   ", --    
      unselected = "   ",
      -- selected = " ",
    },
    git = {
      commit = " ",
    },
    diagnostics = {
      Error = " ",
      Warn = " ",
      Hint = " ",
      Info = " ",
    },
    kinds = {
      Array = " ",
      Boolean = " ",
      Class = "󰠲 ",
      Color = " ",
      Control = " ",
      Collapsed = " ",
      Constant = "󰏿 ",
      Constructor = " ",
      Copilot = " ",
      Enum = " ",
      EnumMember = " ",
      Event = "⚡ ",
      Field = " ",
      File = " ",
      Folder = " ",
      Function = " ",
      Interface = " ",
      Key = " ",
      Keyword = " ",
      Method = "󰅩 ",
      Module = " ",
      Namespace = "󰦮 ",
      Null = " ",
      Number = "󰎠 ",
      Object = " ",
      Operator = " ",
      Package = " ",
      Property = " ",
      Reference = " ",
      Snippet = " ",
      String = " ",
      Struct = " ",
      Text = "󰉿 ",
      TypeParameter = " ",
      Unit = " ",
      Uknown = " ",
      Value = " ",
      Variable = " ",
    },
  },
}
