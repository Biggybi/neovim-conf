local window_is_focused = function(win) return vim.api.nvim_get_current_win() == win end

local custom_path = {
  get_symbols = function(buff, win, cursor)
    local highlight_group = "DropBarFileName"
    local symbols = require("dropbar.sources").path.get_symbols(buff, win, cursor)
    if vim.bo[buff].modified then highlight_group = highlight_group .. "Modified" end
    if not window_is_focused(win) then highlight_group = highlight_group .. "NC" end
    symbols[#symbols].name_hl = highlight_group
    return symbols
  end,
}

return {
  "Bekaboo/dropbar.nvim",
  event = { "BufRead", "BufNewFile" },
  keys = {
    {
      "<leader><c-n>",
      function() require("dropbar.api").pick(vim.v.count ~= 0 and vim.v.count or nil) end,
      desc = "Open dropbar menu",
    },
  },
  opts = {
    bar = {
      sources = function(buf, _)
        local sources = require("dropbar.sources")
        local utils = require("dropbar.utils")
        if vim.bo[buf].ft == "markdown" then return {
          custom_path,
          sources.markdown,
        } end
        if vim.bo[buf].buftype == "terminal" then return {
          sources.terminal,
        } end
        return {
          custom_path,
          utils.source.fallback({
            sources.lsp,
            sources.treesitter,
          }),
        }
      end,
      update_events = {
        win = {
          "CursorMoved",
          "WinEnter",
          "WinResized",
        },
      },
    },
    icons = {
      kinds = {
        symbols = {
          Array = "󰅪 ",
          Boolean = " ",
          BreakStatement = "󰙧 ",
          Call = " ",
          CaseStatement = " ",
          Class = "󰠲 ",
          Color = " ",
          Constant = "󰏿 ",
          Constructor = " ",
          ContinueStatement = " ",
          Copilot = " ",
          Declaration = "󰙠 ",
          Delete = " ",
          DoStatement = " ",
          Enum = " ",
          EnumMember = " ",
          Event = "⚡ ",
          Field = " ",
          File = " ",
          Folder = " ",
          ForStatement = " ",
          Function = " ",
          H1Marker = "󰎥 ", -- Used by markdown treesitter parser
          H2Marker = "󰎨 ",
          H3Marker = "󰎫 ",
          H4Marker = "󰎲 ",
          H5Marker = "󰎯 ",
          H6Marker = "󰎴 ",
          Identifier = " ",
          IfStatement = " ",
          Interface = " ",
          Keyword = " ",
          List = " ",
          Log = " ",
          Lsp = " ",
          Macro = " ",
          MarkdownH1 = "󰎥 ", -- Used by builtin markdown source
          MarkdownH2 = "󰎨 ",
          MarkdownH3 = "󰎫 ",
          MarkdownH4 = "󰎲 ",
          MarkdownH5 = "󰎯 ",
          MarkdownH6 = "󰎴 ",
          Method = "󰅩 ",
          Module = " ",
          Namespace = "󰅩 ",
          Null = " ",
          Number = " ",
          Object = "󰅩 ",
          Operator = " ",
          Package = " ",
          Pair = "󰅪 ",
          Property = " ",
          Reference = " ",
          Regex = " ",
          Repeat = " ",
          Scope = "󰅩 ",
          Snippet = " ",
          Specifier = " ",
          Statement = "󰅩 ",
          String = " ",
          Struct = " ",
          SwitchStatement = " ",
          Terminal = " ",
          Text = "󰉿 ",
          Type = " ",
          TypeParameter = " ",
          Unit = " ",
          Value = " ",
          Variable = " ",
          WhileStatement = " ",
        },
      },
    },
  },
}
