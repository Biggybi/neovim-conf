return {
  "MeanderingProgrammer/markdown.nvim",
  name = "render-markdown",
  dependencies = {
    "nvim-treesitter/nvim-treesitter",
    "nvim-tree/nvim-web-devicons",
  },
  keys = {
    { "yo<c-r>", function() require("render-markdown").toggle() end, desc = "Toggle render-markdown" },
  },
  ft = "markdown",
  opts = {
    file_types = { "markdown", "cmp_menu" },
    code = {
      sign = false,
      border = "thick",
      width = "block",
      left_pad = 2,
      right_pad = 2,
      position = "right",
      language_pad = 1,
      -- above = "",
      -- bellow = "",
    },
    bullet = {
      icons = { "", "", "", "" },
    },
    heading = {
      sign = false,
      icons = { "󰎥 ", "󰎨 ", "󰎫 ", "󰎲 ", "󰎯 ", "󰎴 " },
    },
    checkbox = {
      unchecked = {
        icon = "󰄱 ",
      },
      checked = {
        icon = "󰱒 ",
      },
      custom = {
        todo = { raw = "[-]", rendered = " ", highlight = "RenderMarkdownTodo" },
      },
    },
    overrides = {
      -- for blink.cmp
      filetype = {
        cmp_menu = {
          render_modes = { "n", "c", "i" },
          debounce = 0,
          code = {
            left_pad = 0,
            right_pad = 0,
            language_pad = 0,
          },
        },
      },
      buftype = {
        help = {
          anti_conceal = {
            enabled = false,
          },
        },
      },
    },
  },
}
