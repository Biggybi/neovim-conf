---@param kind 'opt' | 'key' | 'cmd' | nil
local function toggle(option, on, off, kind)
  local on_text = on
  local off_text = off
  if on == nil then
    on = true
    on_text = "on "
  end
  if off == nil then
    off_text = "off"
    off = false
  end
  -- vim.print("on: " .. tostring(on) .. " off: " .. tostring(off))
  if vim.fn.exists("&" .. option) == 0 then
    error(string.format("'%s' option not found", option))
    return
  end
  if kind == "key" then
    -- toggle the 'on' key, don't use 'off'
    local has_key = vim.tbl_contains(vim.opt.cursorlineopt:get(), on)
    if has_key then
      vim.opt.cursorlineopt:remove(on)
      vim.print("cursorlineopt: no " .. on)
    else
      vim.opt.cursorlineopt:append(on)
      vim.print("cursorlineopt:    " .. on)
    end
    return
  end
  if kind == "cmd" then
    vim.cmd(vim.o[option] == true and off or on)
    vim.print(option .. ": " .. (vim.o[option] and on or off))
    return
  end
  vim.o[option] = (vim.o[option] == off and on or off)
  vim.print(option .. ": " .. (vim.o[option] == on and on_text or off_text))
end

local prefix = "yo"

return {
  "tpope/vim-unimpaired",
  event = { "BufRead", "BufNewFile" },
  keys = {
    { "yo", "<Nop>", mode = "n", desc = "Toggles" },
    { "]q", "<Plug>(unimpaired-cnext)", desc = "Cnext" },
    { "[q", "<Plug>(unimpaired-cprevious)", desc = "Cprevious" },
    { "]<c-f>", "<Plug>(unimpaired-directory-next)", desc = "Next file" },
    { "[<c-f>", "<Plug>(unimpaired-directory-previous)", desc = "Prev file" },
    { "]<c-a>", "<Plug>(unimpaired-next)", desc = "Next argument" },
    { "[<c-a>", "<Plug>(unimpaired-previous)", desc = "Prev argument" },
    { "]<c-b>", "<Plug>(unimpaired-bnext)", desc = "Next buffer" },
    { "[<c-b>", "<Plug>(unimpaired-bprevious)", desc = "Prev buffer" },
    { "[e", "<plug>(unimpaired-move-selection-up)|gv", desc = "Move selection up", mode = "x" },
    { "]e", "<Plug>(unimpaired-move-selection-down)|gv", desc = "Move selection down", mode = "x" },
    { "<M-k>", "<plug>(unimpaired-move-selection-up)|gv", desc = "Move selection up", mode = "x" },
    { "<M-j>", "<Plug>(unimpaired-move-selection-down)|gv", desc = "Move selection down", mode = "x" },
    -- replace toggle mappings so they don't blink Trouble command-line
    {
      prefix .. "b",
      function() toggle("background", "light", "dark") end,
      desc = "Toggle dark/light background",
    },
    {
      prefix .. "c",
      desc = "Toggle cursorlineopt screenline",
      function() toggle("cursorlineopt", "screenline", nil, "key") end,
    },
    {
      prefix .. "d",
      function() toggle("diff", "diffthis", "diffoff ", "cmd") end,
      desc = "Toggle diffthis/diffoff",
    },
    {
      prefix .. "h",
      function()
        if vim.o.hlsearch == true and vim.v.hlsearch == 1 then
          vim.cmd("nohl")
          vim.print("highlights off")
        else
          vim.cmd("set hlsearch")
          vim.print("highlights on ")
        end
      end,
      desc = "Toggle 'hlsearch'",
    },
    {
      prefix .. "<c-h>",
      function() toggle("winfixheight") end,
      desc = "Toggle winfixheight",
    },
    {
      prefix .. "<c-w>",
      function() toggle("winfixwidth") end,
      desc = "Toggle winfixwidth",
    },
    {
      prefix .. "z",
      function()
        vim.wo.scrolloff = 999 - vim.wo.scrolloff
        if vim.wo.scrolloff >= 100 then
          vim.cmd("norm! zz")
          vim.notify("center scroll:  on")
        else
          vim.notify("center scroll: off")
        end
      end,
      desc = "Toggle vertical center cursor",
    },
    {
      prefix .. "i",
      function() toggle("ignorecase") end,
      desc = "Toggle 'ignorecase'",
    },
    {
      prefix .. "l",
      function() toggle("list") end,
      desc = "Toggle 'list'",
    },
    {
      prefix .. "n",
      function() toggle("number") end,
      desc = "Toggle 'number'",
    },
    {
      prefix .. "r",
      function() toggle("relativenumber") end,
      desc = "Toggle 'relativenumber'",
    },
    {
      prefix .. "s",
      function() toggle("spell") end,
      desc = "Toggle 'spell'",
    },
    {
      prefix .. "w",
      function() toggle("wrap") end,
      desc = "Toggle 'wrap'",
    },
    {
      prefix .. "x",
      function() toggle("colorcolumn") end,
      desc = "Toggle 'colorcolumn'",
    },
  },
  config = function() end,
}
