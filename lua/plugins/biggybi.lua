return {
  {
    "https://gitlab.com/Biggybi/nvim-neovimux.git",
    opts = {
      shell = {
        prompt = "",
      },
    },
    keys = { { "<C-Space>" } },
    event = { "TermOpen" },
    -- dev = true,
  },
  {
    "https://gitlab.com/Biggybi/nvim-smartcd.git",
    opts = {},
    event = { "BufEnter", "BufRead" },
    cmd = { "SmartCd" },
    keys = { { "<leader>cd" } },
  },
  {
    "https://gitlab.com/Biggybi/nvim-searchdown.git",
    opts = {
      inline_keys = { next = ":", prev = "," },
    },
    keys = {
      { ":", mode = { "n", "o", "x" } },
      { ",", mode = { "n", "o", "x" } },
      { "n", mode = { "n", "o", "x" } },
      { "N", mode = { "n", "o", "x" } },
    },
    dev = true,
  },
  {
    "https://gitlab.com/Biggybi/nvim-trimmer.git",
    opts = {},
    keys = { { "dx", desc = "Trim" } },
    cmd = { "Trim", "TrimHead" },
  },
  {
    "https://gitlab.com/Biggybi/nvim-chimera.git",
    dependencies = { "numToStr/Comment.nvim" },
    opts = {},
    keys = {
      { "yc", desc = "Chimera" },
      { "gyc", desc = "Chimera", mode = "x" },
      { "yC", desc = "Chimera block" },
      { "gyC", desc = "Chimera block", mode = "x" },
    },
    dev = true,
  },
  {
    "https://gitlab.com/Biggybi/nvim-tabline.git",
    dependencies = {
      "nvim-tree/nvim-web-devicons",
      "folke/tokyonight.nvim",
    },
    opts = {},
    event = "ColorScheme",
    dev = true,
  },
  {
    "https://gitlab.com/Biggybi/nvim-verboseupdate.git",
    opts = {},
    keys = { { "<c-s>", desc = "Save file", mode = { "n", "i", "x" } } },
    -- dev = true,
  },
  {
    "https://gitlab.com/Biggybi/nvim-cmdpath.git",
    opts = {},
    keys = { { "<C-n>" } },
    -- dev = true,
  },
  {
    "https://gitlab.com/Biggybi/nvim-toggleeolchar.git",
    opts = {},
    keys = { { "ga", desc = "Toggle EOL char" } },
  },
  {
    "https://gitlab.com/Biggybi/nvim-promptobjects.git",
    opts = {},
    event = { "CmdlineEnter" },
  },
  {
    "https://gitlab.com/Biggybi/nvim-filepaths.git",
    opts = {},
    keys = {
      { "<leader>fp", function() require("filepaths.picker").list_paths() end, desc = "List file paths" },
      { "<c-r><c-f>", function() require("filepaths.picker").list_paths() end, desc = "List file paths", mode = "i" },
    },
  },
  {
    "https://gitlab.com/Biggybi/nvim-cmdswitch.git",
    opts = {},
    keys = {
      { "<M-[>", function() require("cmdswitch.cmdswitch").cycleCmd(false) end, mode = "c" },
      { "<M-]>", function() require("cmdswitch.cmdswitch").cycleCmd(true) end, mode = "c" },
    },
    dev = true,
  },
  {
    "https://gitlab.com/Biggybi/nvim-ftplug.git",
    opts = {},
    enabled = true,
  },
  {
    "https://gitlab.com/Biggybi/nvim-nyx.git",
    opts = {
      default = "light",
    },
    enabled = true,
  },
}
