return {
  "saghen/blink.cmp",
  dependencies = {
    "rafamadriz/friendly-snippets",
    {
      "saghen/blink.compat",
      opts = {},
      version = "*",
    },
  },
  version = "*",
  build = "rustup run nightly cargo build --release",
  opts_extend = {
    "sources.completion.enabled_providers",
    "sources.compat",
    "sources.default",
  },
  event = { "InsertEnter", "CmdlineEnter" },

  ---@type blink.cmp.Config
  opts = {
    cmdline = {
      keymap = {
        ["<c-j>"] = { "show", "select_next", "fallback" },
        ["<c-k>"] = { "show", "select_prev", "fallback" },
        ["<c-e>"] = { "hide", "fallback" },
        ["<c-space>"] = { "show", "select_and_accept", "fallback" },
        ["<c-]>"] = { "show", "hide", "fallback" },
        ["<c-u>"] = { "scroll_documentation_up", "fallback" },
        ["<c-d>"] = { "scroll_documentation_down", "fallback" },
      },
    },
    keymap = {
      ["<c-j>"] = { "show", "select_next", "fallback" },
      ["<c-k>"] = { "show", "select_prev", "fallback" },
      ["<c-e>"] = { "hide", "fallback" },
      ["<c-space>"] = { "show", "select_and_accept", "fallback" },
      ["<up>"] = { "select_prev", "fallback" },
      ["<down>"] = { "select_next", "fallback" },
      ["<c-]>"] = { "show_documentation", "hide_documentation", "fallback" },
      ["<c-u>"] = { "scroll_documentation_up", "fallback" },
      ["<c-d>"] = { "scroll_documentation_down", "fallback" },
      ["<c-f>"] = { "snippet_forward", "fallback" },
      ["<c-b>"] = { "snippet_backward", "fallback" },
    },
    sources = {
      default = {
        "lsp",
        "buffer",
        "path",
        "snippets",
      },
      per_filetype = {
        lua = {
          "lazydev",
          "lsp",
          "buffer",
          "path",
          "snippets",
        },
        sql = {
          "dadbod",
          "lsp",
          "buffer",
          "snippets",
        },
        markdown = {
          "markdown",
          "obsidian",
          "obsidian_new",
          "obsidian_tags",
          "lsp",
          "buffer",
          "snippets",
        },
      },
      providers = {
        dadbod = {
          name = "Dadbod",
          module = "vim_dadbod_completion.blink",
          score_offset = 100,
        },
        lazydev = {
          name = "LazyDev",
          module = "lazydev.integrations.blink",
          score_offset = 100,
        },
        markdown = {
          name = "RenderMarkdown",
          module = "render-markdown.integ.blink",
          fallbacks = { "lsp" },
          score_offset = 100,
        },
        obsidian = {
          name = "obsidian",
          module = "blink.compat.source",
          score_offset = 100,
        },
        obsidian_new = {
          name = "obsidian_new",
          module = "blink.compat.source",
          score_offset = 100,
        },
        obsidian_tags = {
          name = "obsidian_tags",
          module = "blink.compat.source",
          score_offset = 100,
        },
      },
    },
    completion = {
      accept = {
        auto_brackets = {
          enabled = true,
        },
      },
      menu = {
        draw = {
          treesitter = { "lsp" },
        },
      },
      documentation = {
        auto_show = true,
      },
      list = {
        selection = {
          auto_insert = true,
          preselect = false,
        },
      },
    },
    -- windows = {
    --   documentation = {
    --     auto_show = true,
    --   },
    --   autocomplete = {
    --     selection = "auto_insert",
    --   },
    -- },
    appearance = {
      nerd_font_variant = "mono",
      use_nvim_cmp_as_default = false,
      kind_icons = {
        Text = "󰉿",
        Method = "󰅩",
        Function = "",
        Constructor = "",

        Field = "",
        Variable = "",
        Property = "",

        Class = "󰠲",
        Interface = "",
        Struct = "",
        Module = "",

        Unit = "",
        Value = "",
        Enum = "",
        EnumMember = "",

        Keyword = "",
        Constant = "󰏿",

        Snippet = "",
        Color = "", -- 
        -- Color = "▅▅", -- ▅▆ Use block instead of icon for color items to make swatches more usable
        File = "",
        Reference = "",
        Folder = "",
        Event = "⚡",
        Operator = "",
        TypeParameter = "",
      },
    },
  },

  ---@param opts blink.cmp.Config
  config = function(_, opts)
    -- check if we need to override symbol kinds
    for _, provider in pairs(opts.sources.providers or {}) do
      ---@cast provider blink.cmp.SourceProviderConfig|{kind?:string}
      if provider.kind then
        local CompletionItemKind = require("blink.cmp.types").CompletionItemKind
        local kind_idx = #CompletionItemKind + 1

        CompletionItemKind[kind_idx] = provider.kind
        ---@diagnostic disable-next-line: no-unknown
        CompletionItemKind[provider.kind] = kind_idx

        ---@type fun(ctx: blink.cmp.Context, items: blink.cmp.CompletionItem[]): blink.cmp.CompletionItem[]
        local transform_items = provider.transform_items
        ---@param ctx blink.cmp.Context
        ---@param items blink.cmp.CompletionItem[]
        provider.transform_items = function(ctx, items)
          items = transform_items and transform_items(ctx, items) or items
          for _, item in ipairs(items) do
            item.kind = kind_idx or item.kind
          end
          return items
        end

        -- Unset custom prop to pass blink.cmp validation
        provider.kind = nil
      end
    end

    require("blink.cmp").setup(opts)
  end,
}
