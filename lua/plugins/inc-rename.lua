return {
  "smjonas/inc-rename.nvim",
  keys = {
    {
      "<leader>rn",
      function() return ":IncRename " .. vim.fn.expand("<cword>") end,
      desc = "Incremental rename",
      expr = true,
    },
  },
  cmd = { "IncRename" },
  opts = {
    save_in_cmdline_history = false,
  },
}
