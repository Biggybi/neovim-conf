local map = vim.keymap.set

local function get_cursor_line_col()
  local cursor_pos = vim.api.nvim_win_get_cursor(0)
  return cursor_pos[1], cursor_pos[2]
end

local function get_closest_non_empty_line(after)
  local line = vim.fn.line(".")
  return after and vim.fn.nextnonblank(line + 1) or vim.fn.prevnonblank(line - 1)
end

local function get_indent(after, line)
  local non_empty_line = get_closest_non_empty_line(after)
  local non_empty_line_indent = vim.fn.getline(non_empty_line):match("^%s*")
  local current_line_indent = line:match("^%s*")
  return #current_line_indent > #non_empty_line_indent and current_line_indent or non_empty_line_indent
end

local function surround_empty_lines()
  vim.cmd("normal o")
  local indent = get_indent(false, vim.fn.getline("."))
  vim.api.nvim_put({ indent }, "l", false, false)
  vim.cmd("startinsert")
end

local function surround_current_line(after, new_line, nline, line)
  local indent = get_indent(after, line)
  if new_line then vim.api.nvim_put({ "" }, "l", not after, after) end
  if not after then vim.api.nvim_win_set_cursor(0, { nline, 0 }) end
  vim.api.nvim_put({ "" }, "l", after, not after)
  vim.api.nvim_put({ indent }, "l", false, false)
  vim.cmd("startinsert!")
end

local function put_autoindent(string, after)
  after = after or false
  local reg_bak = vim.fn.getreg("0")
  vim.fn.setreg("0", string)
  vim.cmd('norm "0=' .. (after and "p" or "P"))
  vim.fn.setreg("0", reg_bak)
end

local function split(line, col_number, after, new_line)
  after = after or false
  new_line = new_line or false
  vim.api.nvim_set_current_line(line:sub(1, col_number))
  put_autoindent(line:sub(col_number + 1), after)
  if not new_line then
    vim.cmd("norm _")
    vim.cmd("startinsert")
    return
  end
  local cur_line = vim.fn.getline(".")
  vim.api.nvim_put({ cur_line:match("^%s*") }, "l", not after, false)
  vim.cmd("startinsert!")
end

local function new_line_or_split(after, new_line)
  after = after or false
  new_line = new_line or false
  local line = vim.fn.getline(".")

  local is_empty_line = line:match("^%s*$")
  if is_empty_line then
    surround_empty_lines()
    return
  end

  local cur_line, cur_col = get_cursor_line_col()
  local cursor_in_indent = cur_col - 1 < vim.fn.indent(".")
  if cursor_in_indent then
    surround_current_line(after, new_line, cur_line, line)
    return
  end

  split(line, cur_col, after, new_line)
end

map("n", "co", function() new_line_or_split(true, true) end, { desc = "New paragraph or split line up" })
map("n", "cO", function() new_line_or_split(false, true) end, { desc = "New paragraph or split line down" })
map("n", "c<c-o>", function() new_line_or_split(true, false) end, { desc = "New paragraph or split line up" })
map("n", "c<c-O>", function() new_line_or_split(false, false) end, { desc = "New paragraph or split line down" })
map("n", "c<a-o>", function() new_line_or_split(false, false) end, { desc = "New paragraph or split line down" })
