local M = {}

local find_files = function(opts) require("telescope.builtin").find_files(opts) end

function M.curdir() find_files({ prompt_title = vim.fn.fnamemodify(vim.fn.getcwd(), ":t") }) end
function M.home() find_files({ cwd = "$HOME", prompt_title = "Home" }) end
function M.root() find_files({ cwd = "/", prompt_title = "Root" }) end
function M.work() find_files({ cwd = "/data/work", prompt_title = "Work" }) end
function M.dot() find_files({ cwd = "$DOT", prompt_title = "Dotfiles" }) end
function M.nvim() find_files({ cwd = vim.fn.fnamemodify(vim.env.MYVIMRC, ":p:h"), prompt_title = "Neovim Config" }) end
function M.notes() find_files({ cwd = "~/Notes/second_brain/", prompt_title = "Notes" }) end

return M
