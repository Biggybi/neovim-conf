local layout_strategies = require("telescope.pickers.layout_strategies")
local resolve = require("telescope.config.resolve")
local p_window = require("telescope.pickers.window")

-- List of options that are shared by more than one layout.
local shared_options = {
  width = { "How wide to make Telescope's entire layout", "See |resolver.resolve_width()|" },
  height = { "How tall to make Telescope's entire layout", "See |resolver.resolve_height()|" },
  mirror = "Flip the location of the results/prompt and preview windows",
  scroll_speed = "The number of lines to scroll through the previewer",
  prompt_position = { "Where to place prompt window.", "Available Values: 'bottom', 'top'" },
  anchor = { "Which edge/corner to pin the picker to", "See |resolver.resolve_anchor_pos()|" },
  anchor_padding = {
    "Specifies an amount of additional padding around the anchor",
    "Values should be a positive integer",
  },
}

local get_border_size = function(opts)
  if opts.window.border == false then return 0 end

  return 1
end

local get_valid_configuration_keys = function(strategy_config)
  local valid_configuration_keys = {
    -- TEMP: There are a few keys we should say are valid to start with.
    preview_cutoff = true,
    prompt_position = true,
  }

  for key in pairs(strategy_config) do
    valid_configuration_keys[key] = true
  end

  for name in pairs(layout_strategies) do
    valid_configuration_keys[name] = true
  end

  return valid_configuration_keys
end

local function validate_layout_config(strategy_name, configuration, values, default_layout_config)
  assert(strategy_name, "It is required to have a strategy name for validation.")
  local valid_configuration_keys = get_valid_configuration_keys(configuration)

  -- If no default_layout_config provided, check Telescope's config values
  default_layout_config = vim.F.if_nil(default_layout_config, require("telescope.config").values.layout_config)

  local result = {}
  local get_value = function(k)
    -- skip "private" items
    if string.sub(k, 1, 1) == "_" then return end

    local val
    -- Prioritise options that are specific to this strategy
    if values[strategy_name] ~= nil and values[strategy_name][k] ~= nil then val = values[strategy_name][k] end

    -- Handle nested layout config values
    if layout_strategies[k] and strategy_name ~= k and type(val) == "table" then
      val = vim.tbl_deep_extend("force", default_layout_config[k], val)
    end

    if val == nil and values[k] ~= nil then val = values[k] end

    if val == nil then
      if default_layout_config[strategy_name] ~= nil and default_layout_config[strategy_name][k] ~= nil then
        val = default_layout_config[strategy_name][k]
      else
        val = default_layout_config[k]
      end
    end

    return val
  end

  -- Always set the values passed first.
  for k in pairs(values) do
    if not valid_configuration_keys[k] then
      -- TODO: At some point we'll move to error here,
      --    but it's a bit annoying to just straight up crash everyone's stuff.
      vim.api.nvim_err_writeln(
        string.format(
          "Unsupported layout_config key for the %s strategy: %s\n%s",
          strategy_name,
          k,
          vim.inspect(values)
        )
      )
    end

    result[k] = get_value(k)
  end

  -- And then set other valid keys via "inheritance" style extension
  for k in pairs(valid_configuration_keys) do
    if result[k] == nil then result[k] = get_value(k) end
  end

  return result
end

local function make_documented_layout(name, layout_config, layout)
  -- Save configuration data to be used by documentation
  layout_strategies._configurations[name] = layout_config

  -- Return new function that always validates configuration
  return function(self, max_columns, max_lines, override_layout)
    return layout(
      self,
      max_columns,
      max_lines,
      validate_layout_config(
        name,
        layout_config,
        vim.tbl_deep_extend("keep", vim.F.if_nil(override_layout, {}), vim.F.if_nil(self.layout_config, {}))
      )
    )
  end
end

local calc_size_and_spacing = function(cur_size, max_size, bs, w_num, b_num, s_num)
  local spacing = s_num * (1 - bs) + b_num * bs
  cur_size = math.min(cur_size, max_size)
  cur_size = math.max(cur_size, w_num + spacing)
  return cur_size, spacing
end

local function is_mirror(layout_config)
  if layout_config.mirror ~= nil then return layout_config.mirror end
  if not layout_config.anchor then
    return false
  elseif layout_config.anchor:find("S") then
    return true
  elseif layout_config.anchor:find("N") then
    return false
  else
    return false
  end
end

layout_strategies.prompt_center = make_documented_layout(
  "prompt_center",
  vim.tbl_extend("error", shared_options, {
    preview_height = "Height of the preview window",
    preview_cutoff = "When lines are less than this value, the preview will be disabled",
    results_height = "Height of the results window",
    prompt_height = "Height of the prompt window",
  }),
  function(self, max_columns, max_lines, layout_config)
    local initial_options = p_window.get_initial_window_options(self)
    local preview = initial_options.preview
    local results = initial_options.results
    local prompt = initial_options.prompt

    local bs = get_border_size(self)

    -- This sets the width for the whole layout
    local width = resolve.resolve_width(layout_config.width)(self, max_columns, max_lines)

    -- This sets the height for the whole layout
    local height_opt = layout_config.height or 25
    local height = resolve.resolve_height(height_opt)(self, max_columns, max_lines)

    if height > vim.o.lines - 2 then height = vim.o.lines - 4 * bs end

    local w_space
    -- Cap over/undersized width
    width, w_space = calc_size_and_spacing(width, max_columns, bs, 1, 2, 0)

    prompt.width = width - w_space
    results.width = width - w_space
    preview.width = width - w_space

    prompt.height = 1
    results.height = math.floor((height - prompt.height - 4 * bs) / 2)
    preview.height = results.height

    -- center prompt title
    if prompt.title then
      prompt.title = string.rep(" ", math.floor((prompt.width - #prompt.title) / 2)) .. prompt.title
    end
    if preview.title then
      preview.title = string.rep(" ", math.floor((preview.width - #preview.title) / 2)) .. preview.title
    end
    results.title = nil

    if not (max_lines >= layout_config.preview_cutoff) then preview.height = 0 end

    local padding_top = math.floor((vim.o.lines - height) / 2) + 1

    local mirror = is_mirror(layout_config)
    if not mirror then
      preview.line = math.max(padding_top, 2)
      prompt.line = preview.line + preview.height + 2
      results.line = prompt.line + prompt.height + 2
    else
      results.line = math.max(padding_top, 2)
      prompt.line = results.line + results.height + 2
      preview.line = prompt.line + prompt.height + 2
    end

    return {
      preview = self.previewer and preview.height > 0 and preview,
      results = results,
      prompt = prompt,
    }
  end
)
