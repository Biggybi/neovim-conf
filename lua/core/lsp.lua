M = {}

local map = vim.keymap.set
local augroup = vim.api.nvim_create_augroup
local autocmd = vim.api.nvim_create_autocmd

local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
end

-- initial state
vim.diagnostic.config({
  virtual_lines = false,
  virtual_text = {
    prefix = " ", -- Could be  ⵗ ☰ ❙ ❝◉ ◎ ● ▎❚ ⌷ x■ ▢ ◽◾◆ ◇ ▰ ▱ ▶ ▷ ▸ ▹ ◊ ⧫ ♢  ▢ 
  },
  severity_sort = true,
})

---@class Core.Lsp.Diagnostic
---@field config vim.diagnostic.Opts
---@field preview { bufnr: number|nil, winid: number|nil }
---@field is_open fun(self: Core.Lsp.Diagnostic): boolean
---@field close_preview fun(self: Core.Lsp.Diagnostic)
---@field open fun(self: Core.Lsp.Diagnostic)
---@field smart_preview fun(self: Core.Lsp.Diagnostic)
---@field jump fun(self: Core.Lsp.Diagnostic, count: number, show_preview: boolean)
---@field toggle_virtual_lines fun(self: Core.Lsp.Diagnostic)
---@field toggle_virtual_text fun(self: Core.Lsp.Diagnostic)
local diagnostic = {
  ---diagnostic configuration
  config = {
    virtual_lines = {
      current_line = false,
    },
    virtual_text = {
      prefix = " ", -- Could be  ⵗ ☰ ❙ ❝◉ ◎ ● ▎❚ ⌷ x■ ▢ ◽◾◆ ◇ ▰ ▱ ▶ ▷ ▸ ▹ ◊ ⧫ ♢  ▢ 
    },
    severity_sort = true,
  },

  ---current diagnostic preview state
  preview = { bufnr = nil, winid = nil },

  ---check if the diagnostic preview is open
  is_open = function(self)
    local preview_bufnr, winid = self.preview.bufnr, self.preview.winid
    return (preview_bufnr ~= nil and vim.api.nvim_buf_is_valid(preview_bufnr))
      or (winid ~= nil and vim.api.nvim_win_is_valid(winid))
  end,

  ---close the diagnostic preview
  close_preview = function(self)
    if self.preview.winid then pcall(vim.api.nvim_win_close, self.preview.winid, false) end
  end,

  ---open the diagnostic preview
  open = function(self)
    vim.schedule_wrap(function()
      local preview_bufnr, winid = vim.diagnostic.open_float({ scope = "line" })
      self.preview = { preview_bufnr = preview_bufnr, winid = winid }
    end)()
  end,

  ---reopen the diagnostic preview if it's already open
  smart_preview = function(self)
    if not self:is_open() then return end
    self:close_preview()
    self:open()
  end,

  ---jump to next/previous diagnostic, with optional preview
  ---@param count number
  ---@param show_preview boolean
  jump = function(self, count, show_preview)
    vim.diagnostic.jump({ count = count })
    show_preview = show_preview or false
    if show_preview then
      self:smart_preview()
      return
    end
    self:close_preview()
    self:open()
  end,

  toggle_virtual_lines = function(self)
    local enabled = vim.diagnostic.config().virtual_lines
    if enabled then
      vim.diagnostic.config({ virtual_lines = false })
      vim.notify("diagnostic virtual lines: off")
    else
      vim.diagnostic.config({ virtual_lines = self.config.virtual_lines })
      vim.notify("diagnostic virtual lines: on")
    end
  end,

  toggle_virtual_text = function(self)
    local enabled = vim.diagnostic.config().virtual_text
    if enabled then
      vim.diagnostic.config({ virtual_text = false })
      vim.notify("diagnostic virtual text: off")
    else
      vim.diagnostic.config({ virtual_text = self.config.virtual_text })
      vim.notify("diagnostic virtual text: on")
    end
  end,
}
setmetatable(diagnostic, { __index = diagnostic })

local function on_attach_keymaps()
  -- require("telescope").setup({})
  map({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, { buffer = 0, desc = "Code action" })

  map("n", "<leader><c-d>", diagnostic.open, { buffer = 0, desc = "Show diagnostics" })
  map({ "n", "x" }, "[d", function() diagnostic:jump(-1, true) end, { buffer = 0, desc = "Previous diagnostic" })
  map({ "n", "x" }, "]d", function() diagnostic:jump(1, true) end, { buffer = 0, desc = "Next diagnostic" })
  map("n", "[<c-d>", function() diagnostic:jump(-1, false) end, { buffer = 0, desc = "Next diagnostic (preview)" })
  map("n", "]<c-d>", function() diagnostic:jump(1, false) end, { buffer = 0, desc = "Previous diagnostic (preview)" })

  map(
    "n",
    "yuD",
    function() diagnostic:toggle_virtual_lines() end,
    { buffer = 0, desc = "Toggle diagnostic virtual lines" }
  )
  map(
    "n",
    "yud",
    function() diagnostic:toggle_virtual_text() end,
    { buffer = 0, desc = "Toggle diagnostic virtual text" }
  )

  map("n", "K", vim.lsp.buf.hover, { buffer = 0, desc = "Show hover" })
  map("n", "<leader>rs", "<cmd>LspRestart<CR>", { buffer = 0, desc = "Restart LSP" })
end

autocmd("LspAttach", {
  group = augroup("LspAttachKeyMap", {}),
  callback = on_attach_keymaps,
})

autocmd({ "LspAttach" }, {
  group = augroup("LspCodeLens", {}),
  callback = function(args)
    local client = vim.lsp.get_client_by_id(args.data.client_id)
    if client == nil or not client:supports_method("textDocument/codeLens") then return end
    vim.lsp.codelens.refresh({ bufnr = 0 })
  end,
})

return M
