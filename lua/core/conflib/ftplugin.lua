local M = {}

---@class conflib.ftplugin
---@field keys? table<string, string|string[]>
---@field options? string[]
---@field autocmds? number[]
---@field commands? string[]

function M.undo_init()
  ---@class conflib.ftplugin
  M.undo = {
    keys = {},
    options = {},
    autocmds = {},
    commands = {},
  }
end

M.undo_init()

function M.ftplugin_undoer()
  for key, mode in pairs(M.undo.keys) do
    vim.keymap.del(mode, key, { buffer = true })
  end
  for _, id in ipairs(M.undo.autocmds) do
    vim.api.nvim_del_autocmd(id)
  end
  for _, name in ipairs(M.undo.options) do
    vim.cmd(("setl %s<"):format(name))
  end
  for _, name in ipairs(M.undo.commands) do
    vim.api.nvim_buf_del_user_command(0, name)
  end
  M.undo_init()
  vim.b.conflib_ftplugin_loaded = nil
end

---@param cmd string -- vimscript command
function M.append_undo_ftplugin(cmd)
  if not cmd or cmd == "" then return end
  if vim.b.undo_ftplugin then
    vim.b.undo_ftplugin = vim.b.undo_ftplugin .. " | " .. cmd
  else
    vim.b.undo_ftplugin = cmd
  end
end

function M.set_undo_ftplugin()
  if vim.b.conflib_ftplugin_loaded == true then return end
  vim.b.conflib_ftplugin_loaded = true
  if not vim.b.undo_ftplugin then return end
  local undo_cmd = "lua require('core.conflib.ftplugin').ftplugin_undoer()"
  M.append_undo_ftplugin(undo_cmd)
end

---@param event any
---@param opts vim.api.keyset.create_autocmd
function M.set_autocmd(event, opts)
  M.set_undo_ftplugin()
  opts = vim.tbl_extend("force", {
    buffer = 0,
  }, opts or {})
  local id = vim.api.nvim_create_autocmd(event, opts)
  table.insert(M.undo.autocmds, id)
end

---@param name string
---@param value any
function M.set_option(name, value)
  M.set_undo_ftplugin()
  local scope = vim.api.nvim_get_option_info2(name, {}).scope
  local o = scope == "buf" and "bo" or scope == "win" and "wo" or "o"
  vim[o][name] = value
  table.insert(M.undo.options, name)
end

---@param mode string|string[]
---@param lhs string
---@param rhs string|function
---@param opts? vim.keymap.set.Opts
function M.set_keymap(mode, lhs, rhs, opts)
  M.set_undo_ftplugin()
  opts = vim.tbl_extend("force", { buffer = true }, opts or {})
  vim.keymap.set(mode, lhs, rhs, opts)
  M.undo.keys[lhs] = mode
end

---@param name string
---@param command string|function
---@param opts? vim.api.keyset.user_command
function M.set_command(name, command, opts)
  M.set_undo_ftplugin()
  vim.api.nvim_buf_create_user_command(0, name, command, opts or {})
  table.insert(M.undo.commands, name)
end

return M
