local actions = require("telescope.actions")
local pickers = require("telescope.pickers")
local finders = require("telescope.finders")
local conf = require("telescope.config").values

local function copy_to_clipboard(text) vim.fn.setreg("+", text) end

local function copy_to_system_clipboard(text) vim.fn.setreg('"', text) end

local function get_file_paths()
  local file_path = vim.fn.expand("%:p")
  local file_only = vim.fn.expand("%:t")
  local relative_to_cwd = vim.fn.expand("%")
  local relative_to_home = vim.fn.expand("~" .. vim.fn.expand("%:p:h"):gsub(vim.fn.expand("~"), ""))
  local absolute_path = vim.fn.expand("%:p")

  return {
    { label = "File Only", value = file_only },
    { label = "Path Relative to Current Directory", value = relative_to_cwd },
    { label = "Path Relative to Home", value = relative_to_home },
    { label = "Absolute Path", value = absolute_path },
  }
end

local function select_and_copy(prompt_bufnr)
  local selection = actions.get_selected_entry(prompt_bufnr)
  if selection then copy_to_clipboard(selection.value) end
  actions.close(prompt_bufnr)
end

local function select_and_copy_system_clipboard(prompt_bufnr)
  local selection = actions.get_selected_entry(prompt_bufnr)
  if selection then copy_to_system_clipboard(selection.value) end
  actions.close(prompt_bufnr)
end

local M = {}

M.file_paths = function()
  pickers
    .new({}, {
      prompt_title = "File Paths",
      finder = finders.new_table({
        results = get_file_paths(),
        entry_maker = function(entry)
          return {
            value = entry.value,
            display = entry.label,
          }
        end,
      }),
      sorter = conf.generic_sorter({}),
      attach_mappings = function(_, map)
        map("i", "<CR>", select_and_copy)
        map("n", "<CR>", select_and_copy)
        map("i", "<M-CR>", select_and_copy_system_clipboard)
        map("n", "<M-CR>", select_and_copy_system_clipboard)
        return true
      end,
    })
    :find()
end

return M
