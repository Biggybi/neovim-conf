local scratch_dir = vim.fn.stdpath("data") .. "/auto_scratch"
local scratchfiles_basename = "scratch_"
local max_scratch_files = 1000

local function is_new_scratch()
  return vim.bo.buftype == "" and vim.bo.filetype == "" and vim.api.nvim_buf_get_name(0) == ""
end

local function is_file(path) return vim.fn.filereadable(path) == 0 and vim.fn.isdirectory(path) == 0 end

local function get_next_scratch()
  for i = 1, max_scratch_files do
    local filename = string.format("%s%d", scratchfiles_basename, i)
    local fullpath = scratch_dir .. "/" .. filename
    if is_file(fullpath) then return fullpath end
  end
end

local function save_as_scratch()
  if not is_new_scratch() then return end
  local filename = get_next_scratch()
  if filename then
    vim.cmd("write " .. filename)
    vim.notify("Scratch file created at " .. filename)
  else
    vim.notify("No more scratch files available (max: " .. max_scratch_files .. ")")
  end
end

local function get_scratch_files()
  local files = vim.fn.glob(scratch_dir .. "/scratch*", false, true)
  return vim.tbl_filter(function(file) return vim.fn.filereadable(file) == 1 end, files)
end

local function restore_scratch_files()
  local files = get_scratch_files()
  for i, file in ipairs(files) do
    if i == 1 and vim.fn.argc() == 0 then
      vim.cmd("edit " .. file)
    else
      vim.cmd("tabedit " .. file)
    end
  end
end

local function delete_empty_scratch_files()
  local files = get_scratch_files()
  vim.tbl_map(function(file)
    if vim.fn.getfsize(file) == 0 then vim.fn.delete(file) end
  end, files)
end

vim.api.nvim_create_autocmd({ "BufModifiedSet" }, {
  group = vim.api.nvim_create_augroup("AutoScratchSave", {}),
  pattern = "*",
  callback = save_as_scratch,
})

vim.api.nvim_create_autocmd("VimEnter", {
  group = vim.api.nvim_create_augroup("AutoScratchRestore", {}),
  pattern = "*",
  callback = restore_scratch_files,
})

vim.api.nvim_create_autocmd("VimLeavePre", {
  group = vim.api.nvim_create_augroup("AutoScratchDeleteEmptyFile", {}),
  pattern = "*",
  callback = delete_empty_scratch_files,
})
