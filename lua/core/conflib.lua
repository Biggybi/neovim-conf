local M = {}

function M.del_qf_item(line)
  local items = vim.fn.getqflist()
  local title = vim.fn.getqflist({ title = 1 }).title
  table.remove(items, line)
  vim.fn.setqflist(items, "r")
  vim.fn.setqflist({}, "a", { title = title })
  local new_line = (line >= #items and #items > 0) and #items or line
  vim.api.nvim_win_set_cursor(0, { math.max(new_line, 1), 0 })
end

function M.del_qf_range()
  local start_pos = vim.fn.getpos("'[")
  local end_pos = vim.fn.getpos("']")
  local start_line = start_pos[2]
  local end_line = end_pos[2]
  for line = end_line, start_line, -1 do
    M.del_qf_item(line)
  end
end

---@class FeedKeycode.Opts
---@field mode? 'm' | 'n' | 't' | 'L' | 'i' | 'x' | '!'
---@field from_part? boolean
---@field do_lt? boolean
---@field special? boolean

---@param str string
---@param opts? FeedKeycode.Opts
-- feed a key from keycode (e.g. "<CR>")
-- useful for keymaps
function M.feed_keycode(str, opts)
  opts = opts or {}
  local mode = opts.mode or "n"
  local from_part = opts.from_part or true
  local do_lt = opts.do_lt or true
  local special = opts.special or true

  return vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(str, from_part, do_lt, special), mode, true)
end

function M.set_undo_ftplugin(cmd)
  if vim.b.undo_ftplugin then
    vim.b.undo_ftplugin = vim.b.undo_ftplugin .. " | " .. cmd
  else
    vim.b.undo_ftplugin = cmd
  end
end

function M.get_installed_plugins()
  local plugins = require("lazy").plugins()
  local plugin_names = {}

  for _, plugin in ipairs(plugins) do
    table.insert(plugin_names, plugin.name)
  end

  return plugin_names
end

function M.get_highlight_groups(pattern)
  local lines = vim.split(vim.api.nvim_exec2("highlight", { output = true }).output, "\n")
  local groups = vim.tbl_filter(function(line) return line:match(pattern) end, lines)
  local res = vim.tbl_map(function(line) return line:match("([%w_]+)%s+xxx") end, groups)
  return res
end

function M.search_text(text)
  local pattern = vim.fn.escape(text, "/")
  vim.fn.setreg("/", pattern)
  vim.cmd("set hlsearch")
end

function M.get_selection()
  local start_pos = vim.fn.getpos("'<")
  local end_pos = vim.fn.getpos("'>")
  local start_line = start_pos[2]
  local start_col = start_pos[3]
  local end_line = end_pos[2]
  local end_col = end_pos[3]

  local lines = vim.api.nvim_buf_get_lines(0, start_line - 1, end_line, false)
  if #lines > 1 then
    lines[1] = lines[1]:sub(start_col)
    lines[#lines] = lines[#lines]:sub(1, end_col)
  else
    lines[1] = lines[1]:sub(start_col, end_col)
  end
  local selection = table.concat(lines, "\n")
  return selection
end

function M.search_selection()
  vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<esc>", true, false, true), "n", true)
  vim.schedule(function() M.search_text(M.get_selection()) end)
end

return M
