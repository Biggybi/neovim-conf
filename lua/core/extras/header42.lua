-- # **************************************************************************** #
-- #                                                                              #
-- #                                                         :::      ::::::::    #
-- #    .zsh_aliases                                       :+:      :+:    :+:    #
-- #                                                     +:+ +:+         +:+      #
-- #    By: tris <tris@tristankapous.com>              +#+  +:+       +#+         #
-- #                                                 +#+#+#+#+#+   +#+            #
-- #    Created: 2021/03/04 19:16:30 by tris              #+#    #+#              #
-- #    Updated: 2021/03/04 19:16:30 by tris             ###   ########lyon.fr    #
-- #                                                                              #
-- # **************************************************************************** #

local M = {}

local options = {
  width = 80,
  border = {
    left = "#",
    right = "#",
    top = "*",
    bottom = "*",
  },
  padding = {
    top = 1,
    bottom = 1,
    left = 4,
    right = 4,
  },
  margin = {
    top = 2,
    bottom = 1,
  },
  user = {
    name = vim.fn.system("git config user.name") or vim.fn.system("whoami"),
    email = vim.fn.system("git config user.email") or "",
  },
}

local header_template = {
  "                                ",
  "{{FILENAME}}                    ",
  "                                ",
  "By: {{USER}} {{EMAIL}}          ",
  "                                ",
  "Created: {{CREATED}} by {{USER}}",
  "Updated: {{UPDATED}} by {{USER}}",
}

local ascii = {
  "        :::      ::::::::",
  "      :+:      :+:    :+:",
  "    +:+ +:+         +:+  ",
  "  +#+  +:+       +#+     ",
  "+#+#+#+#+#+   +#+        ",
  "     #+#    #+#          ",
  "    ###   ########lyon.fr",
}

-- local function replace_placeholders(line)
--   local user = options.user.name
--   local email = options.user.email
--   local created = vim.fn.strftime("%Y/%m/%d %H:%M:%S")
--   local updated = vim.fn.strftime("%Y/%m/%d %H:%M:%S")
--   local filename = vim.fn.expand("%:t")

--   line = string.gsub(line, "{{FILENAME}}", filename)
--   line = string.gsub(line, "{{USER}}", user)
--   line = string.gsub(line, "{{EMAIL}}", email)
--   line = string.gsub(line, "{{CREATED}}", created)
--   line = string.gsub(line, "{{UPDATED}}", updated)
--   return line
-- end

-- refactor `replace_placeholders` to use a table
local function replace_placeholders(line)
  local placeholders = {
    FILENAME = vim.fn.expand("%:t"),
    USER = options.user.name,
    EMAIL = options.user.email,
    CREATED = vim.fn.strftime("%Y/%m/%d %H:%M:%S"),
    UPDATED = vim.fn.strftime("%Y/%m/%d %H:%M:%S"),
  }

  for key, value in pairs(placeholders) do
    line = string.gsub(line, "{{" .. key .. "}}", value)
  end
  return line
end

local function comment_line(line)
  return string.format(vim.bo.commentstring, line)
  -- local commented_line = string.format(vim.bo.commentstring, line[1])
end

local function draw_border_line()
  return comment_line(options.border.left .. string.rep(options.border.top, options.width - 2) .. options.border.right)
end

local function draw_padding_line()
  return comment_line(options.border.left .. string.rep(" ", options.width - 2) .. options.border.right)
end

local function draw_middle_line(idx)
  local header_line = replace_placeholders(header_template[idx]):gsub("\n", "")
  local line = options.border.left
    .. string.rep(" ", options.padding.left)
    .. header_line
    .. string.rep(
      " ",
      options.width
        - #header_line
        - #ascii[idx]
        - options.padding.left
        - options.padding.right
        - #options.border.left
        - #options.border.right
    )
    .. ascii[idx]
    .. string.rep(" ", options.padding.right)
    .. options.border.right
  return comment_line(line)
end

local function get_header()
  local header_lines = {}
  for _ = 1, options.margin.top do
    table.insert(header_lines, "")
  end
  table.insert(header_lines, draw_border_line())
  for _ = 1, options.padding.top do
    table.insert(header_lines, draw_padding_line())
  end
  for idx, _ in ipairs(header_template) do
    table.insert(header_lines, draw_middle_line(idx))
  end
  for _ = 1, options.padding.bottom do
    table.insert(header_lines, draw_padding_line())
  end
  table.insert(header_lines, draw_border_line())
  for _ = 1, options.margin.bottom do
    table.insert(header_lines, "")
  end
  return header_lines
end

local function remove_empty_lines(input_lines)
  local trimmed = {}
  for _, line in ipairs(input_lines) do
    if line ~= nil and line ~= "" then table.insert(trimmed, line) end
  end
  return trimmed
end

local function header_is_set(header)
  local bufnr = vim.api.nvim_get_current_buf()
  local lines = vim.api.nvim_buf_get_lines(
    bufnr,
    0,
    #header + options.margin.top + options.padding.top + options.padding.bottom,
    false
  )
  local trimmed_header = remove_empty_lines(header)
  local trimmed_lines = remove_empty_lines(lines)
  print("DEBUGPRINT[3]: header42.lua:164: trimmed_lines=" .. vim.inspect(trimmed_lines))
  return trimmed_lines[1] == trimmed_header[1]
    and trimmed_lines[2] == trimmed_header[2]
    and trimmed_lines[-1] == trimmed_header[-1]
    and trimmed_lines[-2] == trimmed_header[-2]
end

-- local function header_is_set(new_header)
--   local bufnr = vim.api.nvim_get_current_buf()
--   local lines = vim.api.nvim_buf_get_lines(
--     bufnr,
--     0,
--     math.max(#header_template, #ascii) + options.padding.top + options.padding.bottom + options.margin.top + 2,
--     false
--   )
--   local trimmed_new_header = {}
--   for _, line in ipairs(new_header) do
--     if line ~= nil and line ~= "" then table.insert(trimmed_new_header, line) end
--   end
--   local trimmed_lines = {}
--   for _, line in ipairs(lines) do
--     if line ~= nil and line ~= "" then table.insert(trimmed_lines, line) end
--   end
--   if trimmed_lines[1] ~= trimmed_new_header[1] then return false end
--   if trimmed_lines[2] ~= trimmed_new_header[2] then return false end
--   if trimmed_lines[-2] ~= trimmed_new_header[-2] then return false end
--   if trimmed_lines[-1] ~= trimmed_new_header[-1] then return false end
--   return true
-- end

function M.put_header()
  local header = get_header()
  local bufnr = vim.api.nvim_get_current_buf()
  if header_is_set(header) then
    print("Header already exists")
    return
  end
  vim.api.nvim_buf_set_lines(bufnr, 0, 0, false, header)
end

vim.api.nvim_create_user_command("Header42", M.put_header, {})

return M
