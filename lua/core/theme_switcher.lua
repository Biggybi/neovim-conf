local M = {}

---@class ThemeConfig
---@field themes table<string, Theme>

---@class Theme
---@field gtk? string -- Name of the GTK theme
---@field background? string -- "light" or "dark"
---@field vim? string -- Name of the vim theme
---@field shell? string -- Path to the shell script that sets the theme
---@field callback? function -- Function to run when the theme is switched
---@field tmux? string -- Path to the tmux theme file

---@type ThemeConfig
M.config = {
  themes = {
    day = {
      gtk = "Adwaita",
      background = "light",
      vim = "tokyonight-day",
      shell = "~/dotfiles/shells/zsh/plugins/zsh-onehalf-shell/scripts/tokyonight_day",
      tmux = "~/dotfiles/tmux/colors/tokyonight_day",
    },
    night = {
      gtk = "Adwaita-dark",
      background = "dark",
      vim = "tokyonight-moon",
      shell = "~/dotfiles/shells/zsh/plugins/zsh-onehalf-shell/scripts/tokyonight_moon",
      tmux = "~/dotfiles/tmux/colors/tokyonight_moon",
    },
    storm = {
      gtk = "Adwaita-dark",
      background = "dark",
      vim = "tokyonight-storm",
      shell = "~/dotfiles/shells/zsh/plugins/zsh-onehalf-shell/scripts/tokyonight_moon",
      tmux = "~/dotfiles/tmux/colors/tokyonight_moon",
    },
  },
}

local function complete_themes(_, _, _)
  local themes = {}
  for theme, _ in pairs(M.config.themes) do
    table.insert(themes, theme)
  end
  return themes
end

local function complete_themes_gtk(_, _, _)
  local themes = {}
  for _, theme in pairs(M.config.themes) do
    if not vim.tbl_contains(themes, theme.gtk) then table.insert(themes, theme.gtk) end
  end
  return themes
end

local function complete_themes_vim(_, _, _)
  local themes = {}
  for _, theme in pairs(M.config.themes) do
    if not vim.tbl_contains(themes, theme.vim) then table.insert(themes, theme.vim) end
  end
  return themes
end

local function theme_name_is_dark(theme)
  local dark_theme_names = {
    "dark",
    "black",
    "night",
    "moon",
    "midnight",
    "dusk",
    "evening",
    "twilight",
    "sombre",
  }
  for _, dark_theme_name in ipairs(dark_theme_names) do
    if theme:match("[-_]" .. dark_theme_name:lower()) then return true end
  end
end

function M.gtk_switch_theme(theme, opts)
  if theme == nil then return end
  vim.system({
    "gsettings",
    "set",
    "org.gnome.desktop.interface",
    "gtk-theme",
    theme,
  })

  local dark = opts and opts.dark
  if dark == nil then dark = theme_name_is_dark(theme) end

  vim.system({
    "gsettings",
    "set",
    "org.gnome.desktop.interface",
    "color-scheme",
    dark and "prefer-dark" or "prefer-light",
  })
end

function M.switch_shell_theme(theme)
  print("DEBUGPRINT[1]: theme_switcher.lua:104: theme=" .. vim.inspect(theme))
  if theme == nil then return end
  -- if M.config.themes[theme] then theme = M.config.themes[theme].shell end
  vim.system({ "bash", "-c", "source", theme })
end

function M.switch_background(background)
  if background == nil then return end
  vim.o.background = background
end

function M.switch_vim_theme(theme)
  if theme == nil then return end
  vim.cmd("colorscheme " .. theme)
end

function M.switch_tmux_theme(theme)
  if theme == nil then return end
  -- if M.config.themes[theme] then theme = M.config.themes[theme].tmux end
  vim.cmd("!tmux source-file " .. theme)
end

function M.switch_theme(theme)
  -- if not os.getenv("WEZTERM_EXECUTABLE") then
  --   vim.print("source shell theme")
  M.switch_shell_theme(theme.shell)
  -- end
  M.gtk_switch_theme(theme.gtk, { dark = theme.background == "dark" })
  M.switch_background(theme.background)
  M.switch_vim_theme(theme.vim)
  M.switch_tmux_theme(theme.tmux)
  if theme.callback then theme.callback() end
end

vim.api.nvim_create_user_command("Day", function() M.switch_theme(M.config.themes.day) end, {})

vim.api.nvim_create_user_command("Night", function() M.switch_theme(M.config.themes.night) end, {})

function M.telescope_themes()
  local themes = {}
  for theme, k in pairs(M.config.themes) do
    table.insert(themes, vim.tbl_extend("force", { name = theme }, k))
  end
  return themes
end

function M.theme_switcher_telescope(opts)
  local pickers = require("telescope.pickers")
  local finders = require("telescope.finders")
  local previewers = require("telescope.previewers")
  pickers
    .new(opts, {
      finder = finders.new_table({
        results = M.telescope_themes(),
        entry_maker = function(entry)
          if entry.name == nil then return end
          return {
            value = entry,
            display = entry.name,
            ordinal = entry.name,
          }
        end,
      }),
      previewer = previewers.new_buffer_previewer({
        define_preview = function(self, entry)
          vim.api.nvim_buf_set_lines(self.state.bufnr, 0, 0, false, {
            "background: " .. entry.value.background,
            "gtk: " .. entry.value.gtk,
            "shell: " .. entry.value.shell,
            "vim: " .. entry.value.vim,
          })
        end,
      }),
      attach_mappings = function(prompt_bufnr)
        local actions = require("telescope.actions")
        local actions_state = require("telescope.actions.state")
        actions.select_default:replace(function()
          local selection = actions_state.get_selected_entry()
          actions.close(prompt_bufnr)
          M.switch_theme(selection.value)
        end)
        return true
      end,
    })
    :find()
end

function M.create_commands()
  vim.api.nvim_create_user_command("Theme", function(opts) M.switch_theme(M.config.themes[opts.args]) end, {
    nargs = 1,
    complete = complete_themes,
    desc = "Switch theme (gtk, shell, vim)",
  })
  vim.api.nvim_create_user_command("ThemeGTK", function(opts) M.gtk_switch_theme(opts.args) end, {
    nargs = 1,
    complete = complete_themes_gtk,
    desc = "Switch GTK theme",
  })
  vim.api.nvim_create_user_command("ThemeShell", function(opts) M.switch_shell_theme(opts.args) end, {
    nargs = 1,
    complete = complete_themes,
    desc = "Switch shell theme",
  })
  vim.api.nvim_create_user_command("ThemeVim", function(opts) M.switch_vim_theme(opts.args) end, {
    nargs = 1,
    complete = complete_themes_vim,
    desc = "Switch vim theme",
  })
  vim.api.nvim_create_user_command("ThemeVimBG", function(opts) M.switch_background(opts.args) end, {
    nargs = 1,
    complete = function() return { "light", "dark" } end,
    desc = "Switch background",
  })
  vim.api.nvim_create_user_command("ThemeTmux", function(opts) M.switch_tmux_theme(opts.args) end, {
    nargs = 1,
    complete = complete_themes,
    desc = "Switch tmux theme",
  })
end
M.create_commands()

return M
