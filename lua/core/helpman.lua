local M = {}

M.commands = {
  help = {
    vimcmd = "h",
    usercmd = "H",
    desc = "Help left vert split",
    complete = "help",
  },
  man = {
    vimcmd = "Man",
    usercmd = "M",
    desc = "Man left vert split",
    complete = "shellcmd",
  },
}

function M.create_cmd(cmds)
  for _, cmd in pairs(cmds) do
    vim.api.nvim_create_user_command(cmd.usercmd, function(opts)
      if vim.api.nvim_win_get_width(0) < 160 then vim.cmd(cmd.vimcmd .. " " .. opts.args) end
      vim.cmd("aboveleft vert " .. cmd.vimcmd .. " " .. opts.args)
    end, { nargs = 1, desc = cmd.desc, complete = cmd.complete })
  end
end

function M.create_autocmd()
  vim.api.nvim_create_autocmd("CmdLineChanged", {
    group = vim.api.nvim_create_augroup("helpman", {}),
    callback = function()
      if vim.fn.getcmdtype() ~= ":" then return end
      local cmdline = vim.fn.getcmdline()

      local function set_cmdline(cmd)
        if cmdline:match("^" .. cmd .. " ") then return end
        vim.fn.setcmdline(cmd .. " " .. cmdline:match("%s.*"):sub(2))
        vim.api.nvim_input("<c-a><right><right>")
      end

      if cmdline:match("^[Hh]e?l?p?%s") then set_cmdline("H") end
      if cmdline:match("^[Mm]a?n?%s") then set_cmdline("M") end
    end,
    desc = "HelpMan autocmd",
  })
end

vim.api.nvim_create_autocmd({ "FileType" }, {
  group = vim.api.nvim_create_augroup("VimConfKeyworkprg", {}),
  pattern = { "lua", "vim" },
  desc = "Set 'keywordprg' for (neo)vim config files",
  callback = function()
    local patterns = {
      vim.fn.stdpath("config"),
      "/data/work/neovim_plugins",
      vim.fn.expand("$HOME") .. "/.local/share/nvim/lazy",
    }
    for _, pattern in pairs(patterns) do
      if vim.api.nvim_buf_get_name(0):match(vim.pesc(tostring(pattern))) then
        vim.bo.keywordprg = ":H"
        return
      end
    end
  end,
})

M.create_cmd(M.commands)
M.create_autocmd()

return M
