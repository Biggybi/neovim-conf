local augroup = vim.api.nvim_create_augroup
local autocmd = vim.api.nvim_create_autocmd

-- mariasolos/execute_cmd_and_stay
autocmd("CmdwinEnter", {
  group = augroup("ExeKeepCmdLineWindow", {}),
  desc = "Execute command and stay in the command-line window",
  callback = function(args) vim.keymap.set({ "n", "i" }, "<S-CR>", "<cr>q:", { buffer = args.buf }) end,
})

autocmd({ "WinEnter", "BufEnter", "BufRead" }, {
  group = augroup("NumberToggle", {}),
  pattern = "*",
  callback = function()
    local special_buftype = {
      "help",
      "nofile",
    }
    local is_special = vim.tbl_contains(special_buftype, vim.bo.buftype)
    vim.wo.number = not is_special
  end,
})

autocmd({ "TermOpen" }, {
  group = augroup("TermOpen", {}),
  pattern = "*",
  callback = function()
    vim.wo.number = false
    vim.wo.relativenumber = false
    vim.wo.signcolumn = "no"
    vim.wo.sidescrolloff = 0
  end,
  desc = "Disable gutter in terminal",
})

autocmd({ "TextYankPost" }, {
  group = augroup("HighlightOnYank", {}),
  pattern = "*",
  callback = function() vim.highlight.on_yank() end,
})

autocmd({ "VimResized" }, {
  group = augroup("EqualizeSplits", {}),
  callback = function()
    local current_tab = vim.api.nvim_get_current_tabpage()
    vim.cmd("tabdo wincmd =")
    vim.api.nvim_set_current_tabpage(current_tab)
  end,
  desc = "Resize splits with terminal window",
})

-- :h restore-cursor
local function RestoreCursorPosition()
  if vim.buftype == "terminal" then return end
  local ft = vim.bo.filetype
  if
    not (ft:match("commit") or ft:match("rebase"))
    and vim.fn.line("'\"") > 1
    and vim.fn.line("'\"") <= vim.fn.line("$")
  then
    vim.cmd('normal! g`"')
  end
end

autocmd({ "BufRead" }, {
  group = augroup("RestoreCursor", {}),
  pattern = "*",
  callback = RestoreCursorPosition,
})
