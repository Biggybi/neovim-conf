local function is_uppercase(char) return char:match("%u") ~= nil end
local function is_lowercase(char) return char:match("%l") ~= nil end

local function charwise_find_next_range_start(line, start_col, comp)
  while not comp(line:sub(start_col, start_col)) and start_col < #line do
    start_col = start_col + 1
  end
  return start_col
end

local function charwise_find_range_end(line, start_col, comp)
  local end_col = start_col
  while comp(line:sub(end_col + 1, end_col + 1)) and end_col < #line do
    end_col = end_col + 1
  end
  return end_col
end

local function charwise_find_range_start(line, end_col, comp)
  local start_col = end_col
  while comp(line:sub(start_col - 1, start_col - 1)) and start_col > 0 do
    start_col = start_col - 1
  end
  return start_col
end

local function charwise_consecutive_range(comp)
  local line = vim.api.nvim_get_current_line()
  local current_col = vim.api.nvim_win_get_cursor(0)[2] + 1
  local start_col = current_col

  start_col = charwise_find_next_range_start(line, start_col, comp)
  if start_col >= #line then return end

  local end_col = charwise_find_range_end(line, start_col, comp)
  if start_col ~= current_col then return { start_col, end_col } end

  start_col = charwise_find_range_start(line, start_col, comp)
  return { start_col, end_col }
end

local function charwise_select_range(charwise_range)
  if not charwise_range then return end
  local curr_row = vim.api.nvim_win_get_cursor(0)[1]
  vim.fn.setpos("'<", { 0, curr_row, charwise_range[1], 0 })
  vim.fn.setpos("'>", { 0, curr_row, charwise_range[2], 0 })
  vim.cmd("normal! gv")
end

local function linewise_consecutive_range(start_line, comp)
  local end_line = start_line
  while comp(start_line - 1) do
    start_line = start_line - 1
  end
  while comp(end_line + 1) do
    end_line = end_line + 1
  end
  return { start_line, end_line }
end

local function get_mode_wise()
  local mode = vim.api.nvim_get_mode().mode
  local wise = mode:match("V") and "line" or mode:match("v") and "char" or mode:match("\22") and "block"
  return wise
end

local function select_range_force_linewise(linewise_range)
  if not linewise_range then return end
  local curr_col = vim.api.nvim_win_get_cursor(0)[2] + 1

  local mode = vim.api.nvim_get_mode().mode
  local is_pending_mode = mode:match("^no") ~= nil
  local is_visual_mode = mode:lower():match("^v") or mode == "\22" ~= nil

  local col_start = vim.fn.getpos("'<")[3]
  local col_end = vim.fn.getpos("'>")[3]
  if is_pending_mode then
    col_start = curr_col
    col_end = curr_col
  end

  vim.fn.setpos("'<", { 0, linewise_range[1], col_start, 0 })
  vim.fn.setpos("'>", { 0, linewise_range[2], col_end, 0 })
  if not is_pending_mode and not is_visual_mode then return end

  local wise = get_mode_wise()
  vim.cmd("normal! gv")
  mode = vim.api.nvim_get_mode().mode
  local new_wise = get_mode_wise()
  if wise == new_wise then return end
  if wise == "line" then
    vim.cmd("normal! V")
  elseif wise == "char" then
    vim.cmd("normal! v")
  elseif wise == "block" then
    vim.cmd('execute "normal! \\<C-v>"')
  end
end

vim.keymap.set({ "o", "x" }, "i<c-i>", function()
  local linenr = vim.fn.line(".")
  select_range_force_linewise(
    linewise_consecutive_range(linenr, function(line) return vim.fn.indent(line) == vim.fn.indent(linenr) end)
  )
end, { desc = "Select consecutive lines with same indent" })

vim.keymap.set({ "o", "x" }, "i<c-i>", function()
  local linenr = vim.fn.line(".")
  select_range_force_linewise(
    linewise_consecutive_range(linenr, function(line) return vim.fn.indent(line) == vim.fn.indent(linenr) end)
  )
end, { desc = "Select consecutive lines with same indent" })

vim.keymap.set({ "o", "x" }, "i<c-o>", function()
  local linenr = vim.fn.line(".")
  select_range_force_linewise(
    linewise_consecutive_range(linenr, function(line) return vim.fn.indent(line) == vim.fn.indent(linenr) end)
  )
end, { desc = "Select consecutive empty lines" })

vim.keymap.set(
  { "o", "x" },
  "aU",
  function() charwise_select_range(charwise_consecutive_range(is_uppercase)) end,
  { desc = "Select consecutive uppercase letters" }
)

vim.keymap.set(
  { "o", "x" },
  "iu",
  function() charwise_select_range(charwise_consecutive_range(is_lowercase)) end,
  { desc = "Select consecutive lowercase letters" }
)

vim.keymap.set(
  { "o", "x" },
  "au",
  function() charwise_select_range(charwise_consecutive_range(is_lowercase)) end,
  { desc = "Select consecutive lowercase letters" }
)
