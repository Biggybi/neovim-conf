-- Define a table with the file paths and corresponding keys
local file_paths = {
  { key = "bn", path = "$DOT/inputrc" },
  { key = "zh", path = "$HOME/.zsh_history" },
  { key = "za", path = "$DOT/shells/zsh/zsh_aliases" },
  { key = "zz", path = "$DOT/shells/zsh/zshrc" },
  { key = "bp", path = "$DOT/shells/bash/bash_profile" },
  { key = "ba", path = "$DOT/shells/bash/bash_aliases" },
  { key = "bb", path = "$DOT/shells/bash/bashrc" },
  { key = "a", path = "$DOT/shells/alacritty/alacritty.toml" },
  { key = "t", path = "$DOT/tmux/tmux.conf" },
  { key = "i", path = "~/.config/nvim/init.lua" },
  { key = "I", path = "~/.config/nvim/lua/trx/core/init.lua" },
  { key = "p", path = "~/.config/nvim/lua/trx/plugins/init.lua" },
  { key = "v", path = "$DOT/vim/vimrc" },
  { key = "n", path = "~/Notes/second_brain/todo.md" },
}

-- Create three mappings for each file that open it in:
-- * current split      <leader>e key
-- * vertical split     <leader><c-e> key
-- * horizontal split   <leader>E key
for _, file in ipairs(file_paths) do
  local file_key = file.key
  local path = file.path
  local prefixes = { "e", "<c-e>", "E" }

  for _, prefix in ipairs(prefixes) do
    local key = prefix .. file_key
    prefix = prefix == "e" and "e" or (prefix == "<c-e>" and "vs" or "sp")
    local command = string.format("<cmd>%s %s<CR>", prefix, path)
    vim.api.nvim_set_keymap("n", "<leader>" .. key, command, { noremap = true, silent = true })
  end
end
