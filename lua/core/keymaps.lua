vim.g.mapleader = " "

local fn = vim.fn
local cmd = vim.cmd

local map = vim.keymap.set
local unmap = vim.keymap.del

if vim.fn.maparg("grn", "n") ~= "" then unmap("n", "grn") end
if vim.fn.maparg("grr", "n") ~= "" then unmap("n", "grr") end
if vim.fn.maparg("gri", "n") ~= "" then unmap("n", "gri") end
if vim.fn.maparg("gra", "n") ~= "" then unmap({ "n", "x" }, "gra") end

map("n", "<leader>xi", function()
  vim.cmd("source $MYVIMRC")
  vim.print("Reloaded config")
end, { desc = "Source config" })

map("n", "<c-l>", function()
  vim.cmd([[nohlsearch|diffupdate|NoiceDismiss]])
  vim.cmd([[normal! <C-L>]])
end, { desc = "Redraw + close notifications" })

map("ca", "qqq", function()
  if vim.fn.getcmdtype() == ":" and vim.fn.getcmdline() == "qqq" then
    return "echom 'FUS ROH DAH'<CR>:qall!"
  else
    return "qqq"
  end
end, { expr = true, desc = "Quit all" })

map("s", "<BS>", "<BS>i", { desc = "Delete selection and start insert" })
map("s", "<c-h>", "<BS>i", { desc = "Delete selection and start insert" })

map("x", "<leader><c-g>", "<c-g>", { desc = "Select mode" })
map("t", "<M-[>", "<C-\\><C-n>", { desc = "Exit terminal" })

map("n", "'", "`", { desc = "Last jump (position)" })
map("n", "`", "'", { desc = "Last jump (line)" })

map({ "n", "x" }, "<C-;>", ":", { desc = "Command line" })
map({ "n", "x" }, "<M-;>", ":!", { desc = "System command line" })
map({ "n", "x" }, ";", ":", { desc = "Command line" })
map("n", "<M-Space>", ":", { desc = "Command line" })

map("n", ":", ";", { desc = "Next t/f search" })

map("i", "<c-z>", "<C-[><c-z>", { desc = "Vim to background" })

map("n", "<leader>K", "K", { desc = "Show documentation" })

map("n", "<leader><c-m>", ":make<CR>:botright copen<CR><c-w>p", { desc = "Make + quickfix list", silent = true })
map("n", "<leader>c<c-x>", "<cmd>make fclean<CR>", { desc = "Make fclean", silent = true })
map("n", "<leader>cx", "<cmd>make clean<CR>", { desc = "Make clean", silent = true })
map("n", "<leader>c<c-r>", "<cmd>make re<CR>", { desc = "Make re", silent = true })
map("n", "<leader>cr", "<cmd>make re<CR>", { desc = "Make re", silent = true })
map("n", "<leader>cm", "<cmd>make<CR>", { desc = "Make", silent = true })

map("n", "g<c-d>", "gd", { desc = "Jump to definition" })
map(
  { "n", "x" },
  "gd",
  -- find te first match of the word under the cursor in the current file
  function()
    local curline, curcol = unpack(vim.api.nvim_win_get_cursor(0))
    local word = vim.fn.expand("<cword>")
    local definition = vim.fn.matchbufline(vim.api.nvim_win_get_buf(0), ("\\<%s\\>"):format(word), 1, "$")[1]
    if definition.lnum ~= curline and definition.byteidx ~= curcol then
      vim.cmd("normal! m'")
      vim.api.nvim_win_set_cursor(0, { definition.lnum, definition.byteidx })
    end
  end,
  { desc = "Jump to definition" }
)
map("n", "g]", "g<c-]>", { desc = "Definition of symbol under cursor" })
map("n", "g<c-]>", "g]", { desc = "Definitions of symbol under cursor" })
map("n", "<c-w>:", ":argdo<space>")
map("n", "<c-w>;", ":windo<space>")

map("n", "<leader>vp", ":e ~/.config/nvim/lua/plugins/", { desc = "Edit plugins" })

map("n", "g*", "<cmd>let @/=expand('<cword>') <bar>set hlsearch<cr>", { desc = "Search string under cursor (no move)" })
map(
  "x",
  "*",
  require("core.conflib").search_selection,
  { desc = "search string under cursor (no move)", silent = true }
)
map(
  "n",
  "*",
  "<cmd>let @/= '\\<' . expand('<cword>') . '\\>' <bar>set hlsearch<cr>",
  { desc = "Search word under cursor (no move)" }
)

map("x", "<leader>gn", "gn", { desc = "Select next match" })
map("x", "<leader>gN", "gN", { desc = "Select prev match" })

map("x", "gn", "<esc>ngn", { desc = "Select next match" })
map("x", "gN", "<esc>NgN", { desc = "Select prev match" })

map("n", "<c-w>u", "<cmd>only<CR>", { desc = "Close other splits" })
map("n", "<c-w><c-u>", "<cmd>only<CR>", { desc = "Close other splits" })

map("n", "<c-w><c-i>", "<cmd>tabprevious<CR>", { desc = "Prev tab" })
map("n", "<c-w><c-o>", "<cmd>tabnext<CR>", { desc = "Next tab" })
map("n", "<m-i>", "<cmd>tabprevious<CR>", { desc = "Prev tab" })
map("n", "<m-o>", "<cmd>tabnext<CR>", { desc = "Next tab" })
map("n", "<m-p>", "<cmd>tabnext#<CR>", { desc = "Last used tab" })

map(
  "n",
  "<M-O>",
  function() cmd(fn.tabpagenr() == fn.tabpagenr("$") and "tabmove 0" or "tabmove +1") end,
  { desc = "Move tab forward" }
)
map(
  "n",
  "<M-I>",
  function() cmd(fn.tabpagenr() == 1 and "tabmove $" or "tabmove -1") end,
  { desc = "Move tab backward" }
)
map(
  "n",
  "<leader>O",
  function() cmd(fn.tabpagenr() == fn.tabpagenr("$") and "tabmove 0" or "tabmove +1") end,
  { desc = "Move tab forward" }
)
map(
  "n",
  "<leader>I",
  function() cmd(fn.tabpagenr() == 1 and "tabmove $" or "tabmove -1") end,
  { desc = "Move tab backward" }
)

map(
  "n",
  "<leader>cq",
  function() cmd(fn.getqflist({ ["winid"] = 1 }).winid == 0 and "copen | wincmd p" or "cclose") end,
  { desc = "Toggle quickfix" }
)

map(
  "n",
  "yoq",
  function() cmd(fn.getqflist({ ["winid"] = 1 }).winid == 0 and "copen | wincmd p" or "cclose") end,
  { desc = "Toggle quickfix" }
)

map("n", "<leader>ci", "<cmd> cnewer <cr>", { desc = "Next quickfix list" })
map("n", "<leader>co", "<cmd> colder <cr>", { desc = "Previous quickfix list" })

map({ "n", "t" }, "<m-L>", "<cmd> vertical resize +1 <CR>", { desc = "Increase vertical size" })
map({ "n", "t" }, "<m-K>", "<cmd> resize +1 <CR>", { desc = "Increase horizontal size" })
map({ "n", "t" }, "<m-J>", "<cmd> resize -1 <CR>", { desc = "Decrease horizontal size" })
map({ "n", "t" }, "<m-H>", "<cmd> vertical resize -1 <CR>", { desc = "Decrease vertical size" })

map("n", "<m-h>", "<cmd> wincmd h <cr>", { desc = "To left split" })
map("n", "<m-j>", "<cmd> wincmd j <cr>", { desc = "To below split" })
map("n", "<m-k>", "<cmd> wincmd k <cr>", { desc = "To above split" })
map("n", "<m-l>", "<cmd> wincmd l <cr>", { desc = "To right split" })

map("x", "<m-l>", ">gv", { desc = "Selection indent" })
map("x", "<m-h>", "<gv", { desc = "Selection unindent" })
map("x", ">", ">gv", { desc = "Selection indent" })
map("x", "<", "<gv", { desc = "Selection unindent" })

map({ "n", "x" }, "<c-w><space>t", "<cmd> tabnew# <CR>", { desc = "Tab alternate file" })
map({ "n", "x" }, "<c-w><space>s", "<cmd> sp# <CR>", { desc = "Split alternate file" })
map({ "n", "x" }, "<c-w><space>v", "<cmd> vs# <CR>", { desc = "Vert alternate file" })
map({ "n", "x" }, "<c-w><space><space>", "<cmd> vs# <CR>", { desc = "Vert alternate file" })
map({ "n", "x" }, "<c-w>#", "<cmd> vs# <CR>", { desc = "Vert alternate file" })
map({ "n", "x" }, "<leader><space>", "<c-6>", { desc = "Alternate file" })

map("n", "<leader>[", "<cmd> bp <CR>", { desc = "Prev buffer" })
map("n", "<leader>]", "<cmd> bn <CR>", { desc = "Next buffer" })

local function search_empty_line(direction) return vim.fn.search("^\\s*$", direction == "up" and "nbW" or "nW") end

local function goto_empty_line(direction)
  local new_line = search_empty_line(direction)
  if new_line == 0 then
    vim.cmd(direction == "up" and "normal! gg" or "normal! G")
    return
  end
  vim.cmd("normal! " .. new_line .. "gg")
end

map({ "n", "o", "x" }, "<c-k>", function() goto_empty_line("up") end, { desc = "Prev empty line" })
map({ "n", "o", "x" }, "<c-j>", function() goto_empty_line("down") end, { desc = "Next empty line" })

-- map('x', '<c-l>', [[:<c-u>call <sid>searchEndStartWord('W')<CR>]])
-- map('x', '<c-h>', [[:<c-u>call <sid>searchEndStartWord('Wb')<CR>]])
-- map('n', '<c-l>', [[:<c-u>call <sid>searchEndStartWord('W')<CR>]])
-- map('n', '<c-h>', [[:<c-u>call <sid>searchEndStartWord('Wb')<CR>]])

map({ "n", "x" }, "k", 'v:count ? "k" : "gk"', { silent = true, expr = true })
map({ "n", "x" }, "j", 'v:count ? "j" : "gj"', { silent = true, expr = true })

map({ "n", "x" }, "<leader>i", function() vim.cmd("norm! zb") end, { desc = "Scroll to bottom of screen" })
map({ "n", "x" }, "<leader>o", function() vim.cmd("norm! zt") end, { desc = "Scroll to top of screen" })
map("n", "gM", "M", { desc = "Middle of screen" })
map("n", "gL", "L", { desc = "Bottom of screen" })
map("n", "gH", "H", { desc = "Top of screen" })

map({ "n", "x", "o" }, "L", "g_", { desc = "End of line" })
map({ "n", "x", "o" }, "H", "^", { desc = "Start of line" })

map("n", "<leader>gl", function()
  vim.cmd("vert terminal git --no-pager log --all --decorate --oneline --graph")
  vim.cmd("stopinsert")
end, { desc = "Git pretty log" })

map("n", "<leader>sf", function()
  vim.cmd("filetype detect")
  vim.notify(vim.bo.filetype, 2, { title = "filetype detect" })
end, { desc = "Filetype detect" })

map("n", "<leader>sr", function()
  vim.cmd("silent Runtime")
  vim.notify(vim.bo.filetype, 2, { title = "filetype detect" })
end, { desc = "Runtime + filetype detect" })

map("n", "<c-w><c-d>", "<cmd>Lexplore <Cfile><CR>", { desc = "Explore file under cursor" })
map("n", "<c-w><c-f>", "<cmd>vertical wincmd f<CR>", { desc = "Vertical file under cursor" })
map("n", "<c-w><c-n>", "<cmd>vertical new<CR>", { desc = "Vertical new file" })

map("n", "<leader>cd", '<cmd>!cd &pwd<CR>:echo "shell cd : " . getcwd()', { desc = "Set shell path to with vim" })
map("n", "C", "c$", { desc = "Change to end of line" })

map("n", "g<c-r>", ":%s/<c-r><c-w>//g<Left><Left>", { desc = "Replace word under cursor" })
map("n", "gR", ":%s///g<Left><Left>", { desc = "Replace last search (whole buffer)" })
map("x", "gr", ":s///g<Left><Left>", { desc = "Replace last search" })
map("n", "gr", ":s///g<Left><Left>", { desc = "Replace last search (line)" })

map("n", "<leader>z=", "z=1<CR><CR>", { desc = "Auto correct word" })
map("x", "<leader>y", '"+y', { desc = "Yank to system clipboard" })
map("n", "<leader>y", '"+y', { desc = "Yank to system clipboard" })

map("n", "<leader>p", function()
  if vim.fn.line("$") == 1 then
    vim.cmd('normal! "+[p')
    return
  end
  local is_empty_curr_line = vim.api.nvim_get_current_line():match("^%s*$")
  if is_empty_curr_line then
    vim.cmd('normal! "+[pO')
    return
  end
  vim.cmd('normal! "+]p')
end, { desc = "Paste system clipboard" })

map("n", "<leader>P", function()
  if vim.fn.line("$") == 1 then
    vim.cmd('normal! "+[P')
    return
  end
  local is_empty_curr_line = vim.api.nvim_get_current_line():match("^%s*$")
  if is_empty_curr_line then
    vim.cmd('normal! "+[P')
    return
  end
  vim.cmd('normal! "+]P')
end, { desc = "Paste system clipboard" })

map("n", "<leader><c-p>", function()
  if vim.fn.line("$") == 1 then
    local new_lines = {}
    for line in (vim.fn.getreg("+") .. "\n"):gmatch("([^\n]+)") do
      table.insert(new_lines, line)
    end
    vim.api.nvim_buf_set_lines(0, 0, -1, false, new_lines)
    return
  end
  local is_empty_curr_line = vim.api.nvim_get_current_line():match("^%s*$")
  if is_empty_curr_line then
    vim.cmd('normal! dd"+]P')
    return
  end
  vim.cmd('normal! "+]p')
end, { desc = "Paste system clipboard" })

map("x", "<leader>p", '"+]p', { desc = "Paste system clipboard" })
map({ "n", "x" }, "D", '"_d', { desc = "Delete without yank" })
map({ "n", "o" }, "gp", "<cmd>normal! `[v`]<cr>", { desc = "Select last paste" })
map("x", "gp", "`[o`]", { desc = "Select last paste" })

map("n", "<leader>J", "i<c-m><esc>ge", { desc = "Split line" })

map("n", "<c-q>", "<cmd>silent! mode<CR>", { desc = "Force redraw" })

map("c", "<s-tab>", "<up>", { desc = "Up" })
map("c", "<c-o>", "<up>", { desc = "Up" })
map("c", "<tab>", "<down>", { desc = "Down" })
map("c", "<c-i>", "<down>", { desc = "Down" })
map("c", "<c-p>", "<up>", { desc = "Up" })
map("c", "<c-n>", "<down>", { desc = "Down" })
map("c", "<c-b>", "<left>", { desc = "Left" })
map("c", "<c-f>", "<right>", { desc = "Right" })

map("c", "<m-h>", "<left>", { desc = "Left" })
map("c", "<m-j>", "<down>", { desc = "Down" })
map("c", "<m-k>", "<up>", { desc = "Up" })
map("c", "<m-l>", "<right>", { desc = "Right" })

map("c", "<c-e>", "<end>", { desc = "End of line" })
map("c", "<c-q>", "<c-f>", { desc = "Ex mode" })
map("c", "<c-l>", "<del>", { desc = "Delete" })

map("c", "<c-r><c-s>", "%!sudo tee > /dev/null %", { desc = "Write file as root" })
map("n", "<leader>sw", "w !sudo tee %<cr>", { desc = "Sudo write" })

map("c", "<c-a>", function()
  local index = vim.fn.getcmdline():match("^IncRename%s+")
  if index and vim.fn.getcmdpos() > #index + 1 then
    return "<home>" .. string.rep("<right>", #index)
  else
    return "<home>"
  end
end, { desc = "Start of line", expr = true })

map(
  "c",
  "<c-x>",
  function() vim.fn.setcmdline(vim.fn.getcmdline():sub(1, vim.fn.getcmdpos() - 1)) end,
  { desc = "Delete till end of command line" }
)

map("c", "<c-r>.", "<c-r>=fnameescape(expand(('%:h')))..'/'<cr>", { desc = "Insert current file path", silent = true })

map("i", "<c-e>", "pumvisible() ? '<c-e>' : '<c-o>$'", { expr = true, desc = "End of line or cancel suggestion" })
map("i", "<c-a>", "<c-o>^", { desc = "Start of line" })

map("i", "<c-f>", "<Right>", { desc = "Right" })
map("i", "<c-b>", "<Left>", { desc = "Left" })

map("i", "<m-h>", "<Left>", { desc = "Left" })
map("i", "<m-j>", "<Down>", { desc = "Down" })
map("i", "<m-k>", "<Up>", { desc = "Up" })
map("i", "<m-l>", "<Right>", { desc = "Right" })
map("i", "<c-l>", "<del>", { desc = "Delete" })

map("o", "ag", "<cmd>normal! ggVG<cr>", { desc = "Whole buffer" })
map("x", "ag", "<cmd>normal! ggoG<cr>", { desc = "Whole buffer" })

map("o", "<c-l>", "<cmd>normal! ^v$h<cr>", { desc = "Line (without indent)" })
map("x", "<c-l>", "<cmd>normal! ^o$h<cr>", { desc = "Line (without indent)" })
map("o", "<c-h>", "<cmd>normal! 0v$h<cr>", { desc = "Line (with indent)" })
map("x", "<c-h>", "<cmd>normal! 0o$h<cr>", { desc = "Line (with indent)" })
