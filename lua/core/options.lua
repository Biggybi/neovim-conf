local o = vim.opt

vim.fn.setenv("DOT", "~/dotfiles/")
vim.g.termdebug_wide = 40
o.guifont = "JetBrainsMono NF:14"

-- 2 moving around, searching and patterns
o.startofline = false
o.whichwrap = "<,>,h,l,[,]"
o.magic = true
o.ignorecase = true
o.smartcase = true
o.jumpoptions = "view"

-- 4 displaying text
o.hidden = true
o.smoothscroll = true
o.scrolloff = 4
o.sidescroll = 1
o.sidescrolloff = 5
o.listchars = "tab:>·,trail:-,leadmultispace:│ "
o.fillchars = "vert:│,eob: "
o.cmdheight = 0
o.conceallevel = 0
o.number = true
o.wrap = true
o.breakindent = true

vim.api.nvim_create_autocmd({ "OptionSet" }, {
  group = vim.api.nvim_create_augroup("SetListChars", {}),
  pattern = "shiftwidth",
  callback = function()
    vim.wo.listchars = "tab:>·,trail:-,leadmultispace:│" .. string.rep(" ", vim.bo.shiftwidth - 1)
  end,
})

-- 5 syntax, highlighting and spelling
o.spellcapcheck = ""
o.signcolumn = "yes"
o.cursorline = true
o.cursorlineopt = "number"

-- 6 multiple windows
o.splitbelow = true
o.splitkeep = "screen"
o.splitright = true
o.switchbuf = "usetab"
o.laststatus = 2

-- 7 multiple tab pages
o.tabclose = "left"
o.tabpagemax = 30
o.showtabline = 2

-- 8 terminal
o.titleold = ""
o.title = true

-- 10 messages and info
o.errorbells = false
o.verbose = 0
o.shortmess:append("scFWxwrnmlifA")
o.showmode = false
o.showcmdloc = "statusline"

-- 12 editing text
o.backspace = "indent,eol,start"
o.joinspaces = false
o.pumheight = 10
o.nrformats = "hex,blank"
o.completeopt:append({ "longest", "menuone" })

vim.api.nvim_create_autocmd({ "BufEnter", "BufNewFile" }, {
  group = vim.api.nvim_create_augroup("SetFormatOptions", {}),
  pattern = "*",
  callback = function() o.formatoptions = "jql" end,
})

-- 13 tabs and indenting
o.shiftwidth = 2
o.softtabstop = 2
o.tabstop = 2
o.expandtab = true
o.smarttab = true
o.shiftround = true
o.autoindent = true
o.smartindent = true

-- 14 folding
o.display = "lastline"
o.foldnestmax = 2
o.foldmethod = "syntax"
o.foldenable = false

-- 15 diff mode
o.diffopt:append({ "vertical" })

-- 16 mapping
o.timeout = false
o.ttimeout = true
-- o.ttimeoutlen = 10

-- 17 reading and writing files
o.autoread = true
o.autowriteall = true
o.modelineexpr = true

-- 18 the swap file
o.updatetime = 500

-- 19 command line editing
o.history = 10000
o.wildoptions = "pum"
o.wildmode = "longest:full,full"
o.wildchar = string.byte("\t")
o.fileignorecase = true
o.wildignorecase = true
o.wildignore:append({
  "*.swp",
  "*.pyc",
  "*.o",
  "**/build/**",
  "**/dist/**",
  "**/node_modules/**",
  "**/venv/**",
  "**/__pycache__/**",
})

-- 22 language specific
o.iskeyword:append({ "-" })

-- 24 various
o.viewoptions = "folds,cursor"
o.sessionoptions = "blank,buffers,folds,help,tabpages,winsize,terminal"
o.virtualedit = "block"
o.winblend = 10
o.pumblend = 10
o.mousescroll = "ver:1,hor:4"
