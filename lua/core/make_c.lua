local M = {}

M.config = {
  run_cmd = "split | term",
}

local conflib = require("core.conflib")

function M.has_makefile() return conflib.file_is_file("Makefile") end

---@param rule string? -- make rule to run
---@param fallback function? -- function to run if no Makefile is found
function M.make(rule, fallback)
  if not M.has_makefile() then
    if fallback then
      vim.notify("No Makefile -> fallback", vim.log.levels.INFO)
      fallback()
      return
    end
    vim.notify("No Makefile -> implicit %<", vim.log.levels.INFO)
    vim.cmd("make %<")
    return
  end

  if rule then
    vim.cmd("make " .. rule)
    return
  end

  vim.cmd("make")
end

function M.file_clean()
  local ok, _ = pcall(function() vim.fs.rm(vim.fn.expand("%<")) end)
  if not ok then
    vim.notify("Failed to remove binary", vim.log.levels.WARN)
    return
  end
  vim.notify("Removed binary", vim.log.levels.INFO)
end

function M.file_make() vim.cmd("make %<") end

function M.file_re()
  M.file_clean()
  M.file_make()
end

function M.file_all()
  M.file_clean()
  M.file_make()
  M.file_run()
end

function M.file_run() vim.cmd(M.config.run_cmd .. " ./%<") end

---@param cmd string
function M.file_exe(cmd)
  local bin = vim.fn.expand("%<")
  if not conflib.file_is_file(bin) then
    vim.notify("Binary not found", vim.log.levels.WARN)
    return
  end
  cmd = ("%s %s -- ./%s"):format(M.config.run_cmd, cmd, bin)
  vim.cmd(cmd)
end

function M.make_clean() M.make("clean", M.file_clean()) end
function M.make_run() M.make("run", M.file_run()) end
function M.make_debug() M.make("debug", M.file_exe("gdb")) end
function M.make_valgrind() M.make("valgrind", M.file_exe("valgrind")) end
function M.make_bear() M.make("bear", M.file_exe("bear")) end
function M.make_re() M.make("re", M.file_re()) end
function M.make_all() M.make("all", M.file_all()) end
function M.make_test() M.make("test") end

return M
