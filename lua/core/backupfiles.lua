local fn = vim.fn
local o = vim.opt

-- 1000 oldfiles, 1000 lines, 100 KiB
o.shada = "!,'1000,<1000,s100,h"

o.undofile = true
if vim.env.SUDO_USER then
  o.swapfile = false
  o.backup = false
  o.writebackup = false
  o.undofile = false
  o.viminfo = ""
  o.verbosefile = ""
else
  local tmppath = vim.fn.stdpath("state")
  o.backupdir = tmppath .. "/backup//"
  o.directory = tmppath .. "/swap//"
  o.undodir = tmppath .. "/undo//"
  o.viewdir = tmppath .. "/view//"
  o.verbosefile = tmppath .. "/verbose.log"
  o.shadafile = tmppath .. "/viminfo/nviminfo"
end

local state_dirs = { vim.o.backupdir, vim.o.directory, vim.o.undodir, vim.o.viewdir }
for _, d in ipairs(state_dirs) do
  if fn.isdirectory(d) == 0 then fn.mkdir(d, "p") end
end
