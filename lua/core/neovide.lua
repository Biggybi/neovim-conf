if vim.g.neovide == nil then return end

-- neovide
vim.g.neovide_cursor_trail_size = 0.4
vim.g.neovide_hide_mouse_when_typing = true

-- toggle fullscren
vim.keymap.set(
  "n",
  "<M-CR>",
  function() vim.g.neovide_fullscreen = not vim.g.neovide_fullscreen end,
  { desc = "Toggle fullscreen" }
)

-- font size

local default_neovide_scale_factor = 0.9
vim.g.neovide_scale_factor = default_neovide_scale_factor
local scale_font = function(delta) vim.g.neovide_scale_factor = vim.g.neovide_scale_factor * delta end
vim.keymap.set("n", "<C-=>", function() scale_font(1.05) end, { desc = "Increase neovide font size" })
vim.keymap.set("n", "<C-->", function() scale_font(1 / 1.05) end, { desc = "Decrease neovide font size" })
vim.keymap.set(
  "n",
  "<C-0>",
  function() vim.g.neovide_scale_factor = default_neovide_scale_factor end,
  { desc = "Reset neovide font size" }
)

-- transparency
local default_neovide_transparency = 0.97
vim.g.neovide_transparency = default_neovide_transparency

local scale_transparency = function(delta)
  local transparency = vim.g.neovide_transparency * delta
  if transparency <= 0 and transparency < vim.g.neovide_transparency then transparency = 0 end
  if transparency >= 1 and transparency > vim.g.neovide_transparency then transparency = 1 end

  if delta > 1 and transparency < 0.1 then transparency = 0.1 end

  vim.g.neovide_transparency = transparency
end

local function get_transparency_scale(up)
  local scale = (1 - vim.g.neovide_transparency) * 0.1
  if scale < 0.005 then scale = 0.005 end
  if scale > 0.999 then scale = 1 end
  return up and 1 + scale or 1 - scale
end

local function scale_transparency_up() scale_transparency(get_transparency_scale(true)) end
local function scale_transparency_down() scale_transparency(get_transparency_scale(false)) end

-- change transparency
vim.keymap.set("n", "<M-=>", function() scale_transparency_up() end, { desc = "Increase transparency" })
vim.keymap.set("n", "<M-->", function() scale_transparency_down() end, { desc = "Decrease transparency" })
vim.keymap.set("n", "<M-+>", function() scale_transparency(1.05) end, { desc = "Increase transparency" })
vim.keymap.set("n", "<M-_>", function() scale_transparency(1 / 1.05) end, { desc = "Decrease transparency" })
vim.keymap.set(
  "n",
  "<M-0>",
  function() vim.g.neovide_transparency = default_neovide_transparency end,
  { desc = "Reset transparency" }
)

-- copy/paste system clipboard

vim.keymap.set({ "s" }, "<C-S-C>", '<c-o>"+ygv<c-g>', { desc = "Copy system" })
vim.keymap.set({ "s" }, "<C-S-V>", '<c-o>"+pgv<c-g>', { desc = "Paste system" })
vim.keymap.set({ "n", "x" }, "<C-S-C>", '"+y', { desc = "Copy system" })

vim.keymap.set({ "n", "x", "i", "c", "t" }, "<C-S-v>", function() vim.api.nvim_paste(vim.fn.getreg("+"), true, -1) end)
vim.keymap.set({ "s" }, "<C-S-V>", '<c-o>"+pgv<c-g>', { desc = "Paste system" })

vim.g.neovide_padding_bottom = 5
vim.g.neovide_padding_right = 5
vim.g.neovide_padding_left = 5
vim.g.neovide_padding_top = 5

-- vim.g.neovide_background_color              --
-- vim.g.neovide_channel_id                    --  1
-- vim.g.neovide_confirm_quit                  --  true
-- vim.g.neovide_cursor_animate_command_line   --  true
-- vim.g.neovide_cursor_animate_in_insert_mode --  true
-- vim.g.neovide_cursor_antialiasing           --  true
-- vim.g.neovide_cursor_distance_length_adjust --  true
-- vim.g.neovide_cursor_trail_size             --  0.7
-- vim.g.neovide_cursor_vfx_mode               --
-- vim.g.neovide_cursor_vfx_particle_curl      --  1.0
-- vim.g.neovide_cursor_vfx_particle_density   --  7.0
-- vim.g.neovide_cursor_vfx_particle_lifetime  --  1.2
-- vim.g.neovide_cursor_vfx_particle_phase     --  1.5
-- vim.g.neovide_cursor_vfx_particle_speed     --  10.0
-- vim.g.neovide_debug_renderer                --  false
-- vim.g.neovide_floating_blur                 --  true
-- vim.g.neovide_floating_blur_amount_y        --  2.0
-- vim.g.neovide_floating_z_height             --  10.0
-- vim.g.neovide_hide_mouse_when_typing        --  false
-- vim.g.neovide_idle                          --  true
-- vim.g.neovide_input_ime                     --  true
-- vim.g.neovide_input_macos_alt_is_meta       --  false
-- vim.g.neovide_light_radius                  --  5.0
-- vim.g.neovide_padding_bottom                --  0
-- vim.g.neovide_padding_right                 --  0
-- vim.g.neovide_padding_top                   --  0
-- vim.g.neovide_position_animation_length     --  0.15
-- vim.g.neovide_refresh_rate                  --  60
-- vim.g.neovide_refresh_rate_idle             --  5
-- vim.g.neovide_remember_window_position      --  true
-- vim.g.neovide_scroll_animation_far_lines    --  1
-- vim.g.neovide_scroll_animation_length       --  0.3
-- vim.g.neovide_theme                         --
-- vim.g.neovide_touch_deadzone                --  6.0
-- vim.g.neovide_touch_drag_timeout            --  0.17
-- vim.g.neovide_transparency                  --  0.95
-- vim.g.neovide_underline_stroke_scale        --  1.0
-- vim.g.neovide_unlink_border_highlights      --  true
-- vim.g.neovide_window_blurred                --  false
