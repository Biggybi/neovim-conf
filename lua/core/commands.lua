local com = vim.api.nvim_create_user_command

com(
  "Reverse",
  function(opts) vim.cmd(string.format("keeppatterns %d,%dg/^/m%d", opts.line1, opts.line2, opts.line1 - 1)) end,
  { range = true, desc = "Reverse lines" }
)

local get_highlight_groups = require("core.conflib").get_highlight_groups

com("Hi", function(opts)
  table.foreach(opts.fargs, function(_, arg)
    vim.notify("Highlight groups for " .. arg)
    table.foreach(get_highlight_groups(arg), function(_, group) vim.print(group) end)
  end)
end, {
  nargs = "*",
  complete = "highlight",
  desc = "Get highlight groups matching a pattern",
})
